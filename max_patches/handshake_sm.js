//
//  ConnectedCollaboration - handshake_sm.js
//

var debug = 0;	// set to 1 to print debug messages

var my_state;
var address;
var self_ip = "127.0.0.1";
var focus_ip;	// IP that we're trying to call

var HEADER = {	// the first byte of the packets-indicates the nature of the message
	PING:0,
	ACK:1,
	READY:2,
	BUSY:3,
	RING:4,
	ACKRING:5,
	ACCEPT:7,
	REJECT:8,
	STOP:9
}	
var STATE = {	// state of the SM
	RESET:0,
	PING:1,
	ACKSTATUS:2,
	READYRING:3,
	ACKRING:4,
	ACCEPT:5,
	ALLACCEPT:6
}

var wdt_packet;	// wdt: store the previous action to repeat if we get stuck.
var wdt_ip = "";
var wdt_timeout_count = 0;	// how many times we've failed in a row
var WDT_TIMEOUT_MAX = 10;	// after MAX timeouts we reset

inlets = 1;	// set Max inlets and outlets
outlets = 3;	// 0 = packets, 1 = host ip, 2 = UI messages/alerts

set_state(STATE.RESET);
outlet(2, "ready");
// ---- end init ---- //

function connect(remote_ip){
	//ping
	disconnect();
	focus_ip = remote_ip;	// set focus IP so we know who we're trying to call
	set_state(STATE.PING);
	send(["/tx", HEADER.PING, self_ip], remote_ip);
	outlet(2, "connecting to " + remote_ip);
}

function reset() {	// PRIVATE: reset our state and remote IP
	my_state = STATE.RESET;
	focus_ip = "";
	outlet(1, "close");	// tell the UI to close the call dialog
	outlet(1, "stop");
	wdt_timeout_count = 0;	// reset watch dog timer
	wdt_ip = "";
}

function set_state(newState) {
	my_state = newState;
	if (debug)
		post("state " + newState);
}
	
function disconnect(){
	if (focus_ip != "") {
		send(["/tx", HEADER.STOP, self_ip], focus_ip);
		outlet(2, "disconnected.");
		reset();
	}
}
	
function send(packet, remote_ip){
	if (remote_ip == focus_ip) {	// only want to set our WDT up if we're talking to our focus host
		wdt_packet = packet;
		wdt_ip = remote_ip;
		wdt_timeout_count = 0;	// reset counter
	}
	outlet(0, ["host", remote_ip]);
	outlet(0, packet);
}
	
function receive(header, remote_ip){
	//ping -> ack
	//     -> status
	//ack  -> increment state
	//ready -> stop/ring
	//ring -> ackring
	//      -> accept
	  //    -> reject
	//reject -> stop
	//accept -> call video
	switch(header){
	case HEADER.PING:
		send(HEADER.ACK+" "+self_ip, remote_ip);
		if (my_state == STATE.RESET){
			set_state(STATE.ACKSTATUS);
			focus_ip = remote_ip;
			send(["/tx", HEADER.READY, self_ip], remote_ip);
		} else {
			send(["/tx", HEADER.BUSY, self_ip], remote_ip);
		}
		break;
	case HEADER.READY:	// remote reports they are ready and waiting
		if (remote_ip == focus_ip && (my_state == STATE.ACKSTATUS || my_state == STATE.PING)) {
			send(["/tx", HEADER.RING, self_ip], remote_ip);
			set_state(STATE.READYRING);
			outlet(2, "calling " + remote_ip + ". . .");
		} // else ERROR!
		break;
	case HEADER.BUSY:	// remote reports they are busy, we can't call them
		if (remote_ip == focus_ip && (my_state == STATE.PING || my_state == STATE.ACKSTATUS || my_state == STATE.READYRING)) {
			reset();
			outlet(2, "connection failed, remote is busy.");
		}	// else we're in a call, or otherwise not interested
		break;
	case HEADER.RING:	// remote is requesting a call
		if (remote_ip == focus_ip)
			if (my_state == STATE.ACKSTATUS) {	// other is initiating the call, now we "ring" the UI
				send(["/tx", HEADER.ACKRING, self_ip], remote_ip);
				set_state(STATE.ACKRING);
				// ring!
				outlet(1, ["ring_remote", remote_ip]);
				outlet(2, "someone is calling *ring* *ring*");
			}
		break;
	case HEADER.ACKRING:
		if (remote_ip == focus_ip && my_state == STATE.READYRING) { // we're making the call, start "ringing"
				// ring!
			outlet(1, ["ring_local", remote_ip]);
			outlet(2, "phone is ringing...");
		}
		break;
	case HEADER.ACCEPT:	// remote has accepted our call!
		if (remote_ip == focus_ip) {
			if (my_state == STATE.READYRING) {	// we're making the call, they've accepted
				outlet(1, "close");	// tell the UI to close the call dialog
				set_state(STATE.ALLACCEPT);
				send(["/tx", HEADER.ACCEPT, self_ip], remote_ip);
				outlet(1, ["stream", remote_ip]);				// start streaming.
			} else if (my_state == STATE.ACCEPT) {	// we accepted the call and they ack'd
				set_state(STATE.ALLACCEPT);
				outlet(1, ["stream", remote_ip]);				// start streaming.
			}
			outlet(2, "call accepted!");
		}
		break;
	case HEADER.REJECT: // remote has declined our call, just reset.
	case HEADER.STOP:
		if (focus_ip == remote_ip) {
			reset();
			outlet(2, "call to " + remote_ip + " was declined.");
		}
		break;	
	}
}


function watchDogTimer() {	// we've timed out, take another action to try and jumpstart the process.
	if (wdt_timeout_count++ > WDT_TIMEOUT_MAX) {	// increment the timeout count
		reset();
	} else if (wdt_ip != "") {	// else resend most recent packet
		outlet(0, ["host", wdt_ip]);	// this is the send() function, without the wdt data update
		outlet(0, wdt_packet);
	}
}
function accept() {	// function for user to accept a remote call
	if (my_state == STATE.ACKRING && focus_ip != "") {
		send(["/tx", HEADER.ACCEPT, self_ip], focus_ip);
		set_state(STATE.ACCEPT);
	}
}

function cancel() {	// player has declined a call
	if (focus_ip != "" && (my_state == STATE.READYRING || my_state == STATE.ACKRING)) {
		send(["/tx", HEADER.REJECT, self_ip], focus_ip);
		outlet(2, "declined call (" + focus_ip + ")");
		reset();
	}
}

function ip(local_ip) {
	if (local_ip && local_ip.length > 6)
		self_ip = local_ip;
	else
		post("error: local IP address unknown. Are you connected to the internet?\n");
}