// strcmp.js
// compare two strings, if they match send string out the first outlet
//  if they are different, send string out the second outlet
//

inlets = 2;
outlets = 2;

var string1;
var string2;

function anything() 
{
		var a = arrayfromargs(messagename, arguments);
		if (inlet == 0) {
			string1 = messagename;
			if (string1 == string2)
				outlet(0, string1); 
			else
				outlet(1, string1);
		}
		if (inlet == 1)
			string2 = messagename;
}