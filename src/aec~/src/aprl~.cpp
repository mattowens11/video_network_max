#include "maxcpp5.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <memory.h>
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "Gets.h"

#include "RakSleep.h"
#include "../DependentExtensions/RakVoice.h"
#include "RakNetStatistics.h"
#include "GetTime.h"
#include "RakAssert.h"

// Reads and writes per second of the sound data
// Speex only supports these 3 values
//#define SAMPLE_RATE  (8000)
//#define SAMPLE_RATE  (16000)
#define SAMPLE_RATE  (32000)

// number of samples / speex size - # of samps = MSP vector * channels
#define FRAMES_PER_BUFFER 128 // (128 / (32000 / SAMPLE_RATE))
#define RECEIVE_BUFFER_SIZE 4096 //8192

// define sample type. Only short(16 bits sound) is supported at the moment.
typedef short SAMPLE;

struct myStat{
	unsigned int time;
	unsigned int bitsRec;
	unsigned int bitsSent;
};


class Aprl : public MspCpp5<Aprl> {
private:
    RakNet::RakPeerInterface *rakPeer;
    RakNet::RakVoice* rakVoice;
    RakNet::Packet *p;
    SAMPLE* sendBuf;
    SAMPLE* rcvBuf;
    int rcvWritePtr, rcvReadPtr, sendWritePtr, sendReadPtr;
    bool mute;
    int samples;
    
    
    void LogStats(){
        RakNet::RakNetStatistics *rss=rakPeer->GetStatistics(rakPeer->GetSystemAddressFromIndex(0));
        char buffer[1024];
        StatisticsToString(rss,buffer,1);
        printf(buffer);
    }
public:
	Aprl(t_symbol * sym, long ac, t_atom * av) : rcvWritePtr(0), rcvReadPtr(0), sendWritePtr(0), sendReadPtr(0) { 
		setupIO(&Aprl::perform, 2, 2); 
		post("created an example"); 
        
        rakPeer = NULL;
        
        mute=false;
        bool quit;
//        char ch;
        
        char port[256];
        rakPeer = RakNet::RakPeerInterface::GetInstance();
        
        strcpy(port, "60000");
        RakNet::SocketDescriptor socketDescriptor(atoi(port),0);
        
        rakPeer->Startup(4, &socketDescriptor, 1);
        
        rakPeer->SetMaximumIncomingConnections(4);
        rakVoice = new RakNet::RakVoice();
        rakPeer->AttachPlugin(rakVoice);
        
        rakVoice->Init(SAMPLE_RATE, FRAMES_PER_BUFFER*sizeof(SAMPLE));
        rakVoice->SetVAD(false);    // Voice Activation Detection
        rakVoice->SetVBR(false);    // Variable Bit Rate
        rakVoice->SetNoiseFilter(false);
        rakVoice->SetEncoderComplexity(5);
        
        quit=false;

        rakPeer->Connect("127.0.0.1", 60000, 0,0);
        
        sendBuf = new SAMPLE[FRAMES_PER_BUFFER];
        rcvBuf = new SAMPLE[RECEIVE_BUFFER_SIZE];
	}
	Aprl() { 
    	rakPeer->Shutdown(300);
        RakNet::RakPeerInterface::DestroyInstance(rakPeer);
        
        delete sendBuf;
        delete rcvBuf;

        post("freed an example"); 
    }	
	
	// methods:
    void PrintParameters() {
        post("Complexity=%3d Noise filter=%3s VAD=%3s VBR=%3s msp samples=%d of size %d vs speex size of %d"
             ,rakVoice->GetEncoderComplexity()
             ,(rakVoice->IsNoiseFilterActive()) ? "ON" : "OFF"
             ,(rakVoice->IsVADActive()) ? "ON" : "OFF"
             ,(rakVoice->IsVBRActive()) ? "ON" : "OFF"
             ,samples, sizeof(t_sample), sizeof(SAMPLE));
    }
    
	void bang(long inlet) { 
        // Increase encoder complexity
        int v = rakVoice->GetEncoderComplexity();
        if (v<10) rakVoice->SetEncoderComplexity(v+1);

		PrintParameters(); 
	}
	void test(long inlet, t_symbol * s, long ac, t_atom * av) { 
		post("%s in inlet %i (%i args)", s->s_name, inlet, ac);
	}
    void vbr(bool onOff) {
        rakVoice->SetVBR(onOff);
    }
	
	// optional method: gets called when the dsp chain is modified
	void dsp() { post("user-dsp"); }
	
	// signal processing example: inverts sign of inputs
	void perform(int vs, t_sample ** inputs, t_sample ** outputs) {
        short i;
        
        samples = vs*2;
        
//        int channel = 0;
        for (int channel = 0; channel < 1; channel++) {
            t_sample* in = inputs[channel];
            for (i = 0; i < vs; i++, sendWritePtr++)
                sendBuf[sendWritePtr] = (SAMPLE)(in[i] * 32768.0); // + rand() / RAND_MAX);
//            if (sendWritePtr == vs) {
//                sendBuf[0] = (sendBuf[0] + sendBuf[FRAMES_PER_BUFFER-1]) * 0.5;
//                sendBuf[1] = (sendBuf[0] + sendBuf[1]) * 0.5;
//                sendBuf[2] = (sendBuf[2] + sendBuf[1]) * 0.5;
//            }
        }
        if (sendWritePtr >= FRAMES_PER_BUFFER-1) {
            for (i=0; i < rakPeer->GetMaximumNumberOfPeers(); i++)
            {
                rakVoice->SendFrame(rakPeer->GetGUIDFromIndex(i), sendBuf); //inputs[0]);
            }
            sendWritePtr = 0;
        }
//#else
//        rakVoice->SendFrame(RakNet::UNASSIGNED_SYSTEM_ADDRESS, (char*)sendBuf); //inputs[0]);
//#endif
        
//        rakVoice->ReceiveFrame(outputs[0]);
        
        p=rakPeer->Receive();
		while (p)
		{
			if (p->data[0]==ID_CONNECTION_REQUEST_ACCEPTED)
			{
				post("ID_CONNECTION_REQUEST_ACCEPTED from %s\n", p->systemAddress.ToString());
				rakVoice->RequestVoiceChannel(p->guid);
			}
			else if (p->data[0]==ID_RAKVOICE_OPEN_CHANNEL_REQUEST)
			{
				post("\nOpen Channel request from %s\n", p->systemAddress.ToString());
			}
			else if (p->data[0]==ID_RAKVOICE_OPEN_CHANNEL_REPLY)
			{
				post("\nGot new channel from %s\n", p->systemAddress.ToString());
			}
            else if (p->data[0]==ID_ALREADY_CONNECTED)
			{
				post("ID_ALREADY_CONNECTED\n");
			}
			else if (p->data[0]==ID_RAKVOICE_CLOSE_CHANNEL)
			{
				post("ID_RAKVOICE_CLOSE_CHANNEL\n");
			}
			else if (p->data[0]==ID_DISCONNECTION_NOTIFICATION)
			{
				post("ID_DISCONNECTION_NOTIFICATION\n");
			}
            
			rakPeer->DeallocatePacket(p);
			p=rakPeer->Receive();
		}

        // calculate some variables we'll need ahead
//        int bufferSizeBytes = rakVoice->GetBufferSizeBytes();
        int bytesToRead = rakVoice->GetBufferedBytesToReturn(rakPeer->GetGUIDFromIndex(0));
        if (bytesToRead >= FRAMES_PER_BUFFER) {
//            for (int i = 0; i < bytesToRead/(FRAMES_PER_BUFFER*2); i++) {
            rakVoice->ReceiveFrame(rcvBuf+rcvWritePtr); //outputs[0]);
            
            for (int i = 0; i < 128; i++) {
                int index = (rcvWritePtr+i) % RECEIVE_BUFFER_SIZE;
                if (index < 0) index += RECEIVE_BUFFER_SIZE;
                int index2 = (index+1) % RECEIVE_BUFFER_SIZE;
                rcvBuf[index] = index * 8; //(rcvBuf[index]+rcvBuf[index2]) * 0.5;
            }
            rcvWritePtr = (rcvWritePtr + FRAMES_PER_BUFFER) % RECEIVE_BUFFER_SIZE;
//            }
        }
        int sampsToRead = (rcvWritePtr >= rcvReadPtr ? rcvWritePtr - rcvReadPtr : rcvWritePtr+RECEIVE_BUFFER_SIZE - rcvReadPtr);
        if (sampsToRead >= vs) {
            for (int channel = 0; channel < 1; channel++) {
                t_sample * out = outputs[channel];
                for (int i=0; i<vs; i++) {
                    out[i] = (t_sample)(rcvBuf[rcvReadPtr] / 32768.0);
//                    out[i] = (t_sample)(rcvBuf[i + channel*vs] / 32768.0); //-in[i];
                    rcvReadPtr = (rcvReadPtr+1) % RECEIVE_BUFFER_SIZE;
                }
//                if (rcvReadPtr >= RECEIVE_BUFFER_SIZE)
//                    rcvReadPtr = 0;
            }
        } else {
            post("aprl~: buffer underrun");
            for (int channel = 0; channel < 1; channel++) {
                t_sample * out = outputs[channel];
                for (int i=0; i<vs; i++) {
                    out[i] = 0.0;
                    //                    out[i] = (t_sample)(rcvBuf[i + channel*vs] / 32768.0); //-in[i];
                }
            }
        }

	}
};

extern "C" int main(void) {
	// create a class with the given name:
	Aprl::makeMaxClass("aprl~");
	REGISTER_METHOD(Aprl, bang);
	REGISTER_METHOD_GIMME(Aprl, test);
}