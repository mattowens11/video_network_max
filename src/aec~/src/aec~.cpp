#include "maxcpp5.h"

#include "speex/speex_echo.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <memory.h>

#define MIN_FILTER_LENGTH 100				// the smallest filter length allowed. 100 ms is thought to be ideal (sample rate * 0.1)

#define INT_TO_FLOAT 0.000030517578125		// constant: 1/32768.0 used to convert 16bit int samples to 32 bit float

struct myStat{
	unsigned int time;
	unsigned int bitsRec;
	unsigned int bitsSent;
};


class aec : public MspCpp5<aec> {
private:
    SpeexEchoState *echo_state;
	spx_int16_t *buf1, *buf2, *bufOut;
	int bufferSizes;
	int frameSize, filterLength;
    
public:
	aec(t_symbol * sym, long ac, t_atom * av) : frameSize(0), filterLength(0) { 
		setupIO(&aec::perform, 2, 1); 
		
		float sr = sys_getsr();
		filterLength = (int)(sr * 0.1f);	// set the filter to 100ms by default
		if (ac > 0 && av->a_type == A_LONG)
			filterLength = atom_getlong(av);
		if (filterLength < MIN_FILTER_LENGTH)
			filterLength = MIN_FILTER_LENGTH;
		echo_state = 0x00;
		//if (echo_state)
		//	post("a(coustic) e(cho) c(anceller)~ initialized."); 
		//else
		//	post("aec~ error: failed to init.");
        
		bufferSizes = sys_getmaxblksize();
		buf1 = new spx_int16_t[bufferSizes];
		buf2 = new spx_int16_t[bufferSizes];
		bufOut = new spx_int16_t[bufferSizes];
	}
	~aec() { 
		delete buf1;
		delete buf2;
		delete bufOut;

		if (echo_state != 0x00)
			speex_echo_state_destroy(echo_state);
    }	
	
	// methods:
    void PrintParameters() {
    }
    
	void bang(long inlet) { 
		PrintParameters(); 
	}
	void reset(long inlet, t_symbol * s, long ac, t_atom * av) { 
		//post("%s in inlet %i (%i args)", s->s_name, inlet, ac);
		//if (s->s_name == "reset")
		//	reset();
		if (echo_state)
			speex_echo_state_reset(echo_state);
	}
    void filterlength(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            int len = atom_getlong(av);
            len = (len < MIN_FILTER_LENGTH ? MIN_FILTER_LENGTH : len);
            filterLength = len;
            
            if (echo_state != 0x00) { // reset the echo state, recreate it
                speex_echo_state_destroy(echo_state);
                echo_state = speex_echo_state_init(frameSize, filterLength);
            }   // otherwise just wait for the dsp() function to create it
        }
    }
	
	// optional method: gets called when the dsp chain is modified
	void dsp() { 
		int blk = sys_getblksize();
		if (echo_state == 0x00 || frameSize != blk) {
			frameSize = blk;
			if (echo_state != 0x00)
				speex_echo_state_destroy(echo_state);
			echo_state = speex_echo_state_init(frameSize, filterLength);
		}
	}
	
	// signal processing example: inverts sign of inputs
	void perform(int vs, t_sample ** inputs, t_sample ** outputs) {

		if (echo_state) {
			t_sample* in = inputs[0];
			t_sample* in2 = inputs[1];
			for (int i = 0; i < vs; i++) {
				buf1[i] = (spx_int16_t)(in[i] * 32768.0);
				buf2[i] = (spx_int16_t)(in2[i] * 32768.0);
			}
			speex_echo_cancellation(echo_state, buf1, buf2, bufOut);

			t_sample* out = outputs[0];
			for (int i = 0; i < vs; i++) {
				out[i] = (t_sample)(bufOut[i] * INT_TO_FLOAT);
			}
		} else {
			t_sample* out = outputs[0];
			for (int i = 0; i < vs; i++) {
				out[i] = 0.0f;
			}
		}
    }
};

extern "C" {
    void aec_assist(aec* x, void *b, long m, long a, char* s);
    
    int main(void) {
        // create a class with the given name:
        aec::makeMaxClass("aec~");
        class_addmethod(MaxCppBase<aec>::m_class, (method)aec_assist, "assist", A_CANT, 0);
        REGISTER_METHOD(aec, bang);
        REGISTER_METHOD_GIMME(aec, reset);
        REGISTER_METHOD_GIMME(aec, filterlength);
    }
    void aec_assist(aec* x, void *b, long m, long a, char* s) {
        if (m == ASSIST_INLET) {
            switch (a) {
                case 0:
                    sprintf(s, "(Signal) Input from microphone.");
                    break;
                case 1:
                    sprintf(s, "(Signal) Audio as sent to speakers (typically from remote host).");
                    break;
            }
        } else
            sprintf(s, "(Signal) Echo canceled microphone input (typically: send to remote host).");
    }
} // extern "C"