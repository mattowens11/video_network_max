#ifdef _WIN32
#define _WINSOCKAPI_    // stops windows.h including winsock.h
#include <windows.h>
#include <stdint.h>		// for INT_MAX value
#endif

#include "maxcpp5.h"
#include "max.trip.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <memory.h>

// TODO: should check sample rates between the two systems and pop some major warnings if they don't match!
// TODO: should there be 2 messages: disconnect and disconnectall? one to cut outgoing connections, one to cut all?

// number of samples / speex size - # of samps = MSP vector * channels
//#define FRAMES_PER_BUFFER 128 // 64 per vector x 2 channels //x triple overlap
//#define RECEIVE_BUFFER_SIZE 1536 //8192

//#define TX_REDUNDANCY 3 // how many times each chunk of data will be sent

#define DEFAULT_PORT 8001

//#define InChannels 2   // how many channels of audio to pack up and send
//#define OutChannels 2  // how many channels we're prepared to receive and put out

#define RECONNECT_TIME 2000 // how often we try to reconnect if the connection drops

// define sample type. 16 bits sound is supclientPorted at the moment.
typedef short SAMPLE;
#define SAMPLE_MAX 32768

class MaxTrip : public MspCpp5<MaxTrip> {
public:
    char InChannels, OutChannels;
private:
    t_sample SAMPLE_MAX_1; // sample max over 1
    void * dumpOut;
    RakNet::RakPeerInterface *rakPeer;
    RakNet::Packet *rakPacket;
    RakNet::BitStream *myBitStream;
    bool TX_ACTIVE; // turn transmission on and off
    unsigned char sampleSize, TX_REDUNDANCY;
    SAMPLE* sendBuf;        // buffer to queue up data for TX
    int sendBufSize, rcvBufSize, rcvBufChannelSize, rcvBufWindowCount;    //  how big the send/rcv buffer is, how much space 1 channel uses in the rcvBuf, how many windows the receive buffer should cover
    SAMPLE* rcvBuf;
    int rcvWritePtr, rcvReadPtr, sendWritePtr, sendReadPtr;
    bool isConnected;
    int inPacketID, outPacketID, inputWindowSize;  //inputWindowSize - how much data we receive in a packet
    char *remoteIPAddress;
    unsigned short   clientPort, serverPort;    // the port we try to connect to (client) and the port we listen on (server)
    RakNet::TimeMS  reconnectTimer;
    bool failedMessageFlag;
    int windowsToBuffer, windowsSinceTx, windowSizeTX;    // how many windows of data to buffer between transmissions, how many we've compiled since sending, how big the sample vector is at each dsp update
    PacketPriority packetPriority;    // the RakNet transmission priority code
    bool    reliableTX;     // send using RakNet reliable option?
    char msgUnderrunCounter;    // prevent message spamming
    
    void createRakPeer() {
        rakPeer = RakNet::RakPeerInterface::GetInstance();
        
        RakNet::SocketDescriptor socketDescriptor(serverPort,0);   //atoi(clientPort)
        
        bool started = (rakPeer->Startup(32, &socketDescriptor, 1)==RakNet::RAKNET_STARTED); // number of allowed connections
        if (!started)
            ; //post("max.trip~: failed to startup network layer"); // this gets triggered when an object is recreated in the patcher... hm.
        else {
            rakPeer->SetMaximumIncomingConnections(32);
        }
    }
    void checkTXBuffer(int vs) {    // compare the TX buffer space to the audio sample vector size. update if necessary
        if (vs != windowSizeTX) {   // we need to reallocate?
            int newSize = vs*InChannels*windowsToBuffer;   // buffer size
            SAMPLE* temp = new SAMPLE[newSize]; // new buffer
            if (sendBufSize > 0 && sendBuf)
                memcpy(temp, sendBuf, (sendBufSize > newSize ? newSize : sendBufSize) * sampleSize); // copy existing data over
            if (sendBuf != NULL)    // remove old buffer?
                delete sendBuf;
            sendBuf = temp;     // update persistent variables
            windowSizeTX = vs;
            sendBufSize = newSize;
        }
    }
    void checkRXBuffer(int vs) {
        int newSize = OutChannels * rcvBufWindowCount * vs; //inputWindowSize * 2;
        if (newSize > rcvBufSize) {   // we need to reallocate?
//            int newSize = vs*InChannels*windowsToBuffer;   // buffer size
            SAMPLE* temp = new SAMPLE[newSize]; // new buffer
            for (int i = 0; i < newSize; i++)
                temp[i] = 0;
            if (rcvBufSize > 0 && rcvBuf)
                memcpy(temp, rcvBuf, (rcvBufSize > newSize ? newSize : rcvBufSize) * sampleSize); // copy existing data over
            if (rcvBuf != NULL)    // remove old buffer?
                delete rcvBuf;
            rcvBuf = temp;     // update persistent variables
            rcvBufSize = newSize;
            if (OutChannels > 0)
                rcvBufChannelSize = newSize / OutChannels;
            else rcvBufChannelSize = newSize;
            rcvReadPtr = newSize * 0.5; // start the read pointer 1/2 way around the ring buffer
            rcvWritePtr = 0;
        }
    }
public:
	MaxTrip(t_symbol * sym, long ac, t_atom * av) : InChannels(2), OutChannels(2), TX_ACTIVE(true), TX_REDUNDANCY(3), rcvBufChannelSize(0), rcvBufWindowCount(16), rcvWritePtr(0), rcvReadPtr(0), sendWritePtr(0), sendReadPtr(0), isConnected(false), inPacketID(0), outPacketID(0), remoteIPAddress(NULL), failedMessageFlag(false), windowsSinceTx(0), windowSizeTX(0), packetPriority(IMMEDIATE_PRIORITY), reliableTX(false), msgUnderrunCounter(0)
    {
        
        SAMPLE_MAX_1 = 1.0f / SAMPLE_MAX; // not going to change now, but if SAMPLE_MAX does then this does too!
//        for (int i = 0; i < ac; i++) {
//            Atom* ap = av+i;
//            switch (atom_gettype(ap)) {
//                case A_LONG:
//                    post("%i=%ld ", i, atom_getlong(ap));
//                    break;
//                case A_FLOAT:
//                    post("%i=%f ", i, atom_getfloat(ap));
//                    break;
//                case A_SYM:
//                    post("%s ", atom_getsym(ap)->s_name);
//                    break;
//                default:
//                    post("%ld: unknown atom type (%ld)", i+1, atom_gettype(ap));
//                    break;
//            }
//        }
        windowsToBuffer = TX_REDUNDANCY; // * 1 //
        
        serverPort = DEFAULT_PORT;
        remoteIPAddress = "127.0.0.1"; //NULL;
        clientPort = DEFAULT_PORT;
        if (ac > 0) {
            bool argError = false;
            if (atom_gettype(av) == A_LONG)
                InChannels = atom_getlong(av);
            else argError = true;
            if (ac > 1) {
                if (atom_gettype(++av) == A_LONG)
                    OutChannels = atom_getlong(av);
                else argError = true;
                if (ac > 2) {
                    if (atom_gettype(++av) == A_LONG)
                        serverPort = atom_getlong(av);
                    else argError = true;
                
                    if (ac > 3) {
                        if (atom_gettype(++av) == A_SYM)
                            remoteIPAddress = atom_getsym(av)->s_name;
                        else if (atom_gettype(av) == A_LONG)
                            clientPort = atom_getlong(av);
                        else argError = true;
                        
                        if (ac > 4) {
                            if (atom_gettype(++av) == A_LONG)
                                clientPort = atom_getlong(av);
                            else argError = true;
                        }
                    }
                }
            }
            if (argError)
                post("max.trip~: argument format error, should be <Local Port> <IP> <Remote Port>");
        }
        
        if (InChannels < 1) {
            TX_ACTIVE = false;
            InChannels = 1;
        }
        OutChannels = (OutChannels < 0 ? 0 : OutChannels);  // no maximum set at the moment. Probably should have some sort of protection!
    
        dumpOut = outlet_new(this, NULL); // create a dump outlet
        setupIO(&MaxTrip::perform, InChannels, OutChannels);
        
        rakPeer = NULL;
        createRakPeer();
        
        sampleSize = sizeof(SAMPLE);
        myBitStream = new RakNet::BitStream();
        sendBufSize = 0; //FRAMES_PER_BUFFER*windowsToBuffer;
        sendBuf = NULL; //new SAMPLE[sendBufSize];
        rcvBufSize = 0;
        rcvBuf = NULL; //new SAMPLE[RECEIVE_BUFFER_SIZE*OutChannels];
        
        reconnectTimer = RakNet::GetTimeMS();
		post("max.trip~ created by Benjamin Smith"); 
	}
	~MaxTrip() { 
        disconnect(0);
    	rakPeer->Shutdown(0);
        RakNet::RakPeerInterface::DestroyInstance(rakPeer);
        
        delete myBitStream;
        delete sendBuf;
        delete rcvBuf;
    }	
	
	// methods:
    void connect(long inlet) {
        if (!rakPeer)
            createRakPeer();
        
//        post("connecting to %s:%d", remoteIPAddress, clientPort);
        if (remoteIPAddress != NULL) {
            bool b = rakPeer->Connect(remoteIPAddress, clientPort, 0, 0, 0)==RakNet::CONNECTION_ATTEMPT_STARTED;
            if (b==false)
                post("max.trip~: net connection failed");
            else {
                post("max.trip~: connecting to %s:%i...", remoteIPAddress, clientPort);
                rcvReadPtr = rcvWritePtr;
            }
        }
    }
    void connect(long inlet, t_symbol * ip, long p) {
        remoteIPAddress = ip->s_name;
        clientPort = p;
        connect(inlet);
    }
    void disconnect(long inlet)
    {
		t_atom at;
        for (int i = 0; i < rakPeer->GetMaximumNumberOfPeers(); i++) {      // here it closes all connections.
			atom_setsym(&at, gensym(rakPeer->GetSystemAddressFromIndex(i).ToString()));
			outlet_anything(dumpOut, gensym("disconnected"), 1, &at);
            rakPeer->CloseConnection(rakPeer->GetSystemAddressFromIndex(i),true,0);	// this doesn't appear to release the peer-system-address... it spits out a stream of "UNASSIGNED_SYSTEM_ADDRESS" after many connects
        }
        if (isConnected)
            post("max.trip~ disconnected");
        isConnected=false;
    }
    void ip(long inlet, t_symbol * s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_SYM) {
            if (isConnected && remoteIPAddress != atom_getsym(av)->s_name)
                disconnect(inlet);                          // this should really only disconnect a self-initiated connection, not all. same for port
            remoteIPAddress = atom_getsym(av)->s_name;
        }
    }
    void port(long inlet, t_symbol * s, long ac, t_atom * av) {
        short p = -1;
        if (ac > 0 && atom_gettype(av) == A_LONG)
            p = atom_getlong(av);
        if (clientPort != p && isConnected)
            disconnect(inlet);
        clientPort = p;
    }
    void rxbuffer(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            rcvBufWindowCount = atom_getlong(av);  // upper limit is not set, so the user has to be somewhat smart!
            rcvBufWindowCount = (rcvBufWindowCount < 2 ? 2 : rcvBufWindowCount);   // this will cause the rx buffer to be reallocated
        }
    }
    void buffer(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            char buf = atom_getlong(av);
            buf = (buf < 1 ? 1 : buf);
            windowsToBuffer = buf * TX_REDUNDANCY;  // upper limit is 255, so the user has to be somewhat smart!
            windowSizeTX = 0;   // this will cause the send buffer to be reallocated in checkTXBuffer
        }
    }
    void redundancy(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            windowsToBuffer /= TX_REDUNDANCY;
            TX_REDUNDANCY = atom_getlong(av);
            TX_REDUNDANCY = (TX_REDUNDANCY < 1 ? 1 : (TX_REDUNDANCY > 8 ? 8 : TX_REDUNDANCY));
            windowsToBuffer = (windowsToBuffer < 1 ? 1 : windowsToBuffer);  // sanity check
            windowsToBuffer *= TX_REDUNDANCY;
            windowSizeTX = 0;   // cause the send buffer to be reallocated
        }
    }
    void transmit(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            TX_ACTIVE = (atom_getlong(av) != 0);
        }
    }
    void priority(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            if (atom_getlong(av) > 0)
                packetPriority = IMMEDIATE_PRIORITY;
            else
                packetPriority = HIGH_PRIORITY;
        }
    }
    void reliable(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            reliableTX = atom_getlong(av) != 0;
        }
    }
    
    // send out some message to all remote hosts. Should have a broadcast vs single host option here!
    void send(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0) {
            myBitStream->Reset();
            myBitStream->Write((u_char)ID_OTHER);
            myBitStream->Write((u_char)ac);
            for (int i = 0; i < ac; i++) {
                switch (atom_gettype(av)) {
                    case A_LONG:
                        myBitStream->Write((u_char)A_LONG);
                        myBitStream->Write((long)atom_getlong(av++));
                        break;
                    case A_FLOAT:
                        myBitStream->Write((u_char)A_FLOAT);
                        myBitStream->Write((float)atom_getfloat(av++));
                        break;
                    case A_SYM:
                        myBitStream->Write((u_char)A_SYM);
                        t_symbol * sym = atom_getsym(av++); // s_name is NULL terminated? ... then "gensym("")" on the rx side
                        char j = 0;
                        while (sym->s_name[j] != 0 && j++ < 256)
                            ;   // count the length of the string, but make sure we don't blow up in an error (j>256)
                        myBitStream->Write(j);
                        myBitStream->Write(sym->s_name, j);
                        break;
                }
            }
            rakPeer->Send(myBitStream, packetPriority, (reliableTX ? RELIABLE_SEQUENCED : UNRELIABLE_SEQUENCED), 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

        }
    }

    void PrintParameters() {
        if (isConnected)
            post("Max.Trip~: listening on port %d, connected to remote host at %s:%d", serverPort, remoteIPAddress, clientPort);
        else
            post("Max.Trip~: listening on port %d, attempting to connect to remote host at %s:%d", serverPort, remoteIPAddress, clientPort);
    }
    
	void bang(long inlet) { 
		PrintParameters(); 
	}
	
	// optional method: gets called when the dsp chain is modified
	void dsp() { 
        rcvWritePtr = 0;
        rcvReadPtr = rcvBufSize * 0.5;
        outPacketID = 0;
    }
	
	// signal processing
	void perform(int vs, t_sample ** inputs, t_sample ** outputs) {
        short i;
		t_atom at;
        
        //samples = vs*2;       // vs * channels, don't hard code
        
//        if (!isConnected) {         // try to reconnect every few seconds
//            if (RakNet::GetTimeMS() > reconnectTimer) {
//                connect(0);
//                reconnectTimer = RakNet::GetTimeMS() + RECONNECT_TIME;
//            }
//        }

        if (TX_ACTIVE) {
            checkTXBuffer(vs);  // check the window size against the size of our buffer and reallocate if needed

            sendWritePtr = sendBufSize - vs * InChannels;
            for (int channel = 0; channel < InChannels; channel++) {
                t_sample* in = inputs[channel];
                for (i = 0; i < vs; i++, sendWritePtr++)
                    sendBuf[sendWritePtr] = (SAMPLE)(in[i] * SAMPLE_MAX); // + rand() / RAND_MAX);
            }
            outPacketID = (outPacketID+1) % INT32_MAX;  // max 16 bit - SAMPLE size
            
            if (++windowsSinceTx >= windowsToBuffer / TX_REDUNDANCY) {  // always send each chunk three times to blast through packet loss

                myBitStream->Reset();
                myBitStream->Write((unsigned char)ID_AUDIO);
                myBitStream->Write((int)outPacketID);
                myBitStream->Write((int)vs);  // window size
                myBitStream->Write((char)windowsToBuffer);
                myBitStream->Write((char)InChannels);
                myBitStream->Write((int)(sendBufSize*sampleSize));
                myBitStream->Write((char*)&sendBuf[0], sendBufSize*sampleSize);

//                if (isConnected) //rakPeer)
                // broadcast the data, in a sequenced manner, to all connected peers!
                rakPeer->Send(myBitStream, packetPriority, (reliableTX ? RELIABLE_SEQUENCED : UNRELIABLE_SEQUENCED), 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
                
                windowsSinceTx = 0;
            }
            
            // move the newest data up to cover the oldest
            memcpy(sendBuf, sendBuf + vs*InChannels, (sendBufSize - vs*InChannels) * sampleSize);
        }
        
        checkRXBuffer(vs);  // make sure we have the appropriate space to receive incoming audio data

//        if (rakPeer) {
            rakPacket = rakPeer->Receive();
            while (rakPacket)
            {
                switch (rakPacket->data[0]) {
                    case ID_NEW_INCOMING_CONNECTION:
                    case ID_CONNECTION_REQUEST_ACCEPTED:
                        post("ID_CONNECTION_REQUEST_ACCEPTED from %s", rakPacket->systemAddress.ToString());
                        isConnected=true;
                        rcvReadPtr = rcvBufSize * 0.5;
                        failedMessageFlag = false;  // reset so that we can announce the drop again
						atom_setsym(&at, gensym(rakPacket->systemAddress.ToString()));
						outlet_anything(dumpOut, gensym("connected"), 1, &at);
                        break;
                        // print out errors
                    case ID_CONNECTION_ATTEMPT_FAILED:
                        if (!failedMessageFlag)
                            post("max.trip~ error: connection attempt failed to %s", rakPacket->systemAddress.ToString());
                        isConnected=false;
                        failedMessageFlag = true;   // set the flag so we don't fill the log with messages
						atom_setsym(&at, gensym(rakPacket->systemAddress.ToString()));
						outlet_anything(dumpOut, gensym("disconnected"), 1, &at);
                        break;
                    case ID_ALREADY_CONNECTED:
                        post("Client Error: ID_ALREADY_CONNECTED\n");
                        break;
                    case ID_CONNECTION_BANNED:
                        post("Client Error: ID_CONNECTION_BANNED\n");
                        break;
                    case ID_INVALID_PASSWORD:
                        post("Client Error: ID_INVALID_PASSWORD\n");
                        break;
                    case ID_INCOMPATIBLE_PROTOCOL_VERSION:
                        post("Client Error: ID_INCOMPATIBLE_PROTOCOL_VERSION, %s\n", rakPacket->systemAddress.ToString());
                        break;
                    case ID_NO_FREE_INCOMING_CONNECTIONS:
                        post("Client Error: ID_NO_FREE_INCOMING_CONNECTIONS\n");
                        isConnected=false;
                        break;
                    case ID_DISCONNECTION_NOTIFICATION:
                        post("max.trip~: %s disconnected.", rakPacket->systemAddress.ToString());
                        isConnected=false;
                        failedMessageFlag = false;
						atom_setsym(&at, gensym(rakPacket->systemAddress.ToString()));
						outlet_anything(dumpOut, gensym("disconnected"), 1, &at);
                        break;
                    case ID_CONNECTION_LOST:
                        post("max.trip~: connection to %s lost.", rakPacket->systemAddress.ToString());
                        isConnected=false;
                        failedMessageFlag = false;
						atom_setsym(&at, gensym(rakPacket->systemAddress.ToString()));
						outlet_anything(dumpOut, gensym("disconnected"), 1, &at);
                        break;
                    case ID_AUDIO:
                        HandleAudioPacket(rakPacket);
                        break;
                    case ID_OTHER:
                        HandleBlob(rakPacket);
                        break;
                    default:
                        break;
                }
                
                rakPeer->DeallocatePacket(rakPacket);
                rakPacket = rakPeer->Receive();
            }
//        }

        if (OutChannels) {  // calc how many samples are waiting in the ring buffer
            int sampsToRead = (rcvWritePtr+inputWindowSize >= rcvReadPtr ? rcvWritePtr+inputWindowSize - rcvReadPtr : rcvWritePtr+rcvBufChannelSize+inputWindowSize - rcvReadPtr);
            if (sampsToRead >= vs && rcvBuf) {  // are there samples available equal to or greater than how many we need (vs)?
                for (int channel = 0; channel < OutChannels; channel++) {
                    t_sample * out = outputs[channel];
                    for (int i=0; i<vs; i++) {
                        out[i] = (t_sample)(rcvBuf[rcvReadPtr+i+rcvBufChannelSize*channel] * (t_sample)SAMPLE_MAX_1);
                    }
                }
                rcvReadPtr = (rcvReadPtr+vs) % rcvBufChannelSize;
                msgUnderrunCounter = 0; //(msgUnderrunCounter > 0 ? msgUnderrunCounter-- : 0); // reset our counter for the next underrun
            } else {
                if (isConnected) {
                    if (msgUnderrunCounter == 0)   // count up, so we don't blast the message window.
                        post("max.trip~: buffer underrun"), msgUnderrunCounter=1;
                    if (rcvBuf) {
                        rcvReadPtr = (int)(rcvReadPtr + rcvBufChannelSize * 0.5) % rcvBufChannelSize; // OutChannels * rcvBufWindowCount * vs
                        for (int channel = 0; channel < OutChannels; channel++) {
                            t_sample * out = outputs[channel];
                            for (int i=0; i<vs; i++) {
                                out[i] = (t_sample)(rcvBuf[rcvReadPtr+i+rcvBufChannelSize*channel] * (t_sample)SAMPLE_MAX_1);
                            }
                        }
                        rcvReadPtr = (rcvReadPtr+vs) % rcvBufChannelSize;
                    }
                } else {   // don't bother saying anything if we're not connected
                    for (int channel = 0; channel < OutChannels; channel++) {   // solution: send out 0s. May produce clicks.
                        t_sample * out = outputs[channel];
                        for (int i=0; i<vs; i++) {
                            out[i] = 0;
                        }
                    }
                }
            }
        }
	}
    
    void HandleAudioPacket(RakNet::Packet* audioPacket) {
//        post("got a packet");
        // pull the data out of the packet
        int inID, packetSize, winSize, writePtr, temp;
        char winBuffered, incomingChan;
        
        if (OutChannels < 1)        // we're not outputting audio? just ignore then
            return;

        myBitStream->Reset();
        myBitStream->Write((char*)audioPacket->data, audioPacket->length);
        myBitStream->IgnoreBits(8);   // ignore header byte
        myBitStream->Read(inID);        // ID of this audio sample vector from the remote host
        myBitStream->Read(winSize);     // the remote host's audio sample vector size
        myBitStream->Read(winBuffered); // how many windows of samples of winSize are in this packet
        myBitStream->Read(incomingChan);    // how many channels of audio data the remote host is sending. we may not want all of them
        inputWindowSize = winSize*winBuffered;
        myBitStream->Read(packetSize);
        
        // calculate the write point based on the new audio packet ID
        if (inID < inPacketID) inPacketID -= INT32_MAX;        
        rcvWritePtr = (rcvWritePtr + (inID - inPacketID) * winSize) % rcvBufChannelSize;
        writePtr = rcvWritePtr;
        inPacketID = inID;
//        if (inID - inPacketID > winBuffered*2)    // we've fallen too far behind!
//            rcvReadPtr = rcvWritePtr;
        
        temp = (rcvReadPtr < winSize ? rcvReadPtr + rcvBufChannelSize : rcvReadPtr);
        for (int i = 0; i < winBuffered; i++) // read out the winBuffered chunks of data from the packet
        {
            for (int j = 0; j < incomingChan; j++)     // read out each channel's data
                if (j < OutChannels)    // do we want this channel's data?
                    myBitStream->Read((char*)(rcvBuf+writePtr+rcvBufChannelSize*j), winSize*sampleSize);
            // detect overrun - when the write pointer would pass over the read pointer, causing an audio glitch
            if (writePtr < temp && writePtr + winSize > temp)   // don't let the writer pass over the read head, just ditch data for the moment
                break;
            writePtr = (writePtr + winSize) % rcvBufChannelSize;
        }
//                post("%i %i %i chanSize:%i bufSize:%i ID:%d", 0, rcvReadPtr, rcvWritePtr, rcvBufChannelSize, rcvBufSize, inPacketID);

    }
    
    void HandleBlob(RakNet::Packet* dataPacket) {
        myBitStream->Reset();
        myBitStream->Write((char*)dataPacket->data, dataPacket->length);
        u_char header;
        if (!myBitStream->Read(header))
            return;
        if (header != ID_OTHER)
            return;
        
        u_char numAtoms;
        if (!myBitStream->Read(numAtoms))
            return;
        
        t_atom* dataList = new t_atom[numAtoms+2];    // will be "received" .... IP address of originator
//        long ac;
//        char alloc = true;
//        if (atom_alloc_array(numAtoms, &ac, &dataList, &alloc) != MAX_ERR_NONE) // || !alloc)
//            return; // no memory was assigned!
//        post("assigned %d atoms", ac);
        
        t_symbol * s = gensym("received");
        atom_setsym(&dataList[0], s);
        for (int i = 1; i < numAtoms+1; i++) {
            myBitStream->Read(header);
            switch (header) {
                case A_LONG:
                    long j;
                    myBitStream->Read(j);
                    atom_setlong(&dataList[i], j);
//                    post("got an int %d", j);
//                    outlet_int(dumpOut, rakPacket->data[0]);
                    break;
                case A_FLOAT:
                    float f;
                    myBitStream->Read(f);
                    atom_setfloat(&dataList[i], f);
//                    post("got a float %f", f);
                    break;
                case A_SYM:
                    char len;
                    myBitStream->Read(len);
                    char* sym = new char[len+1];
                    myBitStream->Read(sym, len);
                    sym[(int)len] = 0;   // null terminate
                    s = gensym(sym);
                    atom_setsym(&dataList[i], s);
//                    post("got a symbol %s", s->s_name);
					delete sym;
                    break;
            }
        }   // end for i
        s = gensym(dataPacket->systemAddress.ToString());
        atom_setsym(&dataList[numAtoms+1], s);
        outlet_atoms(dumpOut, numAtoms+2, dataList);
		delete dataList;
    }
};

extern "C" {
    void max_trip_assist(MaxTrip* x, void *b, long m, long a, char* s);
    
    int main(void) {
        // create a class with the given name:
        MaxTrip::makeMaxClass("max.trip~");
    //	REGISTER_METHOD(MaxTrip, bang);
        //	REGISTER_METHOD_GIMME(MaxTrip, test);
        class_addmethod(MaxCppBase<MaxTrip>::m_class, (method)max_trip_assist, "assist", A_CANT, 0);
        REGISTER_METHOD(MaxTrip, connect);
        REGISTER_METHOD(MaxTrip, disconnect);
        REGISTER_METHOD_GIMME(MaxTrip, ip);
        REGISTER_METHOD_GIMME(MaxTrip, port);
        REGISTER_METHOD_GIMME(MaxTrip, rxbuffer);
        REGISTER_METHOD_GIMME(MaxTrip, buffer);
        REGISTER_METHOD_GIMME(MaxTrip, redundancy);
        REGISTER_METHOD_GIMME(MaxTrip, transmit);
        REGISTER_METHOD_GIMME(MaxTrip, reliable);
        REGISTER_METHOD_GIMME(MaxTrip, priority);
        REGISTER_METHOD_GIMME(MaxTrip, send);
    }
    
    void max_trip_assist(MaxTrip* x, void *b, long m, long a, char* s) {
        if (m == ASSIST_INLET) {
            sprintf(s, "(Signal) Input to send to remote host.");
        } else {
            if (a < x->OutChannels)
                sprintf(s, "(Signal) Audio from remote host.");
            else
                sprintf(s, "Dump out and messages from remote host.");
        }
    }
}