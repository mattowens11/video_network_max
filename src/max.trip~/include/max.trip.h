//
//  max.trip.h
//  max.trip
//
//  Created by Benjamin Smith on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef max_trip_max_trip_h
#define max_trip_max_trip_h


#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "Gets.h"
#include "BitStream.h"

#include "RakSleep.h"
#include "RakNetStatistics.h"
#include "GetTime.h"
#include "RakAssert.h"

enum MAXTRIP_PACKET_ID {
    ID_AUDIO = ID_USER_PACKET_ENUM,
    ID_FORMAT,
    ID_OGG_H1,
    ID_OGG_H2,
    ID_OGG_H3,
    ID_OGG_AUDIO,
    ID_OTHER        // it's just some blob we are going to push out again
};

#endif
