/**
* \file	C:\Users\Ross\git\video_network_max\src\AudioNetworkLibrary\MultiConnectionAudio.cpp
*
* \brief	Implements the multi connection audio class.
*/
#include "MultiConnectionAudio.hpp"

#include <boost/make_shared.hpp>

#include <MessageIdentifiers.h>
#include <RakNetTypes.h>
#include <SocketLayer.h>

#include <queue>

#include <jpatcher_syms.h>
#include <jpatcher_api.h>

#include <ext_critical.h>

#define LOCALHOST "127.0.0.1"
#define CLIENT_PORT_DEFAULT 8001
#define SERVER_PORT_DEFAULT 8001

enum MessageIdentifers {
	/* Client message to the server, send audio to everyone */
	ID_SEND_AUDIO = ID_USER_PACKET_ENUM,
	/* Server message to the clients, play this audio */
	ID_RECIEVE_AUDIO,
	/* New client has connected */
	ID_NEW_CLIENT,
	/* Sets the client's id */
	ID_SET_CLIENT_NUMBER,
	/* Sends text, like a chatbox */
	ID_SEND_TEXT,
};

ClientId MultiConnectionAudio::nextClientId(0);

const SampleType MultiConnectionAudio::SAMPLE_MAX( 36768 );
const float MultiConnectionAudio::SAMPLE_MAX_1( 1.0f / MultiConnectionAudio::SAMPLE_MAX );

MultiConnectionAudio::MultiConnectionAudio( t_symbol* sym, long argc, t_atom* argv ) : numInputs(0), numOutputs(0), clientId(0), connectionState(CS_DISCONNECTED), peerInterface(0),
	timeUntilNextReconnect(2.5f), maxReconnectTime(2.5f), connectionAttempts(0), maxConnectionAttempts(32), attemptingReconnect(false),
	audioEnabled(true), voiceEnabled(true) {
		if ( argc > 0 ) {
			/* First argument is the number of inputs */
			numInputs = atom_getlong( argv++ );
			if ( argc > 1 ) {
				/* Second argument is the number of outputs */
				numOutputs = atom_getlong ( argv++ );
			}
		}

		outlet = outlet_new( this, NULL );
		setupIO( &MultiConnectionAudio::Perform, numInputs, numOutputs );

		clientAddress.FromStringExplicitPort( "\0", CLIENT_PORT_DEFAULT );
		serverAddress.FromStringExplicitPort( LOCALHOST, SERVER_PORT_DEFAULT );
		InitializeRakNet();

		serverThread = boost::make_shared<boost::thread>(boost::bind(&MultiConnectionAudio::ServerThread, this));

		clock = clock_new( this, ( method ) MultiConnectionAudio::RegisterClientScheduledTask );
}

MultiConnectionAudio::~MultiConnectionAudio( void ) {
	serverIsRunning = false;
	serverThread->join();

	freeobject( ( t_object * ) clock );

	boost::lock_guard< boost::recursive_mutex > lock( mutex );

	for ( auto it = gainObjects.begin(), end = gainObjects.end(); it != end; ++it ) {
		delete (*it);
	}
	gainObjects.clear();

	for ( auto it = clientSamples.begin(), end = clientSamples.end(); it != end; ++it ) {
		Sample* sample;
		std::deque< Sample* >& sampleQueue = (*it).second;

		while ( !sampleQueue.empty() ) {
			sample = sampleQueue.front();
			delete sample;
			sampleQueue.pop_front();
		}

		sampleQueue.clear();
	}
	clientSamples.clear();
	clientGains.clear();

	KillServer();

	ShutdownRaknet();
}

void MultiConnectionAudio::Perform( int vs, t_sample** inputs, t_sample** outputs ) {
	/* Don't bother if we aren't connected. */
	if ( connectionState == CS_DISCONNECTED ) {
		return;
	} else if ( connectionState == CS_RECONNECTING ) {
		UpdateReconnectionStatus();
		return;
	}
	/* Also, don't bother if we don't have an Id yet */
	if ( clientId == 0 ) {
		return;
	}
	if ( numInputs > 0 && voiceEnabled ) {
		/* Send an packet of input to the server */
		Sample* sample = new Sample( numInputs, vs );
		sample->ReadIn( ( const t_sample**) inputs, numInputs );

		SendAudioSample( sample );

		delete sample;
	}

	if ( numOutputs > 0 ) {
		/* Clear out the ouputs for this frame */
		for ( int channel = 0; channel < numOutputs; ++channel ) {
			memset( outputs[ channel ], 0, vs * sizeof( float ) );
		}
		if ( audioEnabled ) {
			boost::lock_guard< boost::recursive_mutex > guard( mutex );

			for ( auto it = clientSamples.begin(), end = clientSamples.end(); it != end; ++it ) {
				std::deque< Sample* >& sampleQueue = ( *it ).second;
				ClientId id = ( *it ).first;
				float gain = 1.0f;

				auto gainIt = clientGains.find( id );
				if ( gainIt != clientGains.end() ) {
					gain = ( *gainIt ).second;
				}

				if ( !sampleQueue.empty() ) {
					Sample* sample = sampleQueue.front();

					sample->WriteOut( outputs, numOutputs, gain );

					delete sample;
					sampleQueue.pop_front();
				}
			}
		}
	}
}

void MultiConnectionAudio::InitializeRakNet( void ) {
	if ( peerInterface ) {
		ShutdownRaknet();
	}
	peerInterface = RakNet::RakPeerInterface::GetInstance();

	Restart();
}

void MultiConnectionAudio::ShutdownRaknet( void ) {
	if ( peerInterface ) {
		peerInterface->Shutdown(1000);
		RakNet::RakPeerInterface::DestroyInstance( peerInterface );
	}
}

void MultiConnectionAudio::UpdateReconnectionStatus( void ) {
	if ( attemptingReconnect ) {
		return;
	}

	long elapsed = ( long ) reconnectionTimer.elapsed();
	if ( ( timeUntilNextReconnect += elapsed ) >= maxReconnectTime ) {
		AttemptToConnect();
		timeUntilNextReconnect = 0;
	}

	reconnectionTimer.restart();
}

void MultiConnectionAudio::ServerThread( void ) {
	RakNet::Packet* packet;
	RakNet::BitStream message;

	serverIsRunning = true;

	while ( serverIsRunning ) {
		while ( ( packet = peerInterface->Receive() ) ) {
			message.Reset();
			message.Write( ( char* ) packet->data, packet->length );

			switch ( packet->data[ 0 ] ) {
			case ID_NEW_INCOMING_CONNECTION:
				post( "New connection from %s.", packet->systemAddress.ToString() );

				/* Assign the target a new clientId */
				SendClientId( ++nextClientId, packet->systemAddress );

				/* Broadcast the new addition to all connected peers */
				BroadcastNewClient( nextClientId, packet->systemAddress );

				connectedClients.push_back( nextClientId );
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
				post( "Connection to %s accepted.", packet->systemAddress.ToString() );

				if ( peerInterface->IsLocalIP( packet->systemAddress.ToString( false ) ) ) {
					clientId = ++nextClientId;

					if ( connectedClients.size() > 0 ) {
						for ( unsigned int i = 0; i < connectedClients.size(); ++i ) {
							unsigned int client = connectedClients[ i ];
							RegisterClient( client );
						}
					}

					connectedClients.push_back( clientId );

					BroadcastNewClient( clientId, packet->systemAddress );
				}
				OnConnectionAccepted( message );
				break;
			case ID_CONNECTION_ATTEMPT_FAILED:
				post ( "Connection to %s failed.", packet->systemAddress.ToString() );
				OnConnectionFailed( message );
				break;
			case ID_CONNECTION_LOST:
				post( "Packet loss." );
				break;
			case ID_SEND_AUDIO:
				/* We have recieved audio from a client */
				BroadcastAudio( packet, packet->systemAddress );
				break;
			case ID_RECIEVE_AUDIO:
				/* Recieved audio from the server */
				RecieveAudio( message );
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				post( "Disconnected." );
				clientId = 0;
				connectionState = CS_DISCONNECTED;
				outlet_anything( outlet, gensym( "stop" ), 0, NULL );
				break;
			case ID_NEW_CLIENT:
				OnNewClient( message );
				break;
			case ID_SET_CLIENT_NUMBER:
				OnSetClientNumber( message );
				break;
			default:
				post( "Unknown packet: %i ", packet->data[ 0 ] );
				break;
			}
		}
	}
}

void MultiConnectionAudio::OnConnectionAccepted( RakNet::BitStream& message ) {
	connectionState = CS_CONNECTED;
	connectionAttempts = 0;
	timeUntilNextReconnect = maxReconnectTime;
	attemptingReconnect = false;
}

void MultiConnectionAudio::OnConnectionFailed( RakNet::BitStream& message ) {
	connectionAttempts++;

	if ( connectionAttempts >= maxConnectionAttempts ) {
		connectionState = CS_DISCONNECTED;
		connectionAttempts = 0;
	} else {
		connectionState = CS_RECONNECTING;
	}

	timeUntilNextReconnect = 0;
	attemptingReconnect = false;
}

void MultiConnectionAudio::AttemptToConnect() {
	attemptingReconnect = true;

	post( "Attempting connection to server with address: %s", serverAddress.ToString() );

	if ( peerInterface->Connect( serverAddress.ToString( false ), serverAddress.GetPort(), 0, 0, 0 ) != RakNet::CONNECTION_ATTEMPT_STARTED ) {
	}
}

void MultiConnectionAudio::SetClientPort( long inlet, t_symbol* sym, long argc, t_atom* argv ) {
	if ( argc < 1 ) {
		error( "mac~::SetClientPort port" );
		return;
	}

	clientAddress.FromStringExplicitPort( clientAddress.ToString( false ), atom_getlong( argv ) );

	KillServer();
	Restart();
}

void MultiConnectionAudio::SetServerPort( long inlet, t_symbol* sym, long argc, t_atom* argv ) {
	if ( argc < 1 ) {
		error( "mac~::SetServerPort port" );
		return;
	}
	if ( serverAddress.FromStringExplicitPort( serverAddress.ToString( false ), atom_getlong( argv ) ) ) {
		post( "New server ip address is: %s", serverAddress.ToString() );
	}
}

void MultiConnectionAudio::SetIPAddress( long inlet, t_symbol* sym, long argc, t_atom* argv ) {
	if ( argc < 1 ) {
		error( "mac~::SetIPAddress ip " );
		return;
	}

	if ( serverAddress.FromStringExplicitPort( atom_getsym( argv )->s_name, serverAddress.GetPort() ) ) {
		post( "New server ip address is: %s", serverAddress.ToString() );
	}
}

void MultiConnectionAudio::Reconnect( long inlet ) {
	connectionState = CS_RECONNECTING;
}

void MultiConnectionAudio::Disconnect( long inlet ) {
	if ( peerInterface->IsActive() ) {
		KillServer();
	}
	connectionState = CS_DISCONNECTED;

	outlet_anything( outlet, gensym( "stop" ), 0, NULL );
}

void MultiConnectionAudio::Connect( long inlet ) {
	if ( !peerInterface->IsActive() ) {
		Restart();
	}
	/* Lol */
	connectionState = CS_RECONNECTING;

	outlet_anything( outlet, gensym( "start" ), 0, NULL );
}

void MultiConnectionAudio::Restart() {
	RakNet::SocketDescriptor sd( clientAddress.GetPort(), clientAddress.ToString( false ) );

	if ( peerInterface->Startup( 32, &sd, 1 ) != RakNet::RAKNET_STARTED ) {
		post( "Unable to start server." );
	} else {
		post( "Peer network started, address: %s", clientAddress.ToString() );

		post( "List of your IP addresses." );
		RakNet::SystemAddress ipList[ MAXIMUM_NUMBER_OF_INTERNAL_IDS ];
		RakNet::SocketLayer::GetMyIP( ipList );

		for ( unsigned int i = 0; i < MAXIMUM_NUMBER_OF_INTERNAL_IDS && ipList[ i ] != RakNet::UNASSIGNED_SYSTEM_ADDRESS; ++i ) {
			post( "%i. %s\n", i+1, ipList[ i ].ToString() );
		}
		peerInterface->SetMaximumIncomingConnections(32);
	}
}

void MultiConnectionAudio::KillServer() {
	for ( unsigned int i = 0; i < peerInterface->GetMaximumNumberOfPeers(); ++i ) {
		peerInterface->CloseConnection( peerInterface->GetSystemAddressFromIndex( i ), true, 0 );
	}
	peerInterface->Shutdown( 500, 0 );
}

void MultiConnectionAudio::SendAudioSample( Sample* sample ) {
	RakNet::BitStream message;
	message.Write( ( unsigned char ) ID_SEND_AUDIO );
	message.Write( ( ClientId ) clientId );
	message.Write( sample->channels );
	message.Write( sample->size );
	sample->DataToBitStream( message );

	peerInterface->Send( &message, HIGH_PRIORITY, RELIABLE_ORDERED, 0, serverAddress, false );
}

void MultiConnectionAudio::SendClientId( ClientId clientId, const RakNet::SystemAddress& systemAddress ) {
	/* Sends a client (specified by the system address ) their id */
	RakNet::BitStream message;
	message.Write( ( unsigned char ) ID_SET_CLIENT_NUMBER );
	message.Write( clientId );

	/* Sending the client Id also has us send a list of currently connected clients */
	if ( connectedClients.size() > 0 ) {
		message.Write( ( unsigned int ) connectedClients.size() );
		for ( unsigned int i = 0; i < connectedClients.size(); ++i ) {
			message.Write( connectedClients[ i ] );
		}
	} else {
		message.Write( ( unsigned int ) 0 );
	}

	peerInterface->Send( &message, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, false );
}

void MultiConnectionAudio::BroadcastNewClient( ClientId clientId, const RakNet::SystemAddress& systemAddress ) {
	/* Notify all others that a person has connected, except for the person who connected */
	RakNet::BitStream message;
	message.Write( ( unsigned char ) ID_NEW_CLIENT );
	message.Write( clientId );

	peerInterface->Send( &message, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, true );
}

void MultiConnectionAudio::BroadcastAudio( RakNet::Packet* packet, const RakNet::SystemAddress& systemAddress ) {
	RakNet::BitStream message;
	message.Write( ( unsigned char ) ID_RECIEVE_AUDIO );
	message.Write( ( char* ) packet->data + 1, packet->length - 1 );

	peerInterface->Send( &message, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, true );
}

void MultiConnectionAudio::RecieveAudio( RakNet::BitStream& message ) {
	if ( numOutputs == 0 || !audioEnabled ) {
		/* We don't care. */
		return;
	}

	Sample* sample;
	ClientId id;
	unsigned int channels;
	unsigned int size;

	/* Ignore the header */
	message.IgnoreBits( 8 );
	message.Read( id );
	message.Read( channels );
	message.Read( size );

	if ( clientId == id ) {
		/* Also don't care. */
		return;
	}
	/* Check if we have this client muted */
	if ( clientGains[ id ] <= 0 ) {
		return;
	}

	channels = max( channels, numOutputs );
	sample = new Sample( channels, size );

	sample->DataFromBitstream( message );

	boost::lock_guard< boost::recursive_mutex > guard( mutex );

	std::deque< Sample* >& sampleQueue = clientSamples[ id ];
	sampleQueue.push_back( sample );
}

void MultiConnectionAudio::OnNewClient( RakNet::BitStream& message ) {
	message.IgnoreBits( 8 );
	unsigned int id;
	message.Read( id );

	post( "New client has connected. %i ", id );

	if ( id == clientId ) {
		/* This should never happen. */
		return;
	}

	RegisterClient( id );
}

void MultiConnectionAudio::OnSetClientNumber( RakNet::BitStream& message ) {
	/* If we are connected remotely, here is where we set our client number and
	recieve any client numbers from the server */

	/* Ignore the header */
	message.IgnoreBits( 8 );

	/* Read in our client id */
	message.Read( clientId );

	unsigned int numClients;
	message.Read( numClients );

	post( "There are already %i clients connected.", numClients );

	if ( numClients > 0 ) {
		for ( unsigned int i = 0; i < numClients; ++i ) {
			unsigned int client;
			message.Read( client );

			RegisterClient( client );
		}
	}
}

void MultiConnectionAudio::SetAudioEnabled( long inlet, long val ) {
	audioEnabled = ( val != 0 ) ? true : false;

	if ( !audioEnabled ) {
		Sample* sample;

		boost::lock_guard< boost::recursive_mutex > lock( mutex );

		/* Ensure we delete our entire queues of sample */
		for ( auto it = clientSamples.begin(), end = clientSamples.end(); it != end; ++it ) {
			std::deque< Sample* >& sampleQueue = (*it).second;

			while ( !sampleQueue.empty() ) {
				sample = sampleQueue.front();
				delete sample;
				sampleQueue.pop_front();
			}

			sampleQueue.clear();
		}
	}
}

void MultiConnectionAudio::SetVoiceEnabled( long inlet, long val ) {
	voiceEnabled = ( val != 0 ) ? true : false;
}

void MultiConnectionAudio::RegisterClient( ClientId id ) {
	/* Create a queue for these client's samples */
	std::deque< Sample* >& deque = clientSamples[ id ];
	/* Attach them to our gains list */
	clientGains[ id ] = 1.0f;
	
	/* we have to do this from the main thread */
	clientsToRegister.push( id );
#if 0
	t_object* patcher, *self;
	object_obex_lookup( &m_ob, gensym( "#P" ), &patcher );
	object_obex_lookup( &m_ob, gensym( "#B" ), &self );

	//gainObjects.push_back( new GainObject( id, patcher, self ) );
#endif
	clock_fdelay( clock, 0.0 );
}

void MultiConnectionAudio::SetGain( long inlet, t_symbol* sym, long argc, t_atom* argv ) {
	if ( argc < 2 ) {
		return;
	}

	long id = atom_getlong( argv++ );
	long gainAmt = atom_getlong( argv );

	if ( clientGains.find( id ) != clientGains.end() ) {
		clientGains[ id ] = float ( gainAmt / 100.f );
	}
}



void MultiConnectionAudio::RegisterClientScheduledTask( void* context ) {
	MultiConnectionAudio* mca = ( MultiConnectionAudio* ) context;
	mca->FlushRegisteredClientsQueue();
}

void MultiConnectionAudio::FlushRegisteredClientsQueue() {
	ClientId id;

	while ( !clientsToRegister.empty() ) {
		id = clientsToRegister.front();

		t_object* patcher, *self;
		object_obex_lookup( &m_ob, gensym( "#P" ), &patcher );
		object_obex_lookup( &m_ob, gensym( "#B" ), &self );

		gainObjects.push_back( new GainObject( id, patcher, self ) );


		clientsToRegister.pop();
	}
}
