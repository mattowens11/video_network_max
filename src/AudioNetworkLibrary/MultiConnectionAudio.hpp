/**
 * \file	C:\Users\Ross\git\video_network_max\src\AudioNetworkLibrary\MultiConnectionAudio.hpp
 *
 * \brief	Declares the multi connection audio class.
 */
#ifndef __MULTICONNECTIONAUDIO_HPP_INCLUDED__
#define __MULTICONNECTIONAUDIO_HPP_INCLUDED__

#include <queue>
#include <map>
#include <boost/pool/object_pool.hpp>
#include <boost/timer.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include <RakPeerInterface.h>
#include <BitStream.h>
#include <maxcpp5.h>

typedef unsigned int ClientId;
typedef short SampleType;

class MultiConnectionAudio : public MspCpp5< MultiConnectionAudio > {
public:
	enum ConnectionState {
		/* Introductory status, if disconnected then perform will not process */
		CS_DISCONNECTED = 0,
		/* Attempting to reconnect, check each perform frame for reconnecting status */
		CS_RECONNECTING,
		/* Connected to a remote server, can send and receive audio packets */
		CS_CONNECTED
	};

	/** Syntax mca~ 2 2 */
	MultiConnectionAudio( t_symbol* sym, long argc, t_atom* argv );

	/** */
	~MultiConnectionAudio( void );

	/* Sets the client's port */
	void SetClientPort( long inlet, t_symbol* sym, long argc, t_atom* argv );

	/* Sets the server's port */
	void SetServerPort( long inlet, t_symbol* sym, long argc, t_atom* argv );

	/* Sets the IP address for our connection, if running a server, we will connect to ourself */
	void SetIPAddress( long inlet, t_symbol* sym, long argc, t_atom* argv );

	/* Adjusts the gain for the specified client */
	void SetGain( long inlet, t_symbol* sym, long argc, t_atom* argv );

	/* Mutes the targeted client */
	void Mute( long inlet, t_symbol* sym, long argc, t_atom* argv );

	/* Perform, does the heavy lifting for Max */
	void Perform( int vs, t_sample** inputs, t_sample** outputs );

	/* Forces a reconnect */
	void Reconnect( long inlet );

	/* Forces a disconnect */
	void Disconnect( long inlet );

	/* Connects us to the server specified in serverAddress */
	void Connect( long inlet );

	void SetAudioEnabled( long inlet, long val );
	void SetVoiceEnabled( long inlet, long val );

	static void RegisterClientScheduledTask( void* );
	void FlushRegisteredClientsQueue();
				
private:
	/* Number of input channels, if 0 we don't process input */
	unsigned int numInputs;
	/* Number of output channels, if 0 we don't process output from the server */
	unsigned int numOutputs;

	/* Our client Id, we won't do much if we receive messages where we are the client in question */
	ClientId clientId;
	/* If acting as a server, we will use this to generate new client ids */
	static ClientId nextClientId;
				
	/* Our current connection state */
	ConnectionState connectionState;

	/* RakNet specifics */
	/* Peer interface for server-client relationship */
	RakNet::RakPeerInterface* peerInterface;
	/* The server address we are using to connect to */
	RakNet::SystemAddress serverAddress;
	/* The client address (generally just nothing but whatever) */
	RakNet::SystemAddress clientAddress;

	/* time values all measured in seconds */
	float timeUntilNextReconnect;
	float maxReconnectTime;
	float connectionAttempts;
	float maxConnectionAttempts;
	bool attemptingReconnect;

	/* Measures time */
	boost::timer reconnectionTimer;
	boost::recursive_mutex mutex;

	static const SampleType SAMPLE_MAX;
	static const float SAMPLE_MAX_1;

	boost::shared_ptr< boost::thread > serverThread;
	bool serverIsRunning;

	/* Simple interface for a sample, which is used to handle audio data */
	struct Sample {
		/* The number of channels (from the inputs, if < #outputs, just ignore some) */
		unsigned int channels;
		/* The size of the sample within each channel */
		unsigned int size;
		/* Actual sample data, samples range in size for each channel, so we need a 2D array */
		SampleType** sampleData;

		Sample() : channels(0), size(0), sampleData(0) {
		}

		/* Constructor */
		Sample( unsigned int channels, unsigned int size ) : channels(channels), size(size), sampleData(0) {
			Initialize();
		}

		~Sample() {
			Clear();
		}

		void Initialize() {
			sampleData = new SampleType*[channels];
			for ( unsigned int i = 0; i < channels; ++i ) {
				sampleData[ i ] = new SampleType[ size ];
				memset( sampleData[ i ], 0, sizeof( SampleType ) * size );
			}
		}

		void Clear() {
			for ( int i = channels - 1; i >= 0; --i ) {
				delete [] sampleData[ i ];
				sampleData[ i ] = NULL;
			}
			delete [] sampleData;
			sampleData = NULL;
		}

		void ReadIn( const t_sample** inputs, int maxChannels) {
			if ( !sampleData ) {
				Initialize();
			}
			if ( maxChannels > channels ) {
				maxChannels = channels;
			}

			for ( int channel = 0; channel < maxChannels; ++channel ) {
				if ( inputs [ channel ] && sampleData[ channel ]) {
					for ( int i = 0; i < size; ++i ) {
						sampleData[ channel ][ i ] = inputs[ channel ][ i ] * SAMPLE_MAX;
					}
				}
			}
		}

		void WriteOut( t_sample** outputs, int maxChannels, float gain = 1.0f ) {
			t_sample sample;
			if ( !sampleData ) {
				/* Don't care, no data */
				return;
			}

			if ( maxChannels > channels ) {
				maxChannels = channels;
			}
			for ( int channel = 0; channel < channels; ++channel ) {
				if ( sampleData [ channel ] && outputs[ channel ] ) {
					for ( int i = 0; i < size; ++i ) {
						sample = ( ( float ) sampleData[ channel ][ i ] ) * SAMPLE_MAX_1 * gain;
						outputs[ channel ][ i ] += sample;

						outputs[ channel ][ i ] = Clamp( outputs[ channel ][ i ], -1.0f, 1.0f );
					}
				}
			}
		}

		void DataToBitStream( RakNet::BitStream& bitStream ) {
			/* Assume the bitstream just wants the data */
			for ( unsigned int channel = 0; channel < channels; ++channel ) {
					bitStream.Write( ( char* ) &sampleData[ channel ][ 0 ], sizeof( SampleType ) * size );
			}
		}

		void DataFromBitstream( RakNet::BitStream& bitStream ) {
			/* Assume the channels and size have been read in already */
			for ( unsigned int channel = 0; channel < channels; ++channel ) {
					bitStream.Read( ( char* ) &sampleData[ channel ][ 0 ], sizeof( SampleType ) * size );
			}
		}

	private:
		t_sample Clamp( const t_sample in, const t_sample low, const t_sample high ) {
			if ( in <= low ) 
				return low;
			if ( in >= high )
				return high;
			return in;
		}
	};

	/* Simple encapsulation of a gain object */
	struct GainObject {
		t_object* gain;
		t_object* prepend;
		ClientId id;

		GainObject( ClientId id, t_object* parentPatcher, t_object* mcaObject ) : id( id ) {
			gain = newobject_sprintf( parentPatcher, "@maxclass gain~ @patching_rect 100. 100. 20. 100.");
			prepend =  newobject_sprintf( parentPatcher, "@maxclass newobj @hidden 1 @text \"prepend SetGain %d\"", id );

			object_method( gain, gensym( "set" ), 100 );

			t_atom connect[5], rv;

			atom_setobj( &connect[0], gain );
			atom_setlong( &connect[1], 1 );
			atom_setobj( &connect[2], prepend );
			atom_setlong( &connect[3], 0 );
			atom_setlong( &connect[4], -1 );
			object_method_typed( parentPatcher, gensym( "connect" ), 5, connect, &rv );

			atom_setobj( &connect[0], prepend );
			atom_setlong( &connect[1], 0 );
			atom_setobj( &connect[2], mcaObject );
			atom_setlong( &connect[3], 0 );
			atom_setlong( &connect[4], -1 );
			object_method_typed( parentPatcher, gensym( "connect" ), 5, connect, &rv );
		}

		~GainObject() {
			freeobject( gain );
			freeobject( prepend );
		}
	};

	void InitializeRakNet( void );
	void ShutdownRaknet( void );
	void UpdateReconnectionStatus( void );

	void ServerThread( void );
	void OnConnectionAccepted( RakNet::BitStream& message );
	void OnConnectionFailed( RakNet::BitStream& message );
	void AttemptToConnect( void );
	void Restart( void );
	void KillServer( void );
	void SendAudioSample( Sample* sample );

	void SendClientId( ClientId clientId, const RakNet::SystemAddress& systemAddress );
	void BroadcastNewClient( ClientId clientId, const RakNet::SystemAddress& systemAddress );
	void BroadcastAudio( RakNet::Packet* message, const RakNet::SystemAddress& systemAddress );
	void RecieveAudio( RakNet::BitStream& message );
	void OnNewClient( RakNet::BitStream& message );
	void OnSetClientNumber( RakNet::BitStream& message );

	void RegisterClient( ClientId id );

	/* Buffering utilities, client-side */
	std::map< ClientId, std::deque< Sample* > > clientSamples;
	std::map< ClientId, float > clientGains;

	/* Server-side, connected clients */
	std::vector< ClientId > connectedClients;
	std::queue< ClientId > clientsToRegister;

	/* Client gains */
	std::vector< GainObject* > gainObjects;

	void* outlet, *clock;

	bool audioEnabled;
	bool voiceEnabled;

};

#endif /* __MULTICONNECTIONAUDIO_HPP_INCLUDED__ */
