/**
 * \file	MaxMSPIntegration.cpp
 *
 * \brief	Contains necessary functions for integration into MaxMSP
 */
#include "MultiConnectionAudio.hpp"

/* Library links for MSVC */
#ifdef _MSC_VER
#	if _MSC_VER >= 1400  /* This pragma was introduced in VS2005 */
#		pragma comment(lib, "MaxAudio")
#		pragma comment(lib, "MaxAPI")
#		pragma comment(lib, "maxcrt")
#		ifdef _DEBUG
#			pragma comment(lib, "RakNet_DLL_Debug_Win32")
#		else
#			pragma comment(lib, "RakNet_DLL_Release_Win32")
#		endif
#	endif
#endif

/**
 * \fn	extern "C" int main(void)
 *
 * \brief	Main entry point for the Max MSP program, is exposed through the DLL
 *
 * \return	Exit-code for the process - 0 for success, else an error code.
 */
extern "C" int main(void) 
{
	MultiConnectionAudio::makeMaxClass("mca~");

#define MAX_CLASS MultiConnectionAudio
	REGISTER_METHOD( MAX_CLASS, Connect );
	REGISTER_METHOD( MAX_CLASS, Disconnect );
	REGISTER_METHOD( MAX_CLASS, Reconnect );

	REGISTER_METHOD_GIMME( MAX_CLASS, SetServerPort );
	REGISTER_METHOD_GIMME( MAX_CLASS, SetClientPort );
	REGISTER_METHOD_GIMME( MAX_CLASS, SetIPAddress );
	REGISTER_METHOD_GIMME( MAX_CLASS, SetGain );

	REGISTER_METHOD_LONG( MAX_CLASS, SetAudioEnabled );
	REGISTER_METHOD_LONG( MAX_CLASS, SetVoiceEnabled );


	return 0;
}
