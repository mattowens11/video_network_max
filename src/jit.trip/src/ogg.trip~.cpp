#include "maxcpp5.h"
#include "../include/max.trip.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <memory.h>
#include <iostream>

#include "vorbis/vorbisenc.h"

// TODO: should check sample rates between the two systems and pop some major warnings if they don't match!
// TODO: should there be 2 messages: disconnect and disconnectall? one to cut outgoing connections, one to cut all?

// number of samples / speex size - # of samps = MSP vector * channels
//#define FRAMES_PER_BUFFER 128 // 64 per vector x 2 channels //x triple overlap
//#define RECEIVE_BUFFER_SIZE 1536 //8192

//#define TX_REDUNDANCY 3 // how many times each chunk of data will be sent

#define DEFAULT_PORT 8001

#define ENCODE_QUALITY 0.4

//#define InChannels 2   // how many channels of audio to pack up and send
//#define OutChannels 2  // how many channels we're prepared to receive and put out

#define RECONNECT_TIME 2000 // how often we try to reconnect if the connection drops

// define sample type. 16 bits sound is supclientPorted at the moment.
typedef short SAMPLE;
#define SAMPLE_MAX 32768

class OggTrip : public MspCpp5<OggTrip> {
private:
    RakNet::RakPeerInterface *rakPeer;
    RakNet::Packet *rakPacket;
    RakNet::BitStream *myBitStream;
    char InChannels, OutChannels;
    bool TX_ACTIVE, RX_READY; // turn transmission on and off
    unsigned char sampleSize, TX_REDUNDANCY;
    SAMPLE* sendBuf;        // buffer to queue up data for TX
    int sendBufSize, rcvBufSize, rcvBufChannelSize, rcvBufWindowCount;    //  how big the send/rcv buffer is, how much space 1 channel uses in the rcvBuf, how many windows the receive buffer should cover
    SAMPLE* rcvBuf;
    int rcvWritePtr, rcvReadPtr, sendWritePtr, sendReadPtr;
    bool isConnected;
    int samples, inPacketID, outPacketID, inputWindowSize;  //inputWindowSize - how much data we receive in a packet
    char *remoteIPAddress;
    unsigned short   clientPort, serverPort;    // the port we try to connect to (client) and the port we listen on (server)
    RakNet::TimeMS  reconnectTimer;
    bool failedMessageFlag;
    int windowsToBuffer, windowsSinceTx, windowSizeTX;    // how many windows of data to buffer between transmissions, how many we've compiled since sending, how big the sample vector is at each dsp update
    PacketPriority packetPriority;    // the RakNet transmission priority code
    bool    reliableTX;     // send using RakNet reliable option?
    
    // vorbis data structures
    vorbis_info vi, viRx;
    vorbis_comment vc, vcRx;
    vorbis_dsp_state vDsp, vDspRx;
    vorbis_block vBlock, vBlockRx;
    
    ogg_packet oggHeaders[3];
    ogg_packet encodedData;
    
//    ogg_stream_state os; /* take physical pages, weld into a logical
//                          stream of packets */
//    ogg_page         headerPage, og; /* one Ogg bitstream page.  Vorbis packets are inside */
    
    void InitVorbis() {
        // turn things on
        vorbis_info_init(&vi);
        vorbis_info_init(&viRx);
        // setup the header comment and add a perfunctory comment
        vorbis_comment_init(&vc);
        vorbis_comment_init(&vcRx);
        vorbis_comment_add_tag(&vc, "ENCODER", "ogg.trip~");
    }
    
    void InitVorbisEncoder() {
        int ret;
        
//        InitVorbis();
        
        // init for vbr encoding. channels, rate, nominal quality (from -0.1 to 1.0)
        ret = vorbis_encode_init_vbr(&vi, InChannels, 44100, ENCODE_QUALITY);
        if (ret)
            post("error: ogg.trip~ vorbis init failed");
        
        // setup analysis state and aux encoding storage
        ret = vorbis_analysis_init(&vDsp, &vi); // all should return 0 if things are working well.
        ret = vorbis_block_init(&vDsp, &vBlock);
        
        /* set up our packet->stream encoder */
        /* pick a random serial number; that way we can more likely build
         chained streams just by concatenation */
//        ogg_stream_init(&os,rand());
        
        vorbis_analysis_headerout(&vDsp, &vc, &oggHeaders[0], &oggHeaders[1], &oggHeaders[2]);
//        for (int i = 0; i < 3; i++) {
//            ogg_stream_packetin(&os, &oggHeaders[i]);
//            post("%i %i", i, oggHeaders[i].bytes);
//        }
//        while(true){    // we'll get 2 pages out based on the 3 headers we've just fed in, one of 28 bytes, one of 45 bytes
//            int result=ogg_stream_flush(&os,&og);
//            if(result==0)break;
////            post("%i %i %i %i", og.header, og.header_len, og.body, og.body_len);
////            fwrite(og.body,1,og.body_len,stdout);
//        }
        for (int i = 0; i < 3; i++)
            SendOGGPacket(&oggHeaders[i], (MAXTRIP_PACKET_ID)(ID_OGG_H1+i));
    }
    void DestroyVorbis() {
//        ogg_stream_clear(&os);
        vorbis_block_clear(&vBlock);
//        vorbis_dsp_clear(&vDspRx);
        vorbis_dsp_clear(&vDsp);
        vorbis_comment_clear(&vc);
//        vorbis_info_clear(&viRx);
        vorbis_info_clear(&vi);
    }
    void EncodeData(int vs, t_sample ** inputs) {
        int i, j;
        float **buffer=vorbis_analysis_buffer(&vDsp,vs*InChannels);  // 2nd arg is number of samples
        
        /* uninterleave samples */
        for(i=0; i<vs; i++)
            for (j = 0; j < InChannels; j++)
                buffer[j][i] = (float)inputs[j][i];        // could a memcpy work here just as well?
        
        vorbis_analysis_wrote(&vDsp, i);  // tell analysis how many samples we wrote in
    }
    void createRakPeer() {
        rakPeer = RakNet::RakPeerInterface::GetInstance();
        
        RakNet::SocketDescriptor socketDescriptor(serverPort,0);   //atoi(clientPort)
        
        bool started = (rakPeer->Startup(32, &socketDescriptor, 1)==RakNet::RAKNET_STARTED); // number of allowed connections
        if (!started)
            ; //post("ogg.trip~: failed to startup network layer"); // this gets triggered when an object is recreated in the patcher... hm.
        else {
            rakPeer->SetMaximumIncomingConnections(32);
        }
    }
    void checkTXBuffer(int vs) {    // compare the TX buffer space to the audio sample vector size. update if necessary
        if (vs != windowSizeTX) {   // we need to reallocate?
            int newSize = vs*InChannels*windowsToBuffer;   // buffer size
            SAMPLE* temp = new SAMPLE[newSize]; // new buffer
            if (sendBufSize > 0 && sendBuf)
                memcpy(temp, sendBuf, (sendBufSize > newSize ? newSize : sendBufSize) * sampleSize); // copy existing data over
            if (sendBuf != NULL)    // remove old buffer?
                delete sendBuf;
            sendBuf = temp;     // update persistent variables
            windowSizeTX = vs;
            sendBufSize = newSize;
        }
    }
    void checkRXBuffer(int vs) {
        int newSize = OutChannels * rcvBufWindowCount * vs; //inputWindowSize * 2;
        if (newSize > rcvBufSize) {   // we need to reallocate?
//            int newSize = vs*InChannels*windowsToBuffer;   // buffer size
            SAMPLE* temp = new SAMPLE[newSize]; // new buffer
            for (int i = 0; i < newSize; i++)
                temp[i] = 0;
            if (rcvBufSize > 0 && rcvBuf)
                memcpy(temp, rcvBuf, (rcvBufSize > newSize ? newSize : rcvBufSize) * sampleSize); // copy existing data over
            if (rcvBuf != NULL)    // remove old buffer?
                delete rcvBuf;
            rcvBuf = temp;     // update persistent variables
            rcvBufSize = newSize;
            if (OutChannels > 0)
                rcvBufChannelSize = newSize / OutChannels;
            else rcvBufChannelSize = newSize;
            rcvReadPtr = newSize * 0.5; // start the read pointer 1/2 way around the ring buffer
            rcvWritePtr = 0;
        }
    }
public:
	OggTrip(t_symbol * sym, long ac, t_atom * av) : InChannels(2), OutChannels(2), TX_ACTIVE(true), RX_READY(false), TX_REDUNDANCY(3), rcvBufChannelSize(0), rcvBufWindowCount(16), rcvWritePtr(0), rcvReadPtr(0), sendWritePtr(0), sendReadPtr(0), isConnected(false), inPacketID(0), outPacketID(0), remoteIPAddress(NULL), failedMessageFlag(false), windowsSinceTx(0), windowSizeTX(0), packetPriority(IMMEDIATE_PRIORITY), reliableTX(true) { 
        
//        for (int i = 0; i < ac; i++) {
//            Atom* ap = av+i;
//            switch (atom_gettype(ap)) {
//                case A_LONG:
//                    post("%i=%ld ", i, atom_getlong(ap));
//                    break;
//                case A_FLOAT:
//                    post("%i=%f ", i, atom_getfloat(ap));
//                    break;
//                case A_SYM:
//                    post("%s ", atom_getsym(ap)->s_name);
//                    break;
//                default:
//                    post("%ld: unknown atom type (%ld)", i+1, atom_gettype(ap));
//                    break;
//            }
//        }
        windowsToBuffer = TX_REDUNDANCY; // * 1 //
        
        serverPort = DEFAULT_PORT;
        remoteIPAddress = "127.0.0.1"; //NULL;
        clientPort = DEFAULT_PORT;
        if (ac > 0) {
            bool argError = false;
            if (atom_gettype(av) == A_LONG)
                InChannels = atom_getlong(av);
            else argError = true;
            if (ac > 1) {
                if (atom_gettype(++av) == A_LONG)
                    OutChannels = atom_getlong(av);
                else argError = true;
                if (ac > 2) {
                    if (atom_gettype(++av) == A_LONG)
                        serverPort = atom_getlong(av);
                    else argError = true;
                
                    if (ac > 3) {
                        if (atom_gettype(++av) == A_SYM)
                            remoteIPAddress = atom_getsym(av)->s_name;
                        else if (atom_gettype(av) == A_LONG)
                            clientPort = atom_getlong(av);
                        else argError = true;
                        
                        if (ac > 4) {
                            if (atom_gettype(++av) == A_LONG)
                                clientPort = atom_getlong(av);
                            else argError = true;
                        }
                    }
                }
            }
            if (argError)
                post("ogg.trip~: argument format error, should be <Local Port> <IP> <Remote Port>");
        }
        
        if (InChannels < 1) {
            TX_ACTIVE = false;
            InChannels = 1;
        }
        OutChannels = (OutChannels < 0 ? 0 : OutChannels);  // no maximum set at the moment. Probably should have some sort of protection!
		setupIO(&OggTrip::perform, InChannels, OutChannels); 
        
        rakPeer = NULL;
        createRakPeer();
        
        sampleSize = sizeof(SAMPLE);
        myBitStream = new RakNet::BitStream();
        sendBufSize = 0; //FRAMES_PER_BUFFER*windowsToBuffer;
        sendBuf = NULL; //new SAMPLE[sendBufSize];
        rcvBufSize = 0;
        rcvBuf = NULL; //new SAMPLE[RECEIVE_BUFFER_SIZE*OutChannels];
        
        reconnectTimer = RakNet::GetTimeMS();
		post("ogg.trip~ created by Benjamin Smith"); 
	}
	~OggTrip() { 
        disconnect(0);
    	rakPeer->Shutdown(0);
        RakNet::RakPeerInterface::DestroyInstance(rakPeer);
        
        DestroyVorbis();
        
        delete myBitStream;
        delete sendBuf;
        delete rcvBuf;
    }	
	
	// methods:
    void connect(long inlet) {
        if (!rakPeer)
            createRakPeer();
        RX_READY = false;   // starting a new connection, we need the vorbis header info before we can init the decoder
        
//        post("connecting to %s:%d", remoteIPAddress, clientPort);
        if (remoteIPAddress != NULL) {
            bool b = rakPeer->Connect(remoteIPAddress, clientPort, 0, 0, 0)==RakNet::CONNECTION_ATTEMPT_STARTED;
            if (b==false)
                post("ogg.trip~: net connection failed");
            else {
                post("ogg.trip~: connected to %s:%i", remoteIPAddress, clientPort);
                rcvReadPtr = rcvWritePtr;
            }
        }
    }
    void connect(long inlet, t_symbol * ip, long p) {
        remoteIPAddress = ip->s_name;
        clientPort = p;
        connect(inlet);
    }
    void disconnect(long inlet)
    {
        for (int i = 0; i < rakPeer->GetMaximumNumberOfPeers(); i++) {
            rakPeer->CloseConnection(rakPeer->GetSystemAddressFromIndex(i),true,0);
        }
        if (isConnected)
            post("ogg.trip~ disconnected");
        isConnected=false;
    }
    void ip(long inlet, t_symbol * s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_SYM) {
            if (isConnected)
                disconnect(inlet);
            remoteIPAddress = atom_getsym(av)->s_name;
        }
    }
    void port(long inlet, t_symbol * s, long ac, t_atom * av) {
        short p = -1;
        if (ac > 0 && atom_gettype(av) == A_LONG)
            p = atom_getlong(av);
        if (clientPort != p && isConnected)
            disconnect(inlet);
        clientPort = p;
    }
    void rxbuffer(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            rcvBufWindowCount = atom_getlong(av);  // upper limit is not set, so the user has to be somewhat smart!
            rcvBufWindowCount = (rcvBufWindowCount < 2 ? 2 : rcvBufWindowCount);   // this will cause the rx buffer to be reallocated
        }
    }
    void buffer(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            char buf = atom_getlong(av);
            buf = (buf < 1 ? 1 : buf);
            windowsToBuffer = buf * TX_REDUNDANCY;  // upper limit is 255, so the user has to be somewhat smart!
            windowSizeTX = 0;   // this will cause the send buffer to be reallocated in checkTXBuffer
        }
    }
    void redundancy(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            windowsToBuffer /= TX_REDUNDANCY;
            TX_REDUNDANCY = atom_getlong(av);
            TX_REDUNDANCY = (TX_REDUNDANCY < 1 ? 1 : (TX_REDUNDANCY > 8 ? 8 : TX_REDUNDANCY));
            windowsToBuffer = (windowsToBuffer < 1 ? 1 : windowsToBuffer);  // sanity check
            windowsToBuffer *= TX_REDUNDANCY;
            windowSizeTX = 0;   // cause the send buffer to be reallocated
        }
    }
    void transmit(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            TX_ACTIVE = (atom_getlong(av) != 0);
        }
    }
    void priority(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            if (atom_getlong(av) > 0)
                packetPriority = IMMEDIATE_PRIORITY;
            else
                packetPriority = HIGH_PRIORITY;
        }
    }
    void reliable(long inlet, t_symbol *s, long ac, t_atom * av) {
        if (ac > 0 && atom_gettype(av) == A_LONG) {
            reliableTX = atom_getlong(av) != 0;
        }
    }

    void PrintParameters() {
        if (isConnected)
            post("ogg.trip~: listening on port %d, connected to remote host at %s:%d", serverPort, remoteIPAddress, clientPort);
        else
            post("ogg.trip~: listening on port %d, attempting to connect to remote host at %s:%d", serverPort, remoteIPAddress, clientPort);
    }
    
	void bang(long inlet) { 
		PrintParameters(); 
	}
	
	// gets called when the dsp chain is modified
	void dsp() { 
        DestroyVorbis();
        InitVorbis();
        
        if (TX_ACTIVE)
            InitVorbisEncoder();
        
        rcvWritePtr = 0;
        rcvReadPtr = rcvBufSize * 0.5;
        outPacketID = 0;
    }
    void SendOGGPacket(ogg_packet* packet, MAXTRIP_PACKET_ID id = ID_OGG_AUDIO) {
        myBitStream->Reset();
        myBitStream->Write((unsigned char)id);
        myBitStream->Write((int)sizeof(ogg_packet));
        myBitStream->Write((char*)packet, sizeof(ogg_packet));
        myBitStream->Write((int)packet->bytes);
        myBitStream->Write((char*)packet->packet, packet->bytes);
        
        std::cout << "tx bytes: " << packet->bytes << std::endl;
        
        // broadcast the data, in a sequenced manner, to all connected peers!
        rakPeer->Send(myBitStream, packetPriority, (reliableTX ? RELIABLE_SEQUENCED : UNRELIABLE_SEQUENCED), 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
    }
	
	// signal processing example: inverts sign of inputs
	void perform(int vs, t_sample ** inputs, t_sample ** outputs) {
//        short i;
        
        samples = vs*2;
        
//        if (!isConnected) {         // try to reconnect every few seconds
//            if (RakNet::GetTimeMS() > reconnectTimer) {
//                connect(0);
//                reconnectTimer = RakNet::GetTimeMS() + RECONNECT_TIME;
//            }
//        }

        if (TX_ACTIVE) {
//            checkTXBuffer(vs);  // check the window size against the size of our buffer and reallocate if needed
            
            EncodeData(vs, inputs);
            
            if (vorbis_analysis_blockout(&vDsp, &vBlock) == 1) {    // there's new data!
                
                /* analysis, assume we want to use bitrate management */
                vorbis_analysis(&vBlock, NULL);
                vorbis_bitrate_addblock(&vBlock);
                
                while (vorbis_bitrate_flushpacket(&vDsp,&encodedData)) {
                    SendOGGPacket(&encodedData);
                }
            }
            
        }
        
        if (!TX_ACTIVE) // we're a receive object
            checkRXBuffer(vs);  // make sure we have the appropriate space to receive incoming audio data

//        if (rakPeer) {
            rakPacket = rakPeer->Receive();
            while (rakPacket)
            {
                switch (rakPacket->data[0]) {
                    case ID_NEW_INCOMING_CONNECTION:
                    case ID_CONNECTION_REQUEST_ACCEPTED:
                        post("ID_CONNECTION_REQUEST_ACCEPTED from %s", rakPacket->systemAddress.ToString());
                        // if transmitting then send OGG headers
                        isConnected=true;
                        rcvReadPtr = rcvBufSize * 0.5;
                        failedMessageFlag = false;  // reset so that we can announce the drop again
//                        for (int i = 0; i < 3; i++)
//                            SendOGGPacket(&oggHeaders[i], (MAXTRIP_PACKET_ID)(ID_OGG_H1 + i));
                        break;
                        // print out errors
                    case ID_CONNECTION_ATTEMPT_FAILED:
                        if (!failedMessageFlag)
                            post("ogg.trip~ error: connection attempt failed to %s", rakPacket->systemAddress.ToString());
                        isConnected=false;
                        failedMessageFlag = true;   // set the flag so we don't fill the log with messages
                        break;
                    case ID_ALREADY_CONNECTED:
                        post("Client Error: ID_ALREADY_CONNECTED\n");
                        break;
                    case ID_CONNECTION_BANNED:
                        post("Client Error: ID_CONNECTION_BANNED\n");
                        break;
                    case ID_INVALID_PASSWORD:
                        post("Client Error: ID_INVALID_PASSWORD\n");
                        break;
                    case ID_INCOMPATIBLE_PROTOCOL_VERSION:
                        post("Client Error: ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
                        break;
                    case ID_NO_FREE_INCOMING_CONNECTIONS:
                        post("Client Error: ID_NO_FREE_INCOMING_CONNECTIONS\n");
                        isConnected=false;
                        break;
                    case ID_DISCONNECTION_NOTIFICATION:
                        post("ogg.trip~: %s disconnected.", rakPacket->systemAddress.ToString());
                        isConnected=false;
                        failedMessageFlag = false;
                        break;
                    case ID_CONNECTION_LOST:
                        post("ogg.trip~: connection to %s lost.", rakPacket->systemAddress.ToString());
                        isConnected=false;
                        failedMessageFlag = false;
                        break;
//                    case ID_AUDIO:
//                        HandleAudioPacket(rakPacket);
//                        break;
                    case ID_OGG_H1:
                    case ID_OGG_H2:
                    case ID_OGG_H3:
                        HandleOGGHeader(rakPacket);
                        break;
                    case ID_OGG_AUDIO:
//                        post("got OGG audio");
                        HandleOGGPacket(rakPacket);
                        break;
                    default:
                        break;
                }
                
                rakPeer->DeallocatePacket(rakPacket);
                rakPacket = rakPeer->Receive();
            }
//        }

        if (OutChannels && !TX_ACTIVE) {
            float **pcm;
            if((samples = vorbis_synthesis_pcmout(&vDspRx,&pcm)) >= vs) {
                int channels = (vi.channels < OutChannels ? vi.channels : OutChannels);
                /* convert floats to 16 bit signed ints (host order) and
                 interleave */
                for(int i=0; i<channels; i++) {
                    t_sample * out = outputs[i];
                    float * mono = pcm[i];
                    for(int j=0;j<vs;j++){
                        out[j] = (t_sample)mono[j];
                    }
                }
                vorbis_synthesis_read(&vDspRx, vs);
            } else {
                if (isConnected)    // don't bother saying anything if we're not connected
                    post("ogg.trip~: buffer underrun"); // should set a flag so we don't blast the message window with this.
                for (int channel = 0; channel < OutChannels; channel++) {
                    t_sample * out = outputs[channel];
                    for (int i=0; i<vs; i++) {
                        out[i] = 0;
                    }
                }
            }
        }
	}
    
    void HandleOGGHeader(RakNet::Packet* headerPacket) {
        unsigned char id;
        int opSize, bytes;
        
        myBitStream->Reset();
        myBitStream->Write((char*)headerPacket->data, headerPacket->length);
        myBitStream->Read(id);   // ignore header byte
        myBitStream->Read(opSize);
        myBitStream->Read((char*)&encodedData, opSize);
        
        myBitStream->Read(bytes);
        encodedData.bytes = bytes;
//        delete encodedData.packet;
        encodedData.packet = (unsigned char*)malloc(bytes);
        myBitStream->Read((char*)encodedData.packet, bytes);
        
        post("%i %i", opSize, bytes);
        
//        switch (id) {
//            case ID_OGG_H1:
//                if (vorbis_synthesis_headerin(&viRx, &vcRx, &oggHeaders[0]) < 0)
//                    post("ogg.trip~ error receiving stream header packets");
//                break;
//            case ID_OGG_H2:
//                if (vorbis_synthesis_headerin(&viRx, &vcRx, &oggHeaders[1]) < 0)
//                    post("ogg.trip~ error receiving stream header packets");
//                break;
//            case ID_OGG_H3:
//                if (vorbis_synthesis_headerin(&viRx, &vcRx, &oggHeaders[2]) < 0)
//                    post("ogg.trip~ error receiving stream header packets");
//                break;
//        }
        if (vorbis_synthesis_headerin(&viRx, &vcRx, &encodedData) < 0)
            post("ogg.trip~ error receiving stream header packets");
        if (id == ID_OGG_H3) {
            if (vorbis_synthesis_init(&vDspRx, &viRx) == 0) {
                vorbis_block_init(&vDspRx, &vBlockRx);
                RX_READY = true;    // we're ready to go!
                post("ogg.trip~ receive stream initialized.");
            }
        }

    }
    
    void HandleOGGPacket(RakNet::Packet* audioPacket) {
        int opSize, bytes;
        
        if (OutChannels < 1 || !RX_READY)        // we're not outputting audio or this stream isn't setup yet? just ignore then
            return;
        
        myBitStream->Reset();
        myBitStream->Write((char*)audioPacket->data, audioPacket->length);
        myBitStream->IgnoreBits(8);   // ignore header byte
        myBitStream->Read(opSize);
        myBitStream->Read((char*)&encodedData, opSize);

        myBitStream->Read(bytes);
        encodedData.bytes = bytes;
//        delete encodedData.packet;        // what do we do with old packets? can we keep reusing one or will there be a memory leak? does vorbis_synthesis clear out the packet?
        std::cout << "rx bytes: " << bytes << std::endl;
        if (bytes > 0) {
            encodedData.packet = (unsigned char*)malloc(bytes);
            myBitStream->Read((char*)encodedData.packet, bytes);
        }
        
        if (vorbis_synthesis(&vBlockRx, &encodedData)==0)
            vorbis_synthesis_blockin(&vDspRx, &vBlockRx);
        // and that's it, for now.
    }
    
    void HandleAudioPacket(RakNet::Packet* audioPacket) {
//        post("got a packet");
        // pull the data out of the packet
        int inID, packetSize, winSize, writePtr, temp;
        char winBuffered, incomingChan;
        
        if (OutChannels < 1)        // we're not outputting audio? just ignore then
            return;

        myBitStream->Reset();
        myBitStream->Write((char*)audioPacket->data, audioPacket->length);
        myBitStream->IgnoreBits(8);   // ignore header byte
        myBitStream->Read(inID);        // ID of this audio sample vector from the remote host
        myBitStream->Read(winSize);     // the remote host's audio sample vector size
        myBitStream->Read(winBuffered); // how many windows of samples of winSize are in this packet
        myBitStream->Read(incomingChan);    // how many channels of audio data the remote host is sending. we may not want all of them
        inputWindowSize = winSize*winBuffered;
        myBitStream->Read(packetSize);
        
        // calculate the write point based on the new audio packet ID
        if (inID < inPacketID) inPacketID -= INT32_MAX;        
        rcvWritePtr = (rcvWritePtr + (inID - inPacketID) * winSize) % rcvBufChannelSize;
        writePtr = rcvWritePtr;
        inPacketID = inID;
//        if (inID - inPacketID > winBuffered*2)    // we've fallen too far behind!
//            rcvReadPtr = rcvWritePtr;
        
        temp = (rcvReadPtr < winSize ? rcvReadPtr + rcvBufChannelSize : rcvReadPtr);
        for (int i = 0; i < winBuffered; i++) // read out the winBuffered chunks of data from the packet
        {
            for (int j = 0; j < incomingChan; j++)     // read out each channel's data
                if (j < OutChannels)    // do we want this channel's data?
                    myBitStream->Read((char*)(rcvBuf+writePtr+rcvBufChannelSize*j), winSize*sampleSize);
            // detect overrun - when the write pointer would pass over the read pointer, causing an audio glitch
            if (writePtr < temp && writePtr + winSize > temp)   // don't let the writer pass over the read head, just ditch data for the moment
                break;
            writePtr = (writePtr + winSize) % rcvBufChannelSize;
        }
//                post("%i %i %i chanSize:%i bufSize:%i ID:%d", 0, rcvReadPtr, rcvWritePtr, rcvBufChannelSize, rcvBufSize, inPacketID);

    }
};

extern "C" int main(void) {
	// create a class with the given name:
	OggTrip::makeMaxClass("ogg.trip~");
//	REGISTER_METHOD(OggTrip, bang);
//	REGISTER_METHOD_GIMME(OggTrip, test);
    REGISTER_METHOD(OggTrip, connect);
    REGISTER_METHOD(OggTrip, disconnect);
    REGISTER_METHOD_GIMME(OggTrip, ip);
    REGISTER_METHOD_GIMME(OggTrip, port);
    REGISTER_METHOD_GIMME(OggTrip, rxbuffer);
    REGISTER_METHOD_GIMME(OggTrip, buffer);
    REGISTER_METHOD_GIMME(OggTrip, redundancy);
    REGISTER_METHOD_GIMME(OggTrip, transmit);
    REGISTER_METHOD_GIMME(OggTrip, reliable);
    REGISTER_METHOD_GIMME(OggTrip, priority);
}