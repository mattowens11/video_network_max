/* 
	Copyright 2001-2005 - Cycling '74
	Jeremy Bernstein jeremy@bootsquad.com
*/

#include "jit.common.h"
#include "max.jit.mop.h"

typedef struct _max_jit_trip 
{
	t_object		ob;
	void			*obex;
} t_max_jit_trip;

t_jit_err jit_trip_init(void); 

void *max_jit_trip_new(t_symbol *s, long argc, t_atom *argv);
void max_jit_trip_free(t_max_jit_trip *x);
void *max_jit_trip_class;
		 	
int main(void)
{	
	void *p,*q;
	
	jit_trip_init();	
	setup(&max_jit_trip_class, max_jit_trip_new, (method)max_jit_trip_free, (short)sizeof(t_max_jit_trip), 
		0L, A_GIMME, 0);

	p = max_jit_classex_setup(calcoffset(t_max_jit_trip,obex));
	q = jit_class_findbyname(gensym("jit_trip"));    
    max_jit_classex_mop_wrap(p,q,MAX_JIT_MOP_FLAGS_OWN_BANG|MAX_JIT_MOP_FLAGS_OWN_OUTPUTMATRIX); 		
    max_jit_classex_standard_wrap(p,q,0); 	
    addmess((method)max_jit_mop_assist, "assist", A_CANT,0); 
    
    return 0;
}

void max_jit_trip_free(t_max_jit_trip *x)
{
	max_jit_mop_free(x);
	jit_object_free(max_jit_obex_jitob_get(x));
	max_jit_obex_free(x);
}

void *max_jit_trip_new(t_symbol *s, long argc, t_atom *argv)
{
	t_max_jit_trip *x;
	void *o;

	if (x=(t_max_jit_trip *)max_jit_obex_new(max_jit_trip_class,gensym("jit_trip"))) {
		if (o=jit_object_new(gensym("jit_trip"))) {
			max_jit_mop_setup_simple(x,o,argc,argv);			
			max_jit_attr_args(x,argc,argv);
			// no link?
			jit_mop_output_nolink(max_jit_obex_adornment_get(x, _jit_sym_jit_mop), 0);
		} else {
			jit_object_error((t_object *)x,"jit.trip: could not allocate object");
			freeobject(x);
			x = NULL;
		}
	}
	return (x);
}