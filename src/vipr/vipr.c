/* 
	vipr
	Benjamin Smith ben@musicsmiths.us
*/
#define inline __inline

#include "jit.common.h"
#include "ext_strings.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "libavcodec/avcodec.h"
#include "libavutil/mathematics.h"
#include "libswscale/swscale.h"
#ifdef __cplusplus 
}
#endif


typedef struct _vipr 
{
	t_object	ob;	
	char		decode;		// do we decode or encode?
	uchar		CODEC_ID;		// which codec to use!
	enum PixelFormat	internalPixFmt;	// use YUV420 or YUV422?
	int			bit_rate;		// viprion bit rate, only applicable to encoding, the decoders deduce this from the encoded data
	char		interlaced;		// interlace the video or not?
	int			max_b_frames;	// how many bi-directional predictive frames to use. Some codecs that don't use them will complain if > 0
	int			gop_size;		// how many intra frames between full frames
	int			predictor;	// type of prediction to use for FFVHUFF
	int			width, height;	// width and height we are encoding to.
	int			destWidth, destHeight;	// width and height to scale to.
	char		firsttime;	// have we started up already?
	int			flags;			// copy of codec context flags, used for relaying between encoder and decoder
	uchar		sampleMethod;	// what sampling method to use when converting from RGBA to internal format
	
	AVCodec *codec;				// handle to the libavcodec codec - FFVHUFF in this case
	AVCodecContext *c;			// handle to the libavcodec context
	struct SwsContext* encoderContext;		// context for format conversion to/from yuv422
	AVFrame	*internalFrame;			// used to hold data for viprion/deviprion, converted from Jitter's bitmap format
	uint8_t*	intFrameBuffer;			// the buffer used for the internalFrame
	int		intFrameBufSize;		// size of the data buffer for the internal frame
	
	char	*coBuffer;			// space used to hold vipred data
	int		coBufferSize;		// amount of space allocated for vipred data
	char	*extradata;			// extra data, used to input encoding properties to the decoder
	int		extradata_size;		// size of extra data
	
	AVFrame		*tempFrame;		// temp storage for encoding/decoding, to avoid reallocation and freeing every time!
	uint8_t		*tempBuffer;
	int			tempBufSize;
	
	char	debug;	// flag to turn on lots of extra posts
} t_vipr;

void *_vipr_class;

t_vipr *vipr_new(void);
void vipr_free(t_vipr *x);
AVFrame* convertToYUV422(t_vipr *x, const char* inbuf, t_jit_matrix_info* in_info);
AVFrame* convertToARGB(t_vipr *x, AVCodecContext* c, AVFrame* srcFrame);
void video_encode(t_vipr *x, t_jit_matrix_info* in_info, const char* inbuf, void* out_matrix);
void video_decode(t_vipr *x, t_jit_matrix_info* in_info, const char* inbuf, void* out_matrix);
void InitCodec(t_vipr *x);	// is everything setup to rock and roll already?
void createCodecContext(t_vipr *x);	// create and/or verify the codec context
t_jit_err vipr_matrix_calc(t_vipr *x, void *inputs, void *outputs);
t_jit_err vipr_init(void);
//t_jit_err vipr_calc_out_matrix(t_vipr *x, t_jit_matrix_info *in_minfo, void *in_matrix, 
//	t_jit_matrix_info *out2_minfo, void *out2_matrix, t_jit_matrix_info *out_minfo, void *out_matrix);
void vipr_SetCodec(t_vipr *x, char newCodec);	// actually sets the codec, releasing the old one if necessary
t_jit_err vipr_codec_set(t_vipr *x, void *attr, long ac, t_atom *av);		// setter for the codec
t_jit_err vipr_pixfmt_set(t_vipr *x, void *attr, long ac, t_atom *av);		// setter for the pixel format used as the intermediary
t_jit_err vipr_sampmeth_set(t_vipr *x, void *attr, long ac, t_atom *av);		// setter for the colorspace conversion method
void vipr_scaledim(t_vipr *x, long width, long height);			// set the dimensions to scale to, or 0,0 to leave unchanged
void vipr_handle_log(void* ptr, int level, const char* fmt, va_list vl);				// handler for libavcodec log messages

t_jit_err vipr_init(void) 
{
	long attrflags=0;
	t_jit_object *attr;	
	t_jit_object *mop;	
	
	
	/* must be called before using avcodec lib */
//    avcodec_init();
	
    /* register all the codecs */
    avcodec_register_all();
	
	av_log_set_callback(vipr_handle_log);	// set the log callback because we want to see all the log messages!
	
	_vipr_class = jit_class_new("vipr",(method)vipr_new,(method)vipr_free,
		sizeof(t_vipr),0L);

	//add mop
	mop = jit_object_new(_jit_sym_jit_mop,1,1);	// 1 input, 1 output
	jit_mop_output_nolink(mop,1);
	jit_mop_single_type(mop,_jit_sym_char);
	jit_class_addadornment(_vipr_class,mop);
	//add methods
	jit_class_addmethod(_vipr_class, (method)vipr_matrix_calc, "matrix_calc", A_CANT, 0L);
	jit_class_addmethod(_vipr_class, (method)vipr_scaledim, "out_dim", A_DEFLONG, A_DEFLONG, 0L);

	//add attributes	
	attrflags = JIT_ATTR_GET_DEFER_LOW | JIT_ATTR_SET_USURP_LOW;
	attr = jit_object_new(_jit_sym_jit_attr_offset,"predictor",_jit_sym_char,attrflags, 
						  (method)0L,(method)0L,calcoffset(t_vipr,predictor));
	jit_class_addattr(_vipr_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset,"decode",_jit_sym_char,attrflags, 
						  (method)0L,(method)0L,calcoffset(t_vipr,decode));
	jit_class_addattr(_vipr_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset,"pixfmt",_jit_sym_char,attrflags, 
						  (method)0L,(method)0L,calcoffset(t_vipr,internalPixFmt));
	jit_class_addattr(_vipr_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset,"codec",_jit_sym_char,attrflags, 
						  (method)0L,(method)vipr_codec_set,calcoffset(t_vipr,CODEC_ID));
	jit_class_addattr(_vipr_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset,"bitrate",_jit_sym_long,attrflags, 
						  (method)0L,(method)0L,calcoffset(t_vipr,bit_rate));
	jit_class_addattr(_vipr_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset,"bframes",_jit_sym_long,attrflags, 
						  (method)0L,(method)0L,calcoffset(t_vipr,max_b_frames));
	jit_class_addattr(_vipr_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset,"gop",_jit_sym_long,attrflags, 
						  (method)0L,(method)0L,calcoffset(t_vipr,gop_size));
	jit_class_addattr(_vipr_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset,"interlace",_jit_sym_char,attrflags, 
						  (method)0L,(method)0L,calcoffset(t_vipr,interlaced));
	jit_class_addattr(_vipr_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset,"resample",_jit_sym_char,attrflags, 
						  (method)0L,(method)vipr_sampmeth_set,calcoffset(t_vipr,sampleMethod));
	jit_class_addattr(_vipr_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset,"debug",_jit_sym_char,attrflags, 
		(method)0L,(method)0L,calcoffset(t_vipr,debug));
	jit_class_addattr(_vipr_class,attr);

	//add methods
		
	jit_class_register(_vipr_class);

	post("vipr loaded -- ©2011 Benjamin Smith");
	
	return JIT_ERR_NONE;
}

// NOTE: convert allocates memory from the frame and the data, so the calling function must clean it up!
//	actually converts to x->internalPixFmt, not just YUV422!!!
AVFrame* convertToYUV422(t_vipr *x, const char* inbuf, t_jit_matrix_info* in_info)
{	//----------- convert to YUV422
//	AVFrame *srcFrame;
	int width, height, numBytes;
	
	if(x->internalFrame == NULL)
//	{
//		post("can't allocate an RGB storage picture!");
		goto out; //handle_error();
//	}
	
	if (x->destWidth > 0 && x->destHeight > 0)
	{
		width = x->destWidth;
		height = x->destHeight;
	} else
	{
		width = in_info->dim[0];
		height = in_info->dim[1];
	}
	// Determine required buffer size and allocate buffer
	numBytes = avpicture_get_size(x->internalPixFmt, width, height);
	if (numBytes > x->intFrameBufSize)		// if we need more space in the internal buffer, then make more!
	{
		av_free(x->intFrameBuffer);
		x->intFrameBufSize = numBytes;
		x->intFrameBuffer = av_malloc(x->intFrameBufSize);
		if (x->intFrameBuffer == NULL)
		{
			post("vipr error: can't allocate an internal storage buffer.");
			goto out;
		}
	}
	
	// Assign appropriate parts of buffer to image planes in pFrameRGB
	avpicture_fill((AVPicture *)x->internalFrame, x->intFrameBuffer, x->internalPixFmt, width, height);
	
	// setup the input frame:
	if (x->tempFrame == NULL)
		x->tempFrame = avcodec_alloc_frame();
	if (x->tempFrame == NULL)
		goto out;
	x->tempFrame->data[0] = (uint8_t*)inbuf;
	x->tempFrame->linesize[0] = in_info->dim[0] * 4;
	
	// define the transformation/conversion context:
	x->encoderContext = sws_getCachedContext(x->encoderContext, in_info->dim[0], in_info->dim[1], PIX_FMT_BGR32_1, width, height, 
											 x->internalPixFmt, x->sampleMethod, NULL, NULL, NULL);

	if (x->encoderContext)
		sws_scale(x->encoderContext, x->tempFrame->data, x->tempFrame->linesize, 0, in_info->dim[1], x->internalFrame->data, x->internalFrame->linesize);
	
out:
	return x->internalFrame;
}

AVFrame* convertToARGB(t_vipr *x, AVCodecContext* c, AVFrame* srcFrame)
{
	int     numBytes, width, height;
	
	if(x->internalFrame==NULL)
		goto out;
	
	if (x->destWidth > 0 && x->destHeight > 0)
	{
		width = x->destWidth;
		height = x->destHeight;
	} else
	{
		width = c->width;
		height = c->height;
	}
	
	// Determine required buffer size and allocate buffer
	numBytes=avpicture_get_size(PIX_FMT_BGR32_1, width, height);
	if (numBytes > x->intFrameBufSize)		// if we need more space in the internal buffer, then make more!
	{
		av_free(x->intFrameBuffer);
		x->intFrameBufSize = numBytes;
		x->intFrameBuffer = av_malloc(x->intFrameBufSize);
		if (x->intFrameBuffer == NULL)
		{
			post("vipr error: can't allocate an internal storage buffer.");
			goto out;
		}
	}
	
	// Assign appropriate parts of buffer to image planes in pFrameRGB
	avpicture_fill((AVPicture *)x->internalFrame, x->intFrameBuffer, PIX_FMT_BGR32_1, width, height);
	
	x->encoderContext = sws_getCachedContext(x->encoderContext, c->width, c->height, x->internalPixFmt, width, height, PIX_FMT_BGR32_1, x->sampleMethod, NULL, NULL, NULL);

	if (x->encoderContext)
	{
		sws_scale(x->encoderContext, srcFrame->data, srcFrame->linesize, 0, c->height, x->internalFrame->data, x->internalFrame->linesize);
	}
out:
	
	return x->internalFrame;
}

void video_encode(t_vipr* x, t_jit_matrix_info* in_info, const char* inbuf, void* out_matrix)
{
//	AVCodec *codec;
//    AVCodecContext *c= NULL;
    int out_size, outbuf_size;
	uint16_t headerSize;
//    FILE *f;
//    AVFrame *picture;
//    uint8_t *picture_buf; 
	char *outbuf;
	t_jit_matrix_info out_minfo;
	
	AVFrame* pFrameYUV;
	
	// load in the incoming width and height
	if (x->destWidth > 0 && x->destHeight > 0)
	{
		x->width = x->destWidth;
		x->height = x->destHeight;
	} else
	{
		x->width = in_info->dim[0];
		x->height = in_info->dim[1];
	}
	
	InitCodec(x);	// make sure the codec's loaded for bear
	if (!x->codec)
		return;
	
	createCodecContext(x);
	if (!x->c)
		return;

	//----------- convert to YUV422
	pFrameYUV = convertToYUV422(x, inbuf, in_info);
	
    /* alloc image and output buffer */
    outbuf_size = avpicture_get_size(x->internalPixFmt, x->c->width, x->c->height) * 2;	//double, in case the encoding takes more space... might happen on an I frame?
	if (outbuf_size > x->tempBufSize)
	{
		av_free(x->tempBuffer);
		x->tempBufSize = outbuf_size;
		x->tempBuffer = av_malloc(outbuf_size);
	}
	
	x->c->stats_in = x->c->stats_out;	// <- this line gives a ~5% increase in viprion and more in speed, for FFVHUF!!! Don't close and open x->c or it won't do anything.
	
	/* encode the image */
	out_size = avcodec_encode_video(x->c, x->tempBuffer, outbuf_size, pFrameYUV); // picture);	// encodes pFrameYUV into picture_buf using x->c context

	// now want to pack it into a matrix. 
	//	first 2 bytes are the header size, including these 2
	//	next 8 are width and height
	//  then flags from the encoding, codec ID, pixel format
	//	next n are the extra data (size is header size - 16)
	//	remaining is the encoded frame.
	
	headerSize = x->c->extradata_size + 16;	// extradata size + enough space for all the header info (see memcpy list below)
	out_size += headerSize;
	
	if (out_size > headerSize)
	{		
		jit_object_method(out_matrix, _jit_sym_getinfo, &out_minfo);
		// safe to always set. only reallocs matrix if necessary					
		out_minfo.dim[0] = out_size;
		out_minfo.dim[1] = 1;
		out_minfo.type = in_info->type;
		out_minfo.planecount = 1; // 4
		jit_object_method(out_matrix, _jit_sym_setinfo, &out_minfo);		
		jit_object_method(out_matrix, _jit_sym_getinfo, &out_minfo);
		
		jit_object_method(out_matrix, _jit_sym_getdata, &outbuf);
		memcpy(outbuf, &headerSize, 2);
		memcpy(outbuf+2, &(x->c->width), 4);
		memcpy(outbuf+6, &(x->c->height), 4);
		memcpy(outbuf+10, &(x->c->flags), 4);
		memcpy(outbuf+14, &(x->CODEC_ID), 1);
		memcpy(outbuf+15, &(x->internalPixFmt), 1);		// this appears to be all of the elements pertinant to decoding the frame correctly.
		if (x->c->extradata_size > 0)
			memcpy(outbuf+16, x->c->extradata, x->c->extradata_size);
		memcpy(outbuf+headerSize, x->tempBuffer, out_size-headerSize);
	}
//    av_free(picture_buf);
}

void video_decode(t_vipr* x, t_jit_matrix_info* in_info, const char* inbuf, void* out_matrix)	//static void video_decode_example(const char *outfilename, const char *filename)
{	
	t_jit_matrix_info out_minfo;
	char* outbuf;
	int width, height;
	int bufsize, out_size;
	int got_picture = 0;
	AVPacket avpkt;	
	struct AVFrame* pFrameRGB;
	uint16_t headerSize;
//	char* debuff;
	
	if (in_info->size < 16)
		return;
			/// copy additional configuration numbers from the matrix
	memcpy(&headerSize, inbuf, 2);
	if (headerSize < 16)
		return;
	
	memcpy(&(x->width), inbuf + 2, 4);		// what dimensions are we supposed to decode to?
	memcpy(&(x->height), inbuf + 6, 4);
	memcpy(&(x->flags), inbuf + 10, 4);
	memcpy(&(x->CODEC_ID), inbuf + 14, 1);
	memcpy(&(x->internalPixFmt), inbuf + 15, 1);
	
	if (x->width < 1 || x->height < 1)
		return;
	
	if (x->codec && x->CODEC_ID != x->codec->id)
		vipr_SetCodec(x, x->CODEC_ID);
	InitCodec(x);	// make sure we're ready to go.
	
	if (!x->codec)
	{
		post("vipr: codec not loaded. Was it found?");
		return;
	}
	
	// copy in the extradata sent from the encoder
	if (x->debug && headerSize-16 != x->extradata_size)
		post("vipr: extra data rx: %d", x->extradata_size);
	x->extradata_size = headerSize - 16;
	x->extradata_size = (x->extradata_size > 30720 ? 30720 : x->extradata_size);	// amount of allocated space. don't try for more!
	memcpy(x->extradata, inbuf+16, x->extradata_size);
	
	createCodecContext(x);	// x data must be all set at this point so that x->c is initialized correctly
	
	if (!x->c)
		goto out;
	
	avpkt.data = (uint8_t*)inbuf+headerSize;
	avpkt.size = in_info->size - headerSize;

	// allocate frame to hold the decoded image
	if (x->tempFrame == NULL)
		x->tempFrame = avcodec_alloc_frame();
	if (x->tempFrame == NULL)
		goto out;
	bufsize = avpicture_get_size(x->internalPixFmt, x->c->width, x->c->height);
	if (bufsize > x->tempBufSize)
	{
		av_free(x->tempBuffer);
		x->tempBufSize = bufsize;
		x->tempBuffer = av_malloc(bufsize);
	}
	avpicture_fill((AVPicture *)x->tempFrame, x->tempBuffer, x->internalPixFmt,  x->c->width, x->c->height);
	
	out_size = avcodec_decode_video2(x->c, x->tempFrame, &got_picture, &avpkt);	// decoded into tempFrame
	
	if (x->destWidth > 0 && x->destHeight > 0)
	{
		width = x->destWidth;
		height = x->destHeight;
	} else
	{
		width = x->c->width;
		height = x->c->height;
	}
	// convert back to ARGB!
	pFrameRGB = convertToARGB(x, x->c, x->tempFrame);			// converts tempFrame into internalFrame with BGR32_1 pixel format
	
	if (out_size > 1)
	{		
		jit_object_method(out_matrix, _jit_sym_getinfo, &out_minfo);
		// safe to always set. only reallocs matrix if necessary	
		out_minfo.dim[0] = width;
		out_minfo.dim[1] = height;
		out_minfo.type = in_info->type;
		out_minfo.planecount = 4; //1;
		jit_object_method(out_matrix, _jit_sym_setinfo, &out_minfo);		
		jit_object_method(out_matrix, _jit_sym_getinfo, &out_minfo);
		
		jit_object_method(out_matrix, _jit_sym_getdata, &outbuf);
		memcpy(outbuf, pFrameRGB->data[0], avpicture_get_size(PIX_FMT_BGR32_1, width, height));	// TODO: should get pFrameRGB's size out of the conversion function.
	}
out:
	;
	// cleanup decoder memory
//	av_free(debuff);
//	av_free(yuvFrame);
}

t_jit_err vipr_matrix_calc(t_vipr *x, void *inputs, void *outputs)
{
	t_jit_err err=JIT_ERR_NONE;
	t_jit_matrix_info in_minfo,out_minfo; //,out2_minfo;
//	t_matrix_conv_info conv, conv2;
//	char splitdim = CLAMP(x->splitdim,0,JIT_MATRIX_MAX_DIMCOUNT);
//	long splitpoint = x->splitpoint;
//	t_jit_matrix_info *a_minfo,*b_minfo;
//	void *a_matrix,*b_matrix;
	void *in_matrix, *out_matrix; //, *out2_matrix;
	char *in_bp, *out_bp;
	long in_savelock, out_savelock;
	
	in_matrix 	= jit_object_method(inputs,_jit_sym_getindex,0);
	out_matrix 	= jit_object_method(outputs,_jit_sym_getindex,0);
//	out2_matrix = jit_object_method(outputs,_jit_sym_getindex,1);
	
	if (x && in_matrix && out_matrix) { // && out2_matrix) {
		in_savelock = (long) jit_object_method(in_matrix, _jit_sym_lock, 1);
		out_savelock = (long) jit_object_method(out_matrix, _jit_sym_lock, 1);	
		
		jit_object_method(in_matrix,_jit_sym_getinfo,&in_minfo);
		jit_object_method(out_matrix,_jit_sym_getinfo,&out_minfo);
		
		if (in_minfo.dimcount != 2)
		{
			post("jit.avcodec: only 2-dimensioned matrix encoding supported.");
			goto out;
		}
		
		// safe to always set. only reallocs matrix if necessary					
//		out_minfo.dim[0] = in_minfo.dim[0] * in_minfo.dim[1];
//		out_minfo.dim[1] = 1;
//		out_minfo.type = in_minfo.type;
//		out_minfo.planecount = in_minfo.planecount; 
//		jit_object_method(out_matrix, _jit_sym_setinfo, &out_minfo);		
//		jit_object_method(out_matrix, _jit_sym_getinfo, &out_minfo);
		
		jit_object_method(in_matrix,_jit_sym_getdata,&in_bp);
		jit_object_method(out_matrix,_jit_sym_getdata,&out_bp);
		
		if (x->decode)
		{
			video_decode(x, &in_minfo, in_bp, out_matrix);
		}
		else
			video_encode(x, &in_minfo, in_bp, out_matrix);
out:
		jit_object_method(out_matrix, _jit_sym_lock, out_savelock);
		jit_object_method(in_matrix, _jit_sym_lock, in_savelock);
	}
	else return JIT_ERR_INVALID_PTR;
	
	return err;
}

void InitCodec(t_vipr *x)
{
	int i;
	if (!x->codec)
	{
		if (x->c)
		{
			avcodec_close(x->c);
			av_free(x->c);
			x->c = NULL;
		}
		if (x->decode)
			x->codec = avcodec_find_decoder(x->CODEC_ID);
		else
			x->codec = avcodec_find_encoder(x->CODEC_ID);
		
		if (!x->codec) {
			post("jit.avcodec: codec not found, id: %d", x->CODEC_ID);
			//exit(1);
		}
		else
		{
			post("jit.avcodec: loaded codec %s, id:%d", x->codec->long_name, x->CODEC_ID);
			x->flags = 0;	// reset flags in case something got saved from a previous codec
			
			if (x->codec->pix_fmts)		// some don't specify formats!
				for (i = 0;;i++)
				{
					if (x->codec->pix_fmts[i] > -1)
						post("     codec pix fmt:%d",x->codec->pix_fmts[i]);
					else break;
				}
		}
	}
}

// if the context has not been allocated and opened then we do it.
//		if the width/height have changed then we close and open again
void createCodecContext(t_vipr *x)	// create and/or verify the codec context
{
	AVRational time_base;
	time_base.num = 1;
	time_base.den = 25;
//	if (!x->c)
//		x->c = avcodec_alloc_context();
	
	// see if any of the parameters have changed since context creation. If so then recreate it!
	if (!x->c || x->c->width != x->width || x->c->height != x->height || x->internalPixFmt != x->c->pix_fmt || (x->decode && x->c->extradata_size != x->extradata_size)
				|| (!x->decode && (x->c->prediction_method != x->predictor || x->c->gop_size != x->gop_size || x->c->bit_rate != x->bit_rate
				|| (x->interlaced != (x->c->flags & (CODEC_FLAG_INTERLACED_ME | CODEC_FLAG_INTERLACED_DCT) ? 1 : 0) ))) || x->flags != x->c->flags)
	{
		if (x->debug)
		{
			post("recreating the codec context (decode=%d)", x->decode);
			if (x->c)
				post("%d %d %d %d %d %d", x->c->width, x->c->height, x->c->prediction_method, x->c->extradata_size, x->c->bit_rate, x->c->flags);
			post("%d %d %d %d %d %d %d", x->width, x->height, x->predictor, x->extradata_size, x->bit_rate, x->flags, x->interlaced);
		}
		if (x->c)
			avcodec_close(x->c);
//		av_free(x->c);			//<- this may leave stats_in & out and extradata memory to leak away...
//		x->c = NULL;
		if (!x->c)
			x->c = avcodec_alloc_context();
		avcodec_get_context_defaults(x->c);
		
		if (!x->decode)
		{
			if (x->interlaced)
				x->flags |= CODEC_FLAG_INTERLACED_ME | CODEC_FLAG_INTERLACED_DCT;
			else
			{
				if (x->flags & CODEC_FLAG_INTERLACED_ME)	// check the bit, if set then flip it, other wise leave it.
					x->flags ^= CODEC_FLAG_INTERLACED_ME;
				if (x->flags & CODEC_FLAG_INTERLACED_DCT)
					x->flags ^= CODEC_FLAG_INTERLACED_DCT;
			}
		}
		
		x->c->codec_type = AVMEDIA_TYPE_VIDEO;
		x->c->width = x->width;
		x->c->height = x->height;
		x->c->pix_fmt = x->internalPixFmt;	// our format for all encoding and decoding
		// all these rest are not used by huffyuv, but we include for completeness.
		x->c->bit_rate = x->bit_rate;
		/* frames per second */
		x->c->time_base= time_base; // (AVRational){1,25};
		x->c->gop_size = x->gop_size; // number of pictures in a group, marked by an I frame at the start
		x->c->max_b_frames = x->max_b_frames;
		x->c->flags = x->flags;
		if (x->decode)
		{
			x->c->extradata = (uint8_t*)x->extradata;
			x->c->extradata_size = x->extradata_size;
		} else
			x->c->extradata_size = 0;
		
		x->c->prediction_method = x->predictor;		// prediction method, only used by ffvhuff/huffyuv
		x->c->context_model = (x->CODEC_ID == CODEC_ID_FFVHUFF);		// adaptive huff tables, only used by ffvhuff/huffyuv
		
		if (avcodec_open(x->c, x->codec) < 0)
		{
			post("jit.avcodec error! failed to open codec context.");
			av_free(x->c);
			x->c = NULL;
		}
	}
}

t_vipr *vipr_new(void)
{
	t_vipr *x;
//	t_jit_matrix_info info;
//	long i;
		
	if (x=(t_vipr *)jit_object_alloc(_vipr_class)) {
		x->decode = 0;	// encode by default
		x->firsttime = true;	// we haven't tried to do anything yet.
		x->codec = NULL;
		x->c = NULL;
		x->encoderContext = NULL;
		x->width = x->height = 0;
		x->destWidth = x->destHeight = 0;
		x->predictor = FF_PRED_MEDIAN;
		x->extradata_size = 0;
		x->extradata = av_mallocz(1024*30);	// max size, taken from huffyuv.c
		x->bit_rate = 400000;
		x->CODEC_ID = CODEC_ID_MPEG4;
		x->internalPixFmt = PIX_FMT_YUV420P;
		x->interlaced = 0;	// do not interlace by default
		x->max_b_frames = 0;
		x->gop_size = 10;
		x->debug = false;	// switch to true to get useful post statements
		x->flags = 0;
		x->sampleMethod = SWS_FAST_BILINEAR;
		x->internalFrame = avcodec_alloc_frame();	// allocate us a frame!	
		if(x->internalFrame==NULL)
			post("vipr error: can't allocate an internal storage frame.");
		x->intFrameBuffer = av_malloc(307000);			// set data pointer to 0x00
		x->intFrameBufSize = 307000;				// no data has been allocated yet
		x->coBuffer = NULL;
		x->coBufferSize = 0;				// init the viprion buffer space and size.
		
		x->tempFrame = avcodec_alloc_frame();		// init the temp storage
		x->tempBuffer = NULL;
		x->tempBufSize = 0;
	} else {
		x = NULL;
	}	
	return x;
}

void vipr_free(t_vipr *x)
{
	av_free(x->c);			// free all the things we've allocated
	av_free(x->extradata);
	av_free(x->internalFrame->data[0]);
	av_free(x->internalFrame);
	av_free(x->coBuffer);
}

void vipr_SetCodec(t_vipr *x, char newCodec)
{
	if (!x->codec || newCodec != x->codec->id)	// it's new!
	{
		if (x->c)	// close and free the old context
			avcodec_close(x->c);
		av_free(x->c);
		x->c = NULL;
		
		x->CODEC_ID = newCodec;
		x->firsttime = true;	// will cause the codec to be reloaded
		x->codec = NULL;
		InitCodec(x);	// get it set again right away.
	}
}

t_jit_err vipr_codec_set(t_vipr *x, void *attr, long ac, t_atom *av)		// setter for the codec
{
	if (ac&&av) {
		int nCodec = jit_atom_getcharfix(av);
		vipr_SetCodec(x, nCodec);
	} else {
		// no args, leave it alone
	}
	return JIT_ERR_NONE;
}
t_jit_err vipr_sampmeth_set(t_vipr *x, void *attr, long ac, t_atom *av)		// setter for the colorspace conversion method
{
	if (ac&&av) {
		int sample = jit_atom_getcharfix(av);
		if (sample >= 0 && sample <= 10)
			x->sampleMethod = pow(2, sample);
	} else {
		// no args, leave it alone
	}
	return JIT_ERR_NONE;
}
t_jit_err vipr_pixfmt_set(t_vipr *x, void *attr, long ac, t_atom *av)
{
	if (ac&&av) {
		int fmt = jit_atom_getcharfix(av);
		if (x->internalPixFmt != fmt)
		{
			x->internalPixFmt = fmt; //(fmt ? PIX_FMT_YUV422P : PIX_FMT_YUV420P);
//			if (x->c)
//				avcodec_close(x->c);
//			av_free(x->c);
//			x->c = NULL;	// will cause the context to be recreated.
		}
	} else {
		// no args, leave it alone
		//x->CODEC_ID = 0;
	}
	return JIT_ERR_NONE;
}
			/// called with "out_dim" message
void vipr_scaledim(t_vipr *x, long width, long height)			// set the dimensions to scale to, or 0,0 to leave unchanged
{
	if (x)
	{			// are they new values, and are they even numbers?
		if ((x->destWidth != width || x->destHeight != height) && width % 2 == 0 && height % 2 == 0)
		{
			x->destWidth = width;
			x->destHeight = height;
		}
	}
}

void vipr_handle_log(void* ptr, int level, const char* fmt, va_list vl)	// handler for libavcodec log messages
{
    static int print_prefix=1;
    static int count;
    static char line[1024], prev[1024];
//    static const uint8_t color[]={0x41,0x41,0x11,0x03,9,9,9};
    AVClass* avc= ptr ? *(AVClass**)ptr : NULL;
//    if(level>AV_LOG_WARNING)	// only prints warnings, errors, and fatal messages
//        return;
	if (avc == NULL)
		return;
	if (strcmp(avc->class_name, "sws_scale"))
		return;		// don't show the thousands of "warning: no accelerated x to y conversion" messages
	
//#undef fprintf
    //if(print_prefix && avc) {
    //    sprintf(line, sizeof(line), "[%s @ %p]", avc->item_name(ptr), ptr);
    //}else
    //    line[0]=0;
	
//    vsnprintf(line + strlen(line), sizeof(line) - strlen(line), fmt, vl);
	
    print_prefix= line[strlen(line)-1] == '\n';
    if(print_prefix && !strcmp(line, prev)){	// have we seen this message already? then ignore for now.
        count++;
        return;
    }
    if(count>0){
//        fprintf(stderr, "    Last message repeated %d times\n", count);
        count=0;
    }
	post(line);
//    colored_fputs(color[av_clip(level>>3, 0, 6)], line);
    strcpy(prev, line);		// store line for comparison to next log message
}

// TEST DECODE:

//	//	AVCodec *codec;
//	//	AVCodecContext *c= NULL;
//		int frame, got_picture, len;
//	//	FILE *f;
//		AVFrame *picture;
//	//	uint8_t inbuf[INBUF_SIZE + FF_INPUT_BUFFER_PADDING_SIZE];
//	//	char buf[1024];
//		AVPacket avpkt;
//		t_jit_matrix_info out_minfo;
//		char* out_bp;
//		
//		av_init_packet(&avpkt);
//		
//		/* set end of buffer to 0 (this ensures that no overreading happens for damaged mpeg streams) */
//	//	memset(inbuf + INBUF_SIZE, 0, FF_INPUT_BUFFER_PADDING_SIZE);
//		
//		post("Video decoding\n");
//		
//		/* find the mpeg1 video decoder */
//		x->codec = avcodec_find_decoder(CODEC_ID_FFVHUFF); //CODEC_ID_MPEG1VIDEO);
//	//	if (!codec) {
//	//		post("codec not found");
//	//		return; //exit(1);
//	//	}
//	//	
//	//	c= avcodec_alloc_context();
//		picture= avcodec_alloc_frame();
//		
//	//	if(codec->capabilities&CODEC_CAP_TRUNCATED)
//	//		c->flags|= CODEC_FLAG_TRUNCATED; /* we do not send complete frames */
//		
//		/* For some codecs, such as msmpeg4 and mpeg4, width and height
//		 MUST be initialized there because this information is not
//		 available in the bitstream. */
//	//	c->extradata= av_mallocz(1024*30); // 256*3+4 == 772
//	//	((uint8_t*)c->extradata)[0]= 0 | (1 << 6); //s->predictor | (s->decorrelate << 6);
//	//    ((uint8_t*)c->extradata)[1]= 24; //s->bitstream_bpp;
//	//    ((uint8_t*)c->extradata)[2]= 0x10; //s->interlaced ? 0x10 : 0x20;
//	//	//    if(s->context)
//	//	        ((uint8_t*)c->extradata)[2]|= 0x40;
//	//    ((uint8_t*)c->extradata)[3]= 0;
//	//    c->extradata_size= 4;
//		
//		x->c->pix_fmt = PIX_FMT_RGB32;
//		x->c->time_base = (AVRational){1,25};
//		x->c->width = 320;
//		x->c->height = 240;
//		x->c->bits_per_coded_sample = 24;
//		x->c->flags = 0;
//		x->c->context_model = 1;
//		x->c->prediction_method = FF_PRED_LEFT;
//		
//	//	avpicture_fill((AVPicture*)picture, inbuf, PIX_FMT_YUV420P, c->width, c->height);
//	//	picture->data[0] = inbuf;
//	//    picture->linesize[0] = x->c->width;
//	//    picture->linesize[1] = x->c->width;
//	//    picture->linesize[2] = x->c->width;
//		
//		/* open it */
//		if (avcodec_open(x->c, x->codec) < 0) {
//			post("could not open codec");
//			return; //exit(1);
//		}
//		
//		/* the codec gives us the frame size, in samples */
//		
//	//	f = fopen(filename, "rb");
//	//	if (!f) {
//	//		fprintf(stderr, "could not open %s\n", filename);
//	//		exit(1);
//	//	}
//		
//		frame = 0;
//	//	for(;;) {
//		avpkt.size = in_info->size;		// size of input frame in bytes
//	//		avpkt.size = fread(inbuf, 1, INBUF_SIZE, f);
//		if (avpkt.size > 0)
//		{
//			
//			/* NOTE1: some codecs are stream based (mpegvideo, mpegaudio)
//			 and this is the only method to use them because you cannot
//			 know the vipred data size before analysing it.
//			 
//			 BUT some other codecs (msmpeg4, mpeg4) are inherently frame
//			 based, so you must call them with all the data for one
//			 frame exactly. You must also initialize 'width' and
//			 'height' before initializing them. */
//			
//			/* NOTE2: some codecs allow the raw parameters (frame size,
//			 sample rate) to be changed at any frame. We handle this, so
//			 you should also take care of it */
//			
//			/* here, we use a stream based decoder (mpeg1video), so we
//			 feed decoder and see if it could decode a frame */
//			avpkt.data = inbuf;
//		//		while (avpkt.size > 0) {
//			
//			len = avcodec_decode_video2(x->c, picture, &got_picture, &avpkt);
//			if (len < 0) {
//				post("Error while decoding frame %d\n", frame);
//				goto out; //exit(1);
//			}
//			if (got_picture) {
//				post("decoded frame size: %3d", len);
//				
//				if (len > 1)
//				{
//					struct SwsContext* encoderSwsContext;
//	//				AVFrame* encoderRawFrame;
//	//				AVFrame* encoderRescaledFrame; 
//					AVFrame* pFrameRGB;
//					int     numBytes;
//					uint8_t *buffer;
//					
//					// Allocate an AVFrame structure
//	//				avpicture_alloc(pFrameRGB, PIX_FMT_YUV422P, c->width, c->height);
//					pFrameRGB = avcodec_alloc_frame();
//					if(pFrameRGB==NULL)
//					{
//						post("can't allocate an RGB storage picture");
//						goto out; //handle_error();
//					}
//					
//					// Determine required buffer size and allocate buffer
//					numBytes=avpicture_get_size(PIX_FMT_RGB24, x->c->width,x-> c->height);
//					buffer= (uint8_t*)av_malloc(numBytes);
//	//				post("size of 422:%d 420:%d", avpicture_get_size(PIX_FMT_YUV422P, 320, 240), avpicture_get_size(PIX_FMT_YUV420P, 320, 240));
//					
//					// Assign appropriate parts of buffer to image planes in pFrameRGB
//					avpicture_fill((AVPicture *)pFrameRGB, buffer, PIX_FMT_RGB24, x->c->width, x->c->height);
//
//					encoderSwsContext = sws_getContext(x->c->width, x->c->height, PIX_FMT_YUV422P, x->c->width, x->c->height, PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);
//	//				encoderSwsContext = sws_getCachedContext(encoderSwsContext, c->width, c->height, PIX_FMT_YUV420P, c->width, c->height, PIX_FMT_RGB32, SWS_BICUBIC, NULL, NULL, NULL);
//					
//	//				post("%d",sws_scale(encoderSwsContext, picture->data, picture->linesize, 0, c->height, pFrameRGB->data, pFrameRGB->linesize));
//					
//	//				img_convert((AVPicture *)pFrameRGB, PIX_FMT_RGB32, (AVPicture*)picture, c->pix_fmt, c->width, c->height);
//
//					jit_object_method(out_matrix,_jit_sym_getinfo,&out_minfo);
//					
//					// safe to always set. only reallocs matrix if necessary					
//					out_minfo.dim[0] = x->c->width;
//					out_minfo.dim[1] = x->c->height;
//					out_minfo.type = in_info->type;
//					out_minfo.planecount = 4; 
//					jit_object_method(out_matrix, _jit_sym_setinfo, &out_minfo);		
//					jit_object_method(out_matrix, _jit_sym_getinfo, &out_minfo);
//					
//					jit_object_method(out_matrix,_jit_sym_getdata,&out_bp);
//					
//						/* the picture is allocated by the decoder. no need to
//						 free it */
//					memcpy(out_bp, picture->data[0], len);
//					
//					sws_freeContext(encoderSwsContext);
//					av_free(pFrameRGB);
//					av_free(buffer);
//				}
//		//				snprintf(buf, sizeof(buf), outfilename, frame);
//		//				pgm_save(picture->data[0], picture->linesize[0],
//		//						 c->width, c->height, buf);
//		//				frame++;
//			} else
//				post("did not 'got_picture'!");
//	//	avpkt.size -= len;
//	//	avpkt.data += len;
//	//		}
//	//	}
//		
//		/* some codecs, such as MPEG, transmit the I and P frame with a
//		 latency of one frame. You must do the following to have a
//		 chance to get the last frame of the video */
//	//	avpkt.data = NULL;
//	//	avpkt.size = 0;
//	//	len = avcodec_decode_video2(c, picture, &got_picture, &avpkt);
//	//	if (got_picture) {
//	//		printf("saving last frame %3d\n", frame);
//	//		fflush(stdout);
//	//		
//	//		/* the picture is allocated by the decoder. no need to
//	//		 free it */
//	//		snprintf(buf, sizeof(buf), outfilename, frame);
//	//		pgm_save(picture->data[0], picture->linesize[0],
//	//				 c->width, c->height, buf);
//	//		frame++;
//		}
//	out:
//	//	fclose(f);
//		
//		avcodec_close(x->c);
//	//	av_free(c);
//		av_free(picture);
//	//	printf("\n");
//	//}	
//	}

