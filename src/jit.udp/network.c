/*
 *  network.c
 *  
 *
 *  Created by Ben Smith on 11/14/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "jit.common.h"
#include "network.h"

//void createEnetClient()
//{	
//	ENetHost* client;
//	ENetPeer* peer;
//	ENetEvent event;
//	
//    client = enet_host_create (NULL /* create a client host */,
//							   1 /* only allow 1 outgoing connection */,
//							   0 /* 56K modem with 56 Kbps downstream bandwidth */,
//							   0 /* 56K modem with 14 Kbps upstream bandwidth */);
//	
//    if (client == NULL)
//    {
//        post ("An error occurred while trying to create an ENet client host.\n");
//        exit (EXIT_FAILURE);
//    }
//	
//	// connect
//	ENetAddress address;
//	
//    /* Connect to some.server.net:1234. */
//    enet_address_set_host (& address, "127.0.0.1"); //"abe.ncsa.uiuc.edu");
//    address.port = 1234;
//	
//    /* Initiate the connection, allocating the two channels 0 and 1. */
//    peer = enet_host_connect (client, & address, 2);    
//    
//    if (peer == NULL)
//    {
//		post ("No available peers for initiating an ENet connection.\n");
//		return; //exit (EXIT_FAILURE);
//    }
//	
//    /* Wait up to 5 seconds for the connection attempt to succeed. */
//	if (enet_host_service (client, & event, 5000) > 0 &&
//		event.type == ENET_EVENT_TYPE_CONNECT)
//	{
//		post ("Connection to some.server.net:1234 succeeded.");
//	}
//	else
//	{
//		/* Either the 5 seconds are up or a disconnect event was
//		 * received. Reset the peer in the event the 5 seconds
//		 * had run out without any significant event.
//		 */
//		enet_peer_reset (peer);
//		
//		post ("Connection to some.server.net:1234 failed.");
//	}
//}

int serviceNetwork(ENetHost* host, ENetEvent* event, char* data, long dataSize)	// pass in the host, an event, and space to store the new data!
{
	int bytesRx = 0;
	int index;
//	char * data = 0x00;
    /* Wait up to 1 milliseconds for an event. */
    if (enet_host_service (host, event, 0) > 0)
    {
        switch (event->type)
        {
			case ENET_EVENT_TYPE_CONNECT:
				post ("A new client connected from %x:%u.\n", 
						event->peer -> address.host,
						event->peer -> address.port);
//				
//				/* Store any relevant client information here. */
				event->peer->data = (int)event->peer->address.host;
				
				break;
				
			case ENET_EVENT_TYPE_RECEIVE:
//				printf ("A packet of length %u containing %d was received from %s on channel %u.\n",
//						event.packet -> dataLength,
//						event.packet -> data,
//						event.peer -> data,
//						event.channelID);
//				fflush(stdout);
				
//				data = (float *) malloc (event->packet->dataLength / 4 * sizeof(float));	// TODO: ensure the right number of bytes have been received.. more robust checks here.
//				if (event->channelID == 0)	// 0 is for Matrix data. 1 for other messages
//				{
				
				if (event->packet->dataLength > 3)
					memcpy(&index, event->packet->data, 4);
				if (index >= 0 && index < dataSize)
					memcpy(data+index, event->packet->data, event->packet->dataLength-4);
				
					bytesRx = event->packet->dataLength;
//				}
				
				/* Clean up the packet now that we're done using it. */
				enet_packet_destroy (event->packet);
				break;
				
			case ENET_EVENT_TYPE_DISCONNECT:
				post ("%s disconected.", event->peer->data);
				event->peer -> data = NULL;
				break;
			case ENET_EVENT_TYPE_NONE:
				// nothing
				break;
        }
    }	
	return bytesRx;
}

void destroyEnetPeer(ENetHost* host)
{
    enet_host_destroy(host);
}

void sendData(ENetHost* host, char *data, int size, int packet_size)	// -- broadcast data, regardless of connections.
{
	int i, remaining; //, count = 0; //, count = 0;
//	ENetEvent event;
	int data_size = packet_size - 4;	// less the header size
	
	char* buf = malloc(packet_size);	// first 4 bytes are the location in the matrix/buffer
	
	for (i = 0; i < size; i += data_size)
	{
		remaining = size - i;
		remaining = (remaining > data_size ? packet_size : remaining+4);
		if (remaining > 0)
		{
			memcpy(buf, &i, 4);	// copy in the header int
			memcpy(buf+4, data+i, data_size);	// copy the rest of the data
			ENetPacket * packet = enet_packet_create(buf, remaining, 0); //, ENET_PACKET_FLAG_RELIABLE); //(char*)data+i
			
//			memcpy(&packet->data[0], data + i, remaining);
			/* Send the packet to the peer over channel id 3.
			 * One could also broadcast the packet by
			 * enet_host_broadcast (host, 0, packet);
			 */
//			for (j = 0; j < host->peerCount; j++)
//			{
//				if (enet_peer_send(host->peers+j, 0, packet) == 0)
//					count++;
//			}
//			memcpy(&temp, data+i, 4);
//			printf("id:%f ", temp);
//			fflush(stdout);
			enet_host_broadcast (host, 0, packet);
//			count++;
//			enet_host_flush(host);
//			enet_packet_destroy(packet);
		}
	}
//	post("sent %d packets.\n", count);
}