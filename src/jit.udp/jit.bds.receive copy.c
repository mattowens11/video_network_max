#include "jit.common.h"
#include "math.h"
//#include "network.h"
#include "ext_systhread.h"

// includes for network stuff
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <mach/mach_time.h>	// for measuring time of receive/copy code and attempt to sync on the sender/server

// TODO: add disconnect functions, ensure "size x y" works

typedef struct _jit_bds_receive 
{
	t_object				ob;			// this is where Max stores info about the class
	void* x_systhread;	// thread id
	bool x_systhread_cancel; //thread cancel flag
	void* x_qelem;	// for message passing between threads
	t_systhread_mutex mutex;	// mutex for copying into myMatrix
	char* buffer;		// for receiving and sending packets
	char* myMatrix;		// for building the matrix internally before posting -- requires critical(1) to touch, as does newMatrix
	bool					connected, newMatrix;			// are we connected, and as a server or client?, and do we have new matrix data to send out?
//	ENetHost * host;		// handles to enet peer and host
//	ENetPeer * peer;
//	ENetEvent myEvent;
	int socket;
//	ENetAddress address;
	struct sockaddr_in		server_addr;
	int						packetSize, port;
	long					matSize;			// number of bytes in the matrix we're receiving
} t_jit_bds_receive;

void *_jit_bds_receive_class;

t_jit_bds_receive *jit_bds_receive_new(void);		// creates a new class for us, allocates memory
void jit_bds_receive_free(t_jit_bds_receive *x);			// destructor
t_jit_err jit_bds_receive_matrix_calc(t_jit_bds_receive *x, void *inputs, void *outputs);
//void jit_bds_receive_calculate_ndim(t_jit_bds_receive *x, long dimcount, long *dim, long planecount, 
//	t_jit_matrix_info *out_minfo, char *bop);
void jit_bds_receive_connect(t_jit_bds_receive *x); //, t_symbol* ip, int port);	// connect to host 'IP' as a client
void jit_bds_receive_disconnect(t_jit_bds_receive *x); //, t_symbol* ip, int port);	// close sockets, stop threads
//void jit_bds_receive_server(t_jit_bds_receive *x, int port);	// start as a server
//void jit_bds_receive_bang(t_jit_bds_receive *x);
// thread functions below
void jit_bds_receive_start_thread(t_jit_bds_receive *x);
void jit_bds_receive_stop_thread(t_jit_bds_receive *x);
void* jit_bds_receive_threadproc(t_jit_bds_receive *x);
void jit_bds_receive_qfn(t_jit_bds_receive *x);
//void jit_bds_receive_packetsize(t_jit_bds_receive *x, int packetSize);	// set the moximum packet size used for UDP transmission

t_jit_err jit_bds_receive_init(void);


t_jit_err jit_bds_receive_init(void) 
{
	long attrflags=0;
	t_jit_object *attr;
	t_jit_object *mop;
	
	_jit_bds_receive_class = jit_class_new("jit_bds_receive",(method)jit_bds_receive_new,(method)jit_bds_receive_free, sizeof(t_jit_bds_receive),0L);

	//add mop
	mop = jit_object_new(_jit_sym_jit_mop,1,1);
	jit_mop_single_type(mop, _jit_sym_char);
	jit_mop_single_planecount(mop, 4);
//	o=jit_object_method(mop,_jit_sym_getoutput,1);
//	jit_attr_setlong(o,_jit_sym_planelink,0);
	jit_class_addadornment(_jit_bds_receive_class,mop);
	//add methods
	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_matrix_calc, "matrix_calc", A_CANT, 0L);
	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_connect, "connect", 0L); // A_SYM, A_LONG,
	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_disconnect, "disconnect", 0L); // A_SYM, A_LONG,
//	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_server, "server", A_LONG, 0L);
//	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_packetsize, "packetsize", A_LONG, 0L);
//	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_bang, "bang", 0L);

	//add attributes	
	attrflags = JIT_ATTR_GET_DEFER_LOW | JIT_ATTR_SET_USURP_LOW;

	// start - beginning udp cell
	attr = jit_object_new(_jit_sym_jit_attr_offset, "port", _jit_sym_long, 
		attrflags, (method)0L, (method)0L, calcoffset(t_jit_bds_receive, port));
	jit_class_addattr(_jit_bds_receive_class,attr);

	jit_class_register(_jit_bds_receive_class);			// register the class with Max for dynamic binding

	return JIT_ERR_NONE;
}


t_jit_err jit_bds_receive_matrix_calc(t_jit_bds_receive *x, void *inputs, void *outputs)
{
	t_jit_err err=JIT_ERR_NONE;
	long out_savelock; //, matrixSize; //dimmode, in_savelock
	t_jit_matrix_info out_minfo; //in_minfo
	char *out_bp; //*in_bp
//	long i,dimcount; //planecount,dim[JIT_MATRIX_MAX_DIMCOUNT];
	void *out_matrix; // *in_matrix
	
//	in_matrix = jit_object_method(inputs,_jit_sym_getindex,0);
	out_matrix 	= jit_object_method(outputs,_jit_sym_getindex,0);
	
	if (x && x->socket)	// make sure we're connected
	{
		if (out_matrix) //x->connected&&
		{
//			critical_exit(0);
			out_savelock = (long) jit_object_method(out_matrix,_jit_sym_lock,1);
			jit_object_method(out_matrix,_jit_sym_getinfo,&out_minfo);
			jit_object_method(out_matrix,_jit_sym_getdata,&out_bp);
			if (!out_bp) { err=JIT_ERR_GENERIC; goto out;}
			
			//get dimensions/planecount
//			dimcount   = out_minfo.dimcount;
////				planecount = out_minfo.planecount;	
//			
//			matrixSize = out_minfo.planecount;		
//			for (i=0;i<dimcount;i++) {
////					dim[i] = MIN(in_minfo.dim[i],out_minfo.dim[i]);
//				matrixSize *= out_minfo.dim[i];
//			}
			
			if (out_minfo.size > x->matSize)	// assign new space for our internal matrix
			{
				systhread_mutex_lock(x->mutex);		// get the mutex lock before reading the matrix buffer
				if (x->myMatrix != 0x00)
					free(x->myMatrix);
				x->matSize = out_minfo.size;
				x->myMatrix = malloc(x->matSize);
				systhread_mutex_unlock(x->mutex);		// get the mutex lock before reading the matrix buffer
			}
			if (x->newMatrix)			// do we have new data?
			{
				systhread_mutex_lock(x->mutex);		// get the mutex lock before reading the matrix buffer
				memcpy(out_bp, x->myMatrix, out_minfo.size);
				x->newMatrix = false;	// set flag until there is more data.
				systhread_mutex_unlock(x->mutex);
			}
		} else
		{
			return JIT_ERR_INVALID_PTR;
		}
	}	// if (x)
out:
//	jit_object_method(in_matrix,_jit_sym_lock,in_savelock);
	jit_object_method(out_matrix,_jit_sym_lock,out_savelock);
	return err;
}

//------------------------- JIT_UDP_NEW -------- Constructor
t_jit_bds_receive *jit_bds_receive_new(void)
{
	t_jit_bds_receive *x;
	
//	enet_initialize();	// make sure enet is up and ready to roll!
		
	if (x=(t_jit_bds_receive *)jit_object_alloc(_jit_bds_receive_class)) {
		x->packetSize = 1440;	// default packet size
		x->buffer = malloc(x->packetSize);	// buffer space for building packets
		x->connected = false;
//		x->isServer = false;
//		x->host = 0x00;
//		x->peer = 0x00;
//		x->x_qelem = qelem_new(x,(method)jit_bds_receive_qfn);
		x->x_systhread = NULL;
		x->matSize = 320*240*4;	// set at 0 because we haven't malloc'd any space for 'newMatrix'
		x->myMatrix = malloc(x->matSize);
		x->newMatrix = false;
		x->socket = 0;
		x->port = 8001;
		systhread_mutex_new(&x->mutex, SYSTHREAD_MUTEX_NORMAL);
	} else {
		x = NULL;
	}	
	
	return x;
}

void jit_bds_receive_free(t_jit_bds_receive *x)
{
	jit_bds_receive_stop_thread(x);
	jit_bds_receive_disconnect(x);
//	if (x->x_qelem)
//		qelem_free(x->x_qelem);
	
//	if (x->socket != 0)
//		enet_socket_destroy(x->socket);
//	if (x->host != 0x00)
//		enet_host_destroy(x->host);
//	x->host = 0x00;
//	x->peer = 0x00;
	free(x->buffer);
	if (x->myMatrix != 0x00)
		free(x->myMatrix);
	systhread_mutex_free(x->mutex);
	
//	enet_deinitialize();	// cleanup enet
}
//------------------------------------------- End Destructor ----

// startup a client connection to the given ip and port
void jit_bds_receive_connect(t_jit_bds_receive *x) //, t_symbol* ip, int port)
{	
	jit_bds_receive_disconnect(x);
	systhread_mutex_lock(&x->mutex);	// ensure complete control of the sockets
	
	if ((x->socket = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		post("jit.bds.receive: error creating socket");
		return;
	}
	
	x->server_addr.sin_family = AF_INET;
	x->server_addr.sin_port = htons(x->port);
	x->server_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(x->server_addr.sin_zero),8);
	
	if (bind(x->socket,(struct sockaddr *)&x->server_addr,
			 sizeof(struct sockaddr)) == -1)
	{
		post("jit.bds.receive: unable to bind socket");
		return;
	}
	
	// set non-blocking
	int flags;
	flags = fcntl(x->socket,F_GETFL,0);
	assert(flags != -1);
	fcntl(x->socket, F_SETFL, flags | O_NONBLOCK);
	
//	struct timeval tv;	// 4 lines to set a blocking timeout
//	
//	tv.tv_sec = 0; //5 / 1000.f ;
//	tv.tv_usec = 0;
//	
//    setsockopt (x->socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof tv);
	
	post("jit.bds.receive: ready to receive on port %d.", x->port);
	
//	critical_enter(0);
//	
//	if (x->socket)	// cleanup old connection
//		enet_socket_destroy(x->socket);
//	
//	enet_address_set_host(&x->address, "127.0.0.1");	//send-to socket address
//	//	x->address.host = ENET_HOST_ANY;
//	x->address.port = x->port;
//	
//	ENetAddress address;								//receiving socket address
////	enet_address_set_host(&address, "127.0.0.1");
//	address.host = ENET_HOST_ANY;
//	address.port = x->port;
//	
//	x->socket = enet_socket_create(ENET_SOCKET_TYPE_DATAGRAM, &address);
////	post("socket : %d", x->socket);
//	
//	critical_exit(0);
	systhread_mutex_unlock(&x->mutex);	// let the world run again!
	
	jit_bds_receive_start_thread(x);
}

// startup a client connection to the given ip and port
void jit_bds_receive_disconnect(t_jit_bds_receive *x) //, t_symbol* ip, int port)
{
//	jit_bds_receive_stop_thread(x);	// stop the reception thread
	
	systhread_mutex_lock(&x->mutex);	// stop socket use.
	if (x->socket)	// cleanup old connection
		close(x->socket);
	x->socket = 0;
	systhread_mutex_unlock(&x->mutex);	// let the world run again!
}

// threading----------------

void jit_bds_receive_start_thread(t_jit_bds_receive *x)
{
//	jit_bds_receive_stop_thread(x);														// kill thread if, any
	
	// create new thread + begin execution
	if (x->x_systhread == NULL) {
		x->x_systhread_cancel = false;	// reset cancel flag
		// stacksize, priority and flags are ignored for now.
		systhread_create((method)jit_bds_receive_threadproc, x, 0, 0, 0, &x->x_systhread);
	}	
}

void jit_bds_receive_stop_thread(t_jit_bds_receive *x)
{
	unsigned int ret;
    if (x->x_systhread) {
		critical_enter(0);
		x->x_systhread_cancel = true;										// tell the thread to stop
		critical_exit(0);
	
		post("waiting for thread to stop...");
		systhread_terminate(x->x_systhread);	// brutal, kill it because join isn't working.
//		systhread_join(x->x_systhread, &ret);									// wait for the thread to stop
		x->x_systhread = NULL;
	}
}

// -----------------------------+---------------+----------------------------
void * jit_bds_receive_threadproc(t_jit_bds_receive *x) {
	bool cancel = false;
	int index, counter; //, count;
	int bytesRx, addr_len;
	char buf[9000];
	struct sockaddr_in client_addr;	// who is sending to us?
	uint64_t start_t, end_t;	// measure the time of the receive code block
	uint64_t avg_t;
	
	mach_timebase_info_data_t info = {0,0};
	mach_timebase_info(&info);
	uint64_t t_mod = info.numer / info.denom;	// calculate the mach_time converter
	
//	buf = malloc(9000);	// ready for jumbo frames!
//	buf.dataLength = 9000;
	
	addr_len = sizeof(struct sockaddr);
	
	// loop until told to stop
	while (!cancel) {
		
//		bytesRx = 0;	// data received
//		count = 0;
		
		if (x && x->socket)
		{
			counter = 0;
			avg_t = 0;
			systhread_mutex_lock(&x->mutex);
			do {				
				bytesRx = recvfrom(x->socket,buf,9000,0, (struct sockaddr *)&client_addr, &addr_len);
				//		
				//		
				//		recv_data[bytes_read] = '\0';
				//		
				//		printf("\n(%s , %d) said : ",inet_ntoa(client_addr.sin_addr),
				//			   ntohs(client_addr.sin_port));
				//		printf("%s", recv_data);
//				bytesRx = enet_socket_receive(x->socket, &x->address, &buf, 1);				
				if (bytesRx > 3)
				{
					start_t = mach_absolute_time();
//					if (bytesRx > 1440)
//						post("got a packet of size %d", bytesRx);
					memcpy(&index, &buf[0], 4);
					
					bytesRx -= 4;
					if (index >= 0 && index+bytesRx < x->matSize)	// sanity check on the data
					{
						memcpy(x->myMatrix+index, &buf[4], bytesRx);
						x->newMatrix = true;						// set to tell matrix_calc thread that we have new data to move
		//				qelem_set(x->x_qelem);													// notify main thread using qelem mechanism
					}
					end_t = mach_absolute_time();
					avg_t += (end_t - start_t);		
					counter++;
				}
			} while (bytesRx > 0); // && count++ < 256);
			systhread_mutex_unlock(&x->mutex);
			if (counter > 0)
			{
				avg_t = (avg_t / counter) * t_mod * 1.1f;	// convert to nanoseconds, increase by 110% to account for other process calls...
				avg_t = (avg_t < 30000 ? avg_t : 30000);	// don't make server wait longer than 30 microseconds
				sendto(x->socket,&avg_t,8,0, (struct sockaddr *)&client_addr, addr_len);	// tell server how long it takes to recv a packet
				post("suggesting wait time %d\n", avg_t);
			}
		}		
//		systhread_sleep(1);					// TODO: sleep a bit ----- might be a good idea, to save CPU temp
		critical_enter(0);
		cancel = x->x_systhread_cancel;		// test if we're being asked to die
		critical_exit(0);
	}
	
//	free(buf);	// cleanup buffer space
	
	post("exiting thread");
	systhread_exit(0);														// this can return a value to systhread_join();
	return NULL;
}
// -----------------------------+---------------+----------------------------
// triggered by the helper thread
void jit_bds_receive_qfn(t_jit_bds_receive *x) {
//	int	 myfoo;
//	critical_enter(0);
//	myfoo = x->foo;															// manipulate threaded data
//	critical_exit(0);
	
//	outlet_int(x->x_outlet, myfoo);
}

