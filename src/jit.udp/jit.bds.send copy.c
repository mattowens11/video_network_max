#include "jit.common.h"
#include "math.h"
//#include "network.h"
#include "ext_systhread.h"
// below all for network stuff
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>


typedef struct _jit_bds_send 
{
	t_object				ob;			// this is where Max stores info about the class
	void* x_systhread;	// thread id
	bool x_systhread_cancel; //thread cancel flag
	void* x_qelem;	// for message passing between threads
	t_systhread_mutex mutex;	// mutex for copying into myMatrix
//	long					startcount, endcount, chebycount;
//	double					start[4], end[4], cheby[64];
	char* buffer;		// for receiving and sending packets
	char* myMatrix;		// for building the matrix internally before posting -- requires critical(1) to touch, as does newMatrix
//	bool					connected, 
//	bool		newMatrix;			// are we connected, and as a server or client?, and do we have new matrix data to send out?
//	ENetHost * host;		// handles to enet peer and host
//	ENetPeer * peer;
//	ENetEvent myEvent;
	t_symbol*	ip;
	int		port;
	char	halfscan;	// flag to set sending alternate lines
	bool	oddEven;	// if halfscan = true then switch which lines we're sending
	int socket;	// trying a more brute force approach...
//	ENetAddress address;
	struct sockaddr_in server_addr;
	int						packetSize;
	struct timespec	sleep_time;	// amount of time to sleep between packet sends, in microseconds
} t_jit_bds_send;

void *_jit_bds_send_class;

t_jit_bds_send *jit_bds_send_new(void);		// creates a new class for us, allocates memory
void jit_bds_send_free(t_jit_bds_send *x);			// destructor
t_jit_err jit_bds_send_matrix_calc(t_jit_bds_send *x, void *inputs, void *outputs);
//void jit_bds_send_calculate_ndim(t_jit_bds_send *x, long dimcount, long *dim, long planecount, 
//	t_jit_matrix_info *out_minfo, char *bop);
//void jit_bds_send_connect(t_jit_bds_send *x, t_symbol* ip, int port);	// connect to host 'IP' as a client
void jit_bds_send_connect(t_jit_bds_send *x); //, int port);	// start as a server
void jit_bds_send_disconnect(t_jit_bds_send *x);		// close sockets
//void jit_bds_send_bang(t_jit_bds_send *x);
// thread functions below
void jit_bds_send_start_thread(t_jit_bds_send *x);
void jit_bds_send_stop_thread(t_jit_bds_send *x);
void* jit_bds_send_threadproc(t_jit_bds_send *x);
void jit_bds_send_qfn(t_jit_bds_send *x);
void jit_bds_send_packetsize(t_jit_bds_send *x, int packetSize);	// set the moximum packet size used for UDP transmission

t_jit_err jit_bds_send_init(void);


t_jit_err jit_bds_send_init(void) 
{
	long attrflags=0;
	t_jit_object *attr;
	t_jit_object *mop;
	
	_jit_bds_send_class = jit_class_new("jit_bds_send",(method)jit_bds_send_new,(method)jit_bds_send_free, sizeof(t_jit_bds_send),0L);

	//add mop
	mop = jit_object_new(_jit_sym_jit_mop,1,1);
	jit_mop_single_type(mop, _jit_sym_char);
	jit_mop_single_planecount(mop, 4);
//	o=jit_object_method(mop,_jit_sym_getoutput,1);
//	jit_attr_setlong(o,_jit_sym_planelink,0);
	jit_class_addadornment(_jit_bds_send_class,mop);
	//add methods
	jit_class_addmethod(_jit_bds_send_class, (method)jit_bds_send_matrix_calc, "matrix_calc", A_CANT, 0L);
//	jit_class_addmethod(_jit_bds_send_class, (method)jit_bds_send_connect, "connect", A_SYM, A_LONG, 0L);
	jit_class_addmethod(_jit_bds_send_class, (method)jit_bds_send_connect, "connect", 0L); //A_LONG
	jit_class_addmethod(_jit_bds_send_class, (method)jit_bds_send_disconnect, "disconnect", 0L); //A_LONG
	jit_class_addmethod(_jit_bds_send_class, (method)jit_bds_send_packetsize, "packetsize", A_LONG, 0L);
//	jit_class_addmethod(_jit_bds_send_class, (method)jit_bds_send_bang, "bang", 0L);

	//add attributes	
	attrflags = JIT_ATTR_GET_DEFER_LOW | JIT_ATTR_SET_USURP_LOW;

	// start - beginning bds_send cell
	attr = jit_object_new(_jit_sym_jit_attr_offset, "port", _jit_sym_long, 
						  attrflags, (method)0L, (method)0L, calcoffset(t_jit_bds_send, port));
	jit_class_addattr(_jit_bds_send_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset, "ip", _jit_sym_symbol,
						  attrflags, (method)0L, (method)0L, calcoffset(t_jit_bds_send, ip));
	jit_class_addattr(_jit_bds_send_class,attr);
	attr = jit_object_new(_jit_sym_jit_attr_offset, "halfscan", _jit_sym_char,
						  attrflags, (method)0L, (method)0L, calcoffset(t_jit_bds_send, halfscan));
	jit_class_addattr(_jit_bds_send_class,attr);

	jit_class_register(_jit_bds_send_class);			// register the class with Max for dynamic binding

	return JIT_ERR_NONE;
}


t_jit_err jit_bds_send_matrix_calc(t_jit_bds_send *x, void *inputs, void *outputs)
{
	t_jit_err err=JIT_ERR_NONE;
	long in_savelock; //, matrixSize; //dimmode, out_savelock
	t_jit_matrix_info in_minfo; //,out_minfo;
	char *in_bp; //,*out_bp;
	long i,remaining,stepSize,loopStart;
	uint64_t temp; //planecount,dim[JIT_MATRIX_MAX_DIMCOUNT];
	void *in_matrix; // *out_matrix;
//	ENetBuffer buf;
	int addr_len, bytesRx;
	struct timespec t_temp;

	in_matrix = jit_object_method(inputs,_jit_sym_getindex,0);
//	out_matrix 	= jit_object_method(outputs,_jit_sym_getindex,0);
	if (x)
	{
		if(in_matrix) //x->connected&&
		{
//			critical_exit(0);
			in_savelock = (long) jit_object_method(in_matrix,_jit_sym_lock,1);
			jit_object_method(in_matrix,_jit_sym_getinfo,&in_minfo);
			jit_object_method(in_matrix,_jit_sym_getdata,&in_bp);
			if (!in_bp) { err=JIT_ERR_GENERIC; goto out;}
			
//			dimcount   = in_minfo.dimcount;
//			matrixSize = in_minfo.planecount;		
//			for (i=0;i<dimcount;i++) {
//				matrixSize *= in_minfo.dim[i];
//			}
			addr_len = sizeof(struct sockaddr);
//			buf.data = x->buffer;
//			buf.dataLength = x->packetSize;
			stepSize = (x->halfscan ? (x->packetSize-4)*2 : x->packetSize-4);	// are we alternating lines? set loop step size appropriately
			x->oddEven = !x->oddEven;
			loopStart = (x->oddEven && x->halfscan ? 0 : x->packetSize-4);		// if halfscan then determine which half of the lines to send
			for (i = loopStart; i < in_minfo.size; i += stepSize)
			{
				remaining = in_minfo.size - i;
				remaining = (remaining > x->packetSize-4 ? x->packetSize-4 : remaining);
				if (remaining > 0)
				{
//					systhread_mutex_lock(&x->mutex);	// get the lock to prevent threading errors
					memcpy(x->buffer, &i, 4);	// copy in the header int
					memcpy(x->buffer+4, in_bp+i, remaining);	// copy the rest of the data
//					enet_socket_send(x->socket, &x->address, &buf, 1);
					//	while (1)
					//	{
					//		
					//		printf("Type Something (q or Q to quit):");
					//		gets(send_data);
					//		
					//		if ((strcmp(send_data , "q") == 0) || strcmp(send_data , "Q") == 0)
					//			break;
					//		
					//		else
					sendto(x->socket, x->buffer, remaining+4, 0,
						   (struct sockaddr *)&x->server_addr, addr_len);
					//	}
//					systhread_mutex_unlock(&x->mutex);	// get the lock to prevent threading errors
					
//					count = 0;
//					do {
						// wait for confirmation...
					bytesRx = recv(x->socket,&temp,8,0); //, (struct sockaddr *)&client_addr, &addr_len);	// DON'T BLOCK, not a critical recv
					if (bytesRx == 8)	// we got a time from the client
						x->sleep_time.tv_nsec = x->sleep_time.tv_nsec * 0.95f + temp * 0.05f;	// low pass filter the new input with the stored sleep_time & convert temp to microseconds
					nanosleep(&(x->sleep_time), &t_temp);	// now sleep a little
					
//					} while (bytesRx == 0);// && count++ < 16384);	// time out, in case of packet loss or disconnect
					
//					post("sending packet %d", buf.dataLength);
				}
			}
//			systhread_mutex_lock(&x->mutex);
//			sendData(x->host, in_bp, matrixSize, x->packetSize);
//			systhread_mutex_unlock(&x->mutex);
		} 
//		else if (x->connected&&!x->isServer&&out_matrix)
//		{
////			critical_exit(0);
//			out_savelock = (long) jit_object_method(out_matrix,_jit_sym_lock,1);
//			jit_object_method(out_matrix,_jit_sym_getinfo,&out_minfo);
//			jit_object_method(out_matrix,_jit_sym_getdata,&out_bp);
//			if (!out_bp) { err=JIT_ERR_GENERIC; goto out;}
//			
//			//get dimensions/planecount
//			dimcount   = out_minfo.dimcount;
////				planecount = out_minfo.planecount;	
//			
//			matrixSize = out_minfo.planecount;		
//			for (i=0;i<dimcount;i++) {
////					dim[i] = MIN(in_minfo.dim[i],out_minfo.dim[i]);
//				matrixSize *= out_minfo.dim[i];
//			}
//			
//			systhread_mutex_lock(x->mutex);		// get the mutex lock before reading the matrix buffer
//			if (x->newMatrix)			// do we have new data?
//			{
//				memcpy(out_bp, x->myMatrix, matrixSize);	// TODO: confirm size of myMatrix and matrixSize
//				x->newMatrix = false;	// set flag until there is more data.
//			}
//			systhread_mutex_unlock(x->mutex);
//		} 
		else
		{
			return JIT_ERR_INVALID_PTR;
		}
		post("send sleep: %d", x->sleep_time.tv_nsec);
	}	// if (x)
out:
	jit_object_method(in_matrix,_jit_sym_lock,in_savelock);
//	jit_object_method(out_matrix,_jit_sym_lock,out_savelock);
	return err;
}

//------------------------- JIT_UDP_NEW -------- Constructor
t_jit_bds_send *jit_bds_send_new(void)
{
	t_jit_bds_send *x;
		
	if (x=(t_jit_bds_send *)jit_object_alloc(_jit_bds_send_class)) {
		x->packetSize = 1440;	// default packet size
		x->buffer = malloc(x->packetSize);	// buffer space for building packets
//		x->connected = false;
//		x->isServer = false;
//		x->host = 0x00;
//		x->peer = 0x00;
//		x->x_qelem = qelem_new(x,(method)jit_bds_send_qfn);
		x->x_systhread = NULL;
		x->myMatrix = 0x00;
//		x->newMatrix = false;
		x->socket = 0;
		x->ip = gensym("127.0.0.1");
		x->port = 8001;
		x->halfscan = false;
		x->oddEven = false;
		x->sleep_time.tv_sec = 0;
		x->sleep_time.tv_nsec = 0;
		systhread_mutex_new(&x->mutex, SYSTHREAD_MUTEX_NORMAL);
	} else {
		x = NULL;
	}	
	
	return x;
}

void jit_bds_send_free(t_jit_bds_send *x)
{
	jit_bds_send_stop_thread(x);
//	if (x->x_qelem)
//		qelem_free(x->x_qelem);
	if (x->socket != 0)
		close(x->socket);
//	if (x->host != 0x00)
//		enet_host_destroy(x->host);
//	x->host = 0x00;
//	x->peer = 0x00;
	free(x->buffer);
	if (x->myMatrix != 0x00)
		free(x->myMatrix);
	systhread_mutex_free(x->mutex);
	free(x->ip);
}
//------------------------------------------- End Destructor ----

// startup a client connection to the given ip and port
//void jit_bds_send_connect(t_jit_bds_send *x, t_symbol* ip, int port)
//{
//	if (x->host != 0x00)	// check for previous connection, if so then destroy it
//		enet_host_destroy(x->host);
//	
//	x->host = enet_host_create (NULL /* create a client host */,
//								1 /* only allow 1 outgoing connection */,
//							 0 /* I2 ... 500Mbps each way for the moment */,
//							 0);
//	
//    if (x->host == NULL)
//    {
//        post ("An error occurred while trying to create an ENet client host.\n");
//		x->connected = false;
//		return;
//    }
//	
//	// connect
//	ENetAddress address;
//	
//    /* Connect to some.server.net:1234. */
//    enet_address_set_host (&address, "127.0.0.1"); //"abe.ncsa.uiuc.edu");
//    address.port = port;
//	
//    /* Initiate the connection, allocating the two channels 0 and 1. */
//    x->peer = enet_host_connect (x->host, &address, 2);    
//    
//    if (x->peer == NULL)
//    {
//		post ("No available peers for initiating an ENet connection.\n");
//		x->connected = false;
//		//		exit (EXIT_FAILURE);
//    } else
//	{
//		/* Wait up to 1/2 second for the connection attempt to succeed. */
//		if (enet_host_service (x->host, &x->myEvent, 500) > 0 && x->myEvent.type == ENET_EVENT_TYPE_CONNECT)
//		{
//			x->connected = true;
//			x->isServer = false;
//			post ("Connection to %x:%d succeeded. Connected:%d", address.host, address.port, x->connected);
//			x->myMatrix = malloc(320*240*4);	// TODO: don't hardcode the size of the matrix buffer here!
//			jit_bds_send_start_thread(x);
//		}
//		else
//		{
//			/* Either the 1/2 second is up or a disconnect event was
//			 * received. Reset the peer in the event the time
//			 * had run out without any significant event.
//			 */
//			enet_peer_reset (x->peer);
//			
//			post ("Connection to %x:%d failed.", address.host, address.port);
//			x->connected = false;
//		}
//	}
//}

// startup a server
void jit_bds_send_connect(t_jit_bds_send *x) //, int port)
{
	jit_bds_send_disconnect(x);
	
//	int sock;
//	struct sockaddr_in server_addr;
	struct hostent *host;
//	char send_data[1024];
	
	host= (struct hostent *) gethostbyname(x->ip->s_name); //(char *)"127.0.0.1");
	
	if ((x->socket = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
	{
		post("Jit.bds.send: error creating socket");
		return;
	}
	
	x->server_addr.sin_family = AF_INET;
	x->server_addr.sin_port = htons(x->port);
	x->server_addr.sin_addr = *((struct in_addr *)host->h_addr);
	bzero(&(x->server_addr.sin_zero),8);
	
	// set non-blocking
	int flags;
	flags = fcntl(x->socket,F_GETFL,0);
	assert(flags != -1);
	fcntl(x->socket, F_SETFL, flags | O_NONBLOCK);
	
//	struct timeval tv;	// 4 lines to set a blocking timeout
//	
//	tv.tv_sec = 5 / 1000.f ;
//	tv.tv_usec = ( 1 % 1000) * 1000;
//	
//    setsockopt (x->socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof tv);
	
//	while (1)
//	{
//		
//		printf("Type Something (q or Q to quit):");
//		gets(send_data);
//		
//		if ((strcmp(send_data , "q") == 0) || strcmp(send_data , "Q") == 0)
//			break;
//		
//		else
//			sendto(sock, send_data, strlen(send_data), 0,
//				   (struct sockaddr *)&server_addr, sizeof(struct sockaddr));
//	}
	
	
//	enet_address_set_host(&x->address, x->ip->s_name);	// send-to socket address
////	x->address.host = ENET_HOST_ANY;
//	x->address.port = x->port;
//	
////	ENetAddress address;		// receiving socket address
////	address.host = ENET_HOST_ANY;
////	address.port = x->port;
//	
//	x->socket = enet_socket_create(ENET_SOCKET_TYPE_DATAGRAM, NULL); //&address);
	if (x->socket)
		post("ready to send to %s:%d", x->ip->s_name, x->port);
//	enet_socket_connect(x->socket, &x->address);
	
//	ENetAddress address;
//	
//	/* Bind the server to the default localhost.
//	 * A specific host address can be specified by
//	 * enet_address_set_host (& address, "x.x.x.x");
//	 */
//	address.host = ENET_HOST_ANY;
//	/* Bind the server to port */
//	address.port = port;
//	
//	x->host = enet_host_create (& address /* the address to bind the server host to */, 
//							 32 /* allow up to 32 clients and/or outgoing connections */,
//							 0 /* assume any amount of incoming bandwidth */,
//							 0 /* assume any amount of outgoing bandwidth */);
//	
//	if (x->host == NULL)
//	{
//		post ("An error occurred while trying to create an ENet server host.\n");
//		x->connected = false;
//	} else
//	{
//		post("Server host created:%x", x->host);
//		x->connected = true;
////		x->isServer = true;
//		
//		jit_bds_send_start_thread(x);	// thread test!
//	}
}
// close connections
void jit_bds_send_disconnect(t_jit_bds_send *x) //, int port)
{
	if (x->socket)	// cleanup old socket
		close(x->socket);
}

// set the maximum packet size for UDP transmission.
void jit_bds_send_packetsize(t_jit_bds_send *x, int packetSize)
{
	if (x && x->packetSize != packetSize)	// if same size then don't bother
	{
		x->packetSize = packetSize;
		
		systhread_mutex_lock(&x->mutex);	// get the lock to prevent threading errors
		free(x->buffer);
		x->buffer = malloc(x->packetSize);
		systhread_mutex_unlock(&x->mutex);
	}
}

//void jit_bds_send_bang(t_jit_bds_send *x)
//{
//	int bytesRx = 0;	// data received
//	
//	if (x && x->connected)
//	{
//		post(".");
//		bytesRx = serviceNetwork(x->host, &x->myEvent, x->buffer);
//		if (bytesRx > 0)
//		{
//			post("got a packet of size %d", bytesRx);
//		}
//	}
//}

// threading----------------

void jit_bds_send_start_thread(t_jit_bds_send *x)
{
	jit_bds_send_stop_thread(x);														// kill thread if, any
	
	// create new thread + begin execution
	if (x->x_systhread == NULL) {
		x->x_systhread_cancel = false;	// reset cancel flag
		// stacksize, priority and flags are ignored for now.
		systhread_create((method) jit_bds_send_threadproc, x, 0, 0, 0, &x->x_systhread);
	}	
}

void jit_bds_send_stop_thread(t_jit_bds_send *x)
{
	unsigned int ret;
    if (x->x_systhread) {
		critical_enter(0);
		x->x_systhread_cancel = true;										// tell the thread to stop
		critical_exit(0);
	
//		systhread_terminate(x->x_systhread);	// brutal, kill it because join isn't working.
		systhread_join(x->x_systhread, &ret);									// wait for the thread to stop
		x->x_systhread = NULL;
	}
}

// -----------------------------+---------------+----------------------------
void * jit_bds_send_threadproc(t_jit_bds_send *x) {
	bool cancel = false;
//	int index;
	int bytesRx;
	
//	char* buf = malloc(1404);		// space to receive into. not very important for the sending object
	
	// loop until told to stop
	while (!cancel) {
		critical_enter(0);
		cancel = x->x_systhread_cancel;										// test if we're being asked to die
//		x->foo++;															// fiddle with data
		critical_exit(0);
		
		bytesRx = 0;	// data received
		
		if (x && x->socket) //connected)
		{
			systhread_mutex_lock(&x->mutex);
			do {
//				bytesRx = serviceNetwork(x->host, &x->myEvent, buf, 1404);
				if (bytesRx > 0)
				{
					post("got a packet of size %d", bytesRx);
//					memcpy(&index, buf, 4);
//					systhread_mutex_lock(&x->mutex);
//					if (index >= 0 && index < 307200)
//						memcpy(x->myMatrix+index, buf+4, bytesRx-4);
//					x->newMatrix = true;
	//				qelem_set(x->x_qelem);													// notify main thread using qelem mechanism
				}
			} while (bytesRx > 0);
			systhread_mutex_unlock(&x->mutex);
		}
//		critical_exit(0);
		
		
		// do this _after notifying main thread, otherwise we lose this
		// loop's worth of "data"
//		if (cancel)
//			break;
		
//		systhread_sleep(1);													// sleep a bit
	}
	
//	critical_enter(0);
//	x->x_systhread_cancel = false;											// reset cancel flag for next time, in case
//	// the thread is created again
//	critical_exit(0);
	post("exiting thread");
//	systhread_terminate(systhread_self());
	systhread_exit(0);														// this can return a value to systhread_join();
	return NULL;
}
// -----------------------------+---------------+----------------------------
// triggered by the helper thread
void jit_bds_send_qfn(t_jit_bds_send *x) {
//	int	 myfoo;
//	critical_enter(0);
//	myfoo = x->foo;															// manipulate threaded data
//	critical_exit(0);
	
//	outlet_int(x->x_outlet, myfoo);
}

