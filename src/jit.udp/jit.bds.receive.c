#include "jit.common.h"
#include "math.h"
//#include "network.h"
#include "ext_systhread.h"

// includes for network stuff
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <mach/mach_time.h>	// for measuring time of receive/copy code and attempt to sync on the sender/server

// TODO: add disconnect functions, ensure "size x y" works

typedef struct _jit_bds_receive 
{
	t_object				ob;			// this is where Max stores info about the class
	void* x_systhread;	// thread id
	bool x_systhread_cancel; //thread cancel flag
	void* x_qelem;	// for message passing between threads
	t_systhread_mutex mutex;	// mutex for copying into myMatrix
	char* buffer;		// for receiving and sending packets
	char* myMatrix;		// for building the matrix internally before posting -- used as a ring buffer, so no mutex is required
	long	readIndex, writeIndex;	// offsets into myMatrix for reading and writing as a ring buffer
	bool	connected, newMatrix;			// are we connected, and as a server or client?, and do we have new matrix data to send out?
//	ENetHost * host;		// handles to enet peer and host
//	ENetPeer * peer;
//	ENetEvent myEvent;
	int socket, inSocket;
//	ENetAddress address;
	struct sockaddr_in		server_addr;
	int						packetSize, port;
	long					matSize;			// number of bytes in the matrix we're receiving
	bool			UDP;	// UDP or TCP?
} t_jit_bds_receive;

void *_jit_bds_receive_class;

t_jit_bds_receive *jit_bds_receive_new(void);		// creates a new class for us, allocates memory
void jit_bds_receive_free(t_jit_bds_receive *x);			// destructor
t_jit_err jit_bds_receive_matrix_calc(t_jit_bds_receive *x, void *inputs, void *outputs);
//void jit_bds_receive_calculate_ndim(t_jit_bds_receive *x, long dimcount, long *dim, long planecount, 
//	t_jit_matrix_info *out_minfo, char *bop);
void jit_bds_receive_connect(t_jit_bds_receive *x); //, t_symbol* ip, int port);	// connect to host 'IP' as a client
void jit_bds_receive_disconnect(t_jit_bds_receive *x); //, t_symbol* ip, int port);	// close sockets, stop threads
//void jit_bds_receive_server(t_jit_bds_receive *x, int port);	// start as a server
//void jit_bds_receive_bang(t_jit_bds_receive *x);
// thread functions below
void jit_bds_receive_start_thread(t_jit_bds_receive *x);
void jit_bds_receive_stop_thread(t_jit_bds_receive *x);
void* jit_bds_receive_threadproc(t_jit_bds_receive *x);
void jit_bds_receive_qfn(t_jit_bds_receive *x);
//void jit_bds_receive_packetsize(t_jit_bds_receive *x, int packetSize);	// set the moximum packet size used for UDP transmission

t_jit_err jit_bds_receive_init(void);


t_jit_err jit_bds_receive_init(void) 
{
	long attrflags=0;
	t_jit_object *attr;
	t_jit_object *mop;
	
	_jit_bds_receive_class = jit_class_new("jit_bds_receive",(method)jit_bds_receive_new,(method)jit_bds_receive_free, sizeof(t_jit_bds_receive),0L);

	//add mop
	mop = jit_object_new(_jit_sym_jit_mop,1,1);
	jit_mop_single_type(mop, _jit_sym_char);
	jit_mop_single_planecount(mop, 4);
//	o=jit_object_method(mop,_jit_sym_getoutput,1);
//	jit_attr_setlong(o,_jit_sym_planelink,0);
	jit_class_addadornment(_jit_bds_receive_class,mop);
	//add methods
	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_matrix_calc, "matrix_calc", A_CANT, 0L);
	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_connect, "connect", 0L); // A_SYM, A_LONG,
	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_disconnect, "disconnect", 0L); // A_SYM, A_LONG,
//	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_server, "server", A_LONG, 0L);
//	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_packetsize, "packetsize", A_LONG, 0L);
//	jit_class_addmethod(_jit_bds_receive_class, (method)jit_bds_receive_bang, "bang", 0L);

	//add attributes	
	attrflags = JIT_ATTR_GET_DEFER_LOW | JIT_ATTR_SET_USURP_LOW;

	// start - beginning udp cell
	attr = jit_object_new(_jit_sym_jit_attr_offset, "port", _jit_sym_long, 
		attrflags, (method)0L, (method)0L, calcoffset(t_jit_bds_receive, port));
	jit_class_addattr(_jit_bds_receive_class,attr);

	jit_class_register(_jit_bds_receive_class);			// register the class with Max for dynamic binding

	return JIT_ERR_NONE;
}


t_jit_err jit_bds_receive_matrix_calc(t_jit_bds_receive *x, void *inputs, void *outputs)
{
	t_jit_err err=JIT_ERR_NONE;
	long out_savelock; //, matrixSize; //dimmode, in_savelock
	t_jit_matrix_info out_minfo; //in_minfo
	char *out_bp; //*in_bp
//	long i,dimcount; //planecount,dim[JIT_MATRIX_MAX_DIMCOUNT];
	void *out_matrix; // *in_matrix
	int	packetSize, index;	// temp variables for unpacking myMatrix
	
//	in_matrix = jit_object_method(inputs,_jit_sym_getindex,0);
	out_matrix 	= jit_object_method(outputs,_jit_sym_getindex,0);
	
	if (x && x->socket)	// make sure we're connected
	{
		if (out_matrix) //x->connected&&
		{
//			critical_exit(0);
			out_savelock = (long) jit_object_method(out_matrix,_jit_sym_lock,1);
			jit_object_method(out_matrix,_jit_sym_getinfo,&out_minfo);
			jit_object_method(out_matrix,_jit_sym_getdata,&out_bp);
			if (!out_bp) { err=JIT_ERR_GENERIC; goto out;}
			
			//get dimensions/planecount
//			dimcount   = out_minfo.dimcount;
////				planecount = out_minfo.planecount;	
//			
//			matrixSize = out_minfo.planecount;		
//			for (i=0;i<dimcount;i++) {
////					dim[i] = MIN(in_minfo.dim[i],out_minfo.dim[i]);
//				matrixSize *= out_minfo.dim[i];
//			}
			
			// check matrix size and resize buffer. 2/13/2011 -- setting myMatrix really big, hopefully big enough to avoid overflows
//			if (out_minfo.size * 1.1f > x->matSize)	// assign new space for our internal matrix
//			{
//				systhread_mutex_lock(x->mutex);		// get the mutex lock before reading the matrix buffer
//				if (x->myMatrix != 0x00)
//					free(x->myMatrix);
//				x->matSize = out_minfo.size * 1.1f;	// make some extra space for header bytes and stuff
//				x->myMatrix = malloc(x->matSize);
//				for (index = 0; index < 4; index++)
//					x->myMatrix[index] = 0;	// set the start int to 0 so the read head doesn't go wild.
//				x->readIndex = 0;
//				x->writeIndex = 0;
//				systhread_mutex_unlock(x->mutex);		// get the mutex lock before reading the matrix buffer
//			} else 
			
//			systhread_mutex_lock(x->mutex);
//			writeIndex = x->writeIndex;
//			systhread_mutex_unlock(x->mutex);
			do // (x->readIndex != writeIndex)			// do we have new data? ... could we get stuck here somehow? ...
			{
				memcpy(&packetSize, x->myMatrix+x->readIndex, 4);
				if (packetSize > 3)
				{
					memcpy(&index, x->myMatrix+x->readIndex+4, 4);
					packetSize -= 4;	// take off 4 for the index we just read out
					if (index >= 0 && index + packetSize < out_minfo.size)	// sanity check on index and packet size
					{
						memcpy(out_bp+index, x->myMatrix+x->readIndex+8, packetSize);
						systhread_mutex_lock(x->mutex);
						x->readIndex += packetSize+8;
						systhread_mutex_unlock(x->mutex);
					} else  //else we're screwed, head for the light!!
					{
						systhread_mutex_lock(x->mutex);
						x->readIndex = 0;	// will this save us? Eventually, once the write head loops around again, hopefully
						systhread_mutex_unlock(x->mutex);
						packetSize = 0;	// to bail out of the do/while loop
					}
				} else if (packetSize == -1)
				{
					systhread_mutex_lock(x->mutex);
					x->readIndex = 0;
					systhread_mutex_unlock(x->mutex);
					packetSize = 1;	// to trigger another do/while iteration
				}
			} while (packetSize > 0);
		} else
		{
			return JIT_ERR_INVALID_PTR;
		}
	}	// if (x)
out:
//	jit_object_method(in_matrix,_jit_sym_lock,in_savelock);
	jit_object_method(out_matrix,_jit_sym_lock,out_savelock);
	return err;
}

//------------------------- JIT_UDP_NEW -------- Constructor
t_jit_bds_receive *jit_bds_receive_new(void)
{
	t_jit_bds_receive *x;
	
//	enet_initialize();	// make sure enet is up and ready to roll!
		
	if (x=(t_jit_bds_receive *)jit_object_alloc(_jit_bds_receive_class)) {
		x->packetSize = 1440;	// default packet size
		x->buffer = malloc(x->packetSize);	// buffer space for building packets
		x->connected = false;
//		x->isServer = false;
//		x->host = 0x00;
//		x->peer = 0x00;
//		x->x_qelem = qelem_new(x,(method)jit_bds_receive_qfn);
		x->x_systhread = NULL;
		x->matSize = 10000000;	// set to a really big size, to hold many packets
		x->myMatrix = malloc(x->matSize);
		x->newMatrix = false;
		x->readIndex = 0;
		x->writeIndex = 0;
		x->socket = -1;
		x->inSocket = -1;
		x->port = 8001;
		x->UDP = false;
		systhread_mutex_new(&x->mutex, SYSTHREAD_MUTEX_NORMAL);
	} else {
		x = NULL;
	}	
	
	return x;
}

void jit_bds_receive_free(t_jit_bds_receive *x)
{
	jit_bds_receive_stop_thread(x);
	jit_bds_receive_disconnect(x);
//	if (x->x_qelem)
//		qelem_free(x->x_qelem);
	
//	if (x->socket != 0)
//		enet_socket_destroy(x->socket);
//	if (x->host != 0x00)
//		enet_host_destroy(x->host);
//	x->host = 0x00;
//	x->peer = 0x00;
	free(x->buffer);
	if (x->myMatrix != 0x00)
		free(x->myMatrix);
	systhread_mutex_free(x->mutex);
	
//	enet_deinitialize();	// cleanup enet
}
//------------------------------------------- End Destructor ----

// startup a client connection to the given ip and port
void jit_bds_receive_connect(t_jit_bds_receive *x) //, t_symbol* ip, int port)
{	
	jit_bds_receive_disconnect(x);
	systhread_mutex_lock(&x->mutex);	// ensure complete control of the sockets
	
	if (x->UDP)
	{
		if ((x->socket = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
			post("jit.bds.receive: error creating socket");
			return;
		}
	} else
		if ((x->socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
			post("jit.bds.receive: error creating socket");
			return;
		}
		
	x->server_addr.sin_family = AF_INET;
	x->server_addr.sin_port = htons(x->port);
	x->server_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(x->server_addr.sin_zero),8);
	
	if (bind(x->socket,(struct sockaddr *)&x->server_addr,
			 sizeof(struct sockaddr)) == -1)
	{
		post("jit.bds.receive: unable to bind socket");
		return;
	}
	
	if (!x->UDP)
		if (listen(x->socket, 5) < 0)	// 5 = maximum outstanding connection requests
		{
			post("jit.bds.receive: error setting listen(socket)");
			return;
		}
	
	// set non-blocking -- this will let it run the core to max. but blocking causes a crash when systhread_terminate is called
	int flags;
	flags = fcntl(x->socket,F_GETFL,0);
	assert(flags != -1);
	fcntl(x->socket, F_SETFL, flags | O_NONBLOCK);
	
//	struct timeval tv;	// 4 lines to set a blocking timeout
//	
//	tv.tv_sec = 0.1f; //5 / 1000.f ;
//	tv.tv_usec = 0;
//	
//    setsockopt (x->socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof tv);
	
	post("jit.bds.receive: ready to receive on port %d.", x->port);
	
//	critical_enter(0);
//	
//	if (x->socket)	// cleanup old connection
//		enet_socket_destroy(x->socket);
//	
//	enet_address_set_host(&x->address, "127.0.0.1");	//send-to socket address
//	//	x->address.host = ENET_HOST_ANY;
//	x->address.port = x->port;
//	
//	ENetAddress address;								//receiving socket address
////	enet_address_set_host(&address, "127.0.0.1");
//	address.host = ENET_HOST_ANY;
//	address.port = x->port;
//	
//	x->socket = enet_socket_create(ENET_SOCKET_TYPE_DATAGRAM, &address);
////	post("socket : %d", x->socket);
//	
//	critical_exit(0);
	systhread_mutex_unlock(&x->mutex);	// let the world run again!
	
	jit_bds_receive_start_thread(x);
}

// startup a client connection to the given ip and port
void jit_bds_receive_disconnect(t_jit_bds_receive *x) //, t_symbol* ip, int port)
{
	jit_bds_receive_stop_thread(x);	// stop the reception thread
	
	systhread_mutex_lock(&x->mutex);	// stop socket use.
	if (x->socket)	// cleanup old connection
		close(x->socket);
	x->socket = 0;
	if (x->inSocket)
		close(x->inSocket);
	x->inSocket = 0;
	x->connected = false;
	systhread_mutex_unlock(&x->mutex);	// let the world run again!
}

// threading----------------

void jit_bds_receive_start_thread(t_jit_bds_receive *x)
{
//	jit_bds_receive_stop_thread(x);														// kill thread if, any
	
	// create new thread + begin execution
	if (x->x_systhread == NULL) {
		x->x_systhread_cancel = false;	// reset cancel flag
		// stacksize, priority and flags are ignored for now.
		systhread_create((method)jit_bds_receive_threadproc, x, 0, 0, 0, &x->x_systhread);
	}	
}

void jit_bds_receive_stop_thread(t_jit_bds_receive *x)
{
//	unsigned int ret;
    if (x->x_systhread) {
		critical_enter(0);
		x->x_systhread_cancel = true;										// tell the thread to stop
		critical_exit(0);
	
//		post("waiting for thread to stop...");
		systhread_terminate(x->x_systhread);	// brutal, kill it because join isn't working.
//		systhread_join(x->x_systhread, &ret);									// wait for the thread to stop
		x->x_systhread = NULL;
	}
}

// -----------------------------+---------------+----------------------------
void * jit_bds_receive_threadproc(t_jit_bds_receive *x) {
	bool cancel = false;
	int counter; //, count;
	int minusOne = -1;
	int zero = 0;
	int bytesRx, addr_len, readIndex;
	char buf[9000];
	struct sockaddr_in client_addr;	// who is sending to us?
#ifdef WAITTIME
	uint64_t start_t, end_t;	// measure the time of the receive code block
#endif
	uint64_t avg_t;
	
	mach_timebase_info_data_t info = {0,0};
	mach_timebase_info(&info);
	uint64_t t_mod = info.numer / info.denom;	// calculate the mach_time converter
	
//	buf = malloc(9000);	// ready for jumbo frames!
//	buf.dataLength = 9000;
	
	counter = 0;
	avg_t = 0;
	addr_len = sizeof(struct sockaddr);
	
	// loop until told to stop
	while (!cancel) {
		
//		bytesRx = 0;	// data received
//		count = 0;
		
		if (x && x->socket)
		{
			if (!x->UDP && !x->connected)	// wait for a connection
			{
				if ((x->inSocket = accept(x->socket, (struct sockaddr *) &client_addr, &addr_len)) >= 0)
				{
					post("jit.bds.receive: received connection");
					x->connected = true;
				}
			}
			if (x->UDP || x->connected) {
				do {			
					if (x->UDP)
						bytesRx = recvfrom(x->socket,buf,9000,0, (struct sockaddr *)&client_addr, &addr_len);
					else
						bytesRx = recv(x->inSocket, buf, 9000, 0);
					
					if (bytesRx > 4)
					{
#ifdef WAITTIME
						start_t = mach_absolute_time();
#endif
						
	//					systhread_mutex_lock(x->mutex);	// need mutex to read/write myMatrix... because main thread may resize the buffer. TODO: set the buffer at start, then leave alone
						if (x->writeIndex + bytesRx + 8 > x->matSize)		// would we go past the end? start over again. bytesRx + 8 for the length and a -1 at the end
						{
							memcpy(x->myMatrix+x->writeIndex, &minusOne, 4);	// set a -1 at the end to tell the readIndex to jump back to 0 when it gets here
							x->writeIndex = 0;
						}
						systhread_mutex_lock(&x->mutex);
						readIndex = x->readIndex;
						systhread_mutex_unlock(&x->mutex);
						
						if (x->writeIndex < readIndex && x->writeIndex + bytesRx + 8 > readIndex)
							bytesRx = 0;	// can't proceed, it would overflow the buffer
						else	// else we're good and we copy this packet into the ring buffer
						{
							memcpy(x->myMatrix+x->writeIndex+4+bytesRx, &zero, 4);	// set a 0 at the end,
							memcpy(x->myMatrix+x->writeIndex+4, &buf, bytesRx);	// then copy in the packet,
							memcpy(x->myMatrix+x->writeIndex, &bytesRx, 4);	// then copy in the packet size, otherwise the Read thread could pass us by and get lost
							
							x->writeIndex += bytesRx + 4;		// increment the counter
							
#ifdef WAITTIME
							end_t = mach_absolute_time();
							avg_t += (end_t - start_t);		
							counter++;
#endif
						}
	//					systhread_mutex_unlock(x->mutex);
	//					memcpy(&index, &buf[0], 4);
	//					
	//					bytesRx -= 4;
	//					if (index >= 0 && index+bytesRx < x->matSize)	// sanity check on the data
	//					{
	//						memcpy(x->myMatrix+index, &buf[4], bytesRx);
	//						x->newMatrix = true;						// set to tell matrix_calc thread that we have new data to move
	//		//				qelem_set(x->x_qelem);													// notify main thread using qelem mechanism
	//					}
					}
				} while (bytesRx > 4 && counter < 10001);
			}	// UDP || connected
#ifdef WAITTIME
			if (counter > 10000)
			{
				avg_t = (avg_t / counter) * t_mod * 1.1f;	// convert to nanoseconds, increase by 110% to account for other process calls...
				avg_t = (avg_t < 30000 ? avg_t : 30000);	// don't make server wait longer than 30 microseconds
				sendto(x->socket,&avg_t,8,0, (struct sockaddr *)&client_addr, addr_len);	// tell server how long it takes to recv a packet
				post("suggesting wait time %d\n", avg_t);
				counter = 0;
				avg_t = 0;
			}
#endif
		}		
//		systhread_sleep(1);					// TODO: sleep a bit ----- might be a good idea, to save CPU temp
		critical_enter(0);
		cancel = x->x_systhread_cancel;		// test if we're being asked to die
		critical_exit(0);
	}
	
//	free(buf);	// cleanup buffer space
	
	post("exiting thread");
	systhread_exit(0);														// this can return a value to systhread_join();
	return NULL;
}
// -----------------------------+---------------+----------------------------
// triggered by the helper thread
void jit_bds_receive_qfn(t_jit_bds_receive *x) {
//	int	 myfoo;
//	critical_enter(0);
//	myfoo = x->foo;															// manipulate threaded data
//	critical_exit(0);
	
//	outlet_int(x->x_outlet, myfoo);
}

