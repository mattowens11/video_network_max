/*
 *  network.h
 *  
 *
 *  Created by Ben Smith on 11/14/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "../../../enet/include/enet.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//	char buffer[512];

//bool createEnetServer(ENetPeer* peer, ENetHost* host);	// start a connection, as server host or client
//void createEnetClient(); //ENetPeer* peer, ENetHost* host);

int serviceNetwork(ENetHost* host, ENetEvent* event, char* data, long dataSize);	//-- check for new data, returns # of bytes received and stored in data
void destroyEnetPeer(ENetHost* host);	// clean up

void sendData(ENetHost* host, char *data, int size, int packet_size);