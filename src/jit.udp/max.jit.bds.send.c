#include "jit.common.h"
#include "max.jit.mop.h"

typedef struct _max_jit_bds_send 
{
	t_object		ob;
	void			*obex;
} t_max_jit_bds_send;

t_jit_err jit_bds_send_init(void); 

void *max_jit_bds_send_new(t_symbol *s, long argc, t_atom *argv);
void max_jit_bds_send_free(t_max_jit_bds_send *x);
t_jit_err max_jit_bds_send_outputmatrix(t_max_jit_bds_send *x);
void *max_jit_bds_send_class;
		 	
void main(void)
{	
	void *p,*q;
	
	jit_bds_send_init();	
	setup(&max_jit_bds_send_class, max_jit_bds_send_new, (method)max_jit_bds_send_free, (short)sizeof(t_max_jit_bds_send), 
		0L, A_GIMME, 0);

	p = max_jit_classex_setup(calcoffset(t_max_jit_bds_send,obex));
	q = jit_class_findbyname(gensym("jit_bds_send"));    
    max_jit_classex_mop_wrap(p,q,0); //MAX_JIT_MOP_FLAGS_OWN_JIT_MATRIX); //MAX_JIT_MOP_FLAGS_OWN_OUTPUTMATRIX | 
    max_jit_classex_standard_wrap(p,q,0); 	
    addmess((method)max_jit_mop_assist, "assist", A_CANT,0);
//	max_addmethod_usurp_low((method)max_jit_bds_send_outputmatrix, "outputmatrix");
}

void max_jit_bds_send_free(t_max_jit_bds_send *x)
{
	max_jit_mop_free(x);
	jit_object_free(max_jit_obex_jitob_get(x));
	max_jit_obex_free(x);
}

void *max_jit_bds_send_new(t_symbol *s, long argc, t_atom *argv)
{
	t_max_jit_bds_send *x,*o;

	if (x=(t_max_jit_bds_send *)max_jit_obex_new(max_jit_bds_send_class,gensym("jit_bds_send"))) {
		if (o=jit_object_new(gensym("jit_bds_send"))) {
			max_jit_mop_setup_simple(x,o,argc,argv);
			max_jit_attr_args(x,argc,argv);
		} else {
			jit_object_error((t_object *)x,"jit.bds_send: could not allocate object");
			freeobject(x);
			x = NULL;
		}
	}
	return (x);
}

//t_jit_err max_jit_bds_send_outputmatrix(t_max_jit_bds_send *x)
//{
//	void *mop,*o,*p;
//	long err=JIT_ERR_NONE;
//	t_atom a;
//	
//	if (!(mop=max_jit_obex_adornment_get(x,_jit_sym_jit_mop)))
//		return JIT_ERR_GENERIC;
//	if (jit_attr_getlong(mop,_jit_sym_outputmode)) {
//		if (err=(t_jit_err) jit_object_method(
//			max_jit_obex_jitob_get(x),
//			_jit_sym_matrix_calc,
//			jit_object_method(mop,_jit_sym_getinputlist),
//			jit_object_method(mop,_jit_sym_getoutputlist))) 
//		{
//			jit_error_code(x,err); 
//		} else {
//
//			if ((p=jit_object_method(mop,_jit_sym_getoutput,1)) &&
//				(o=max_jit_mop_io_getoutlet(p))) 
//			{
//				jit_atom_setsym(&a,jit_attr_getsym(p,_jit_sym_matrixname)); 
//				outlet_anything(o,_jit_sym_jit_matrix,1,&a);
//			}
//		}
//	}
//	return err;
//}