# Common Directory
This folder is for usage of common items shared amongst all projects.

The project should add include and lib to their respected lookup directories.

It is the responsibility of the programmer to add what files he or she needs to these
directories to get the job done. 